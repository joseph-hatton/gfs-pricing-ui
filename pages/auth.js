const agent = require('superagent')
const assert = require('assert')
const {getUserAndPass} = require('../config')
const I = actor()

const usernameInput = 'input[placeHolder="Username"]'
const passwordInput = 'input[placeHolder="Password"]'
const submitButton = 'input[value="Submit"]'

module.exports = {
  logInToSecureAuthIfRequired: async () => {
    const {user, password} = await getUserAndPass()
    const title = await I.grabTitle()
    if (title !== 'GFS Pricing') {
      assert(user, 'user must be set or retrieved from vault')
      assert(password, 'user\'s password must be set or retrieved from vault')
      I.see('Please enter your UserID below.')
      I.waitForElement(usernameInput, 10)
      I.fillField(usernameInput, user)
      I.fillField(passwordInput, password)
      I.click(submitButton)
      I.wait(5) // The SecureAuth redirect makes the testing lose the DOM.  We have to manually wait a bit.
    }
  }
}
