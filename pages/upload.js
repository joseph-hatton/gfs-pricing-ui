
const I = actor()
const {baseUrl} = require('../config')

const url = `${baseUrl}/upload`
const dealName = 'AXA Equitable FDA'
const code = 'LE_AME'
const dealPicker = 'div[name="id"] button'
const legalEntityPicker = 'div[name="legalEntity.code"] button'
const dealSelector = locate('span').withText(dealName)
const legalEntitySelector = locate('span').withText(code)
const resetButton = 'button.reset-button'
const nextButton = 'button.next-button'
const nextButtonWhenDisabled = 'button.next-button:disabled'
const saveButton = 'button.save-button'
const uploadSpreadsheetInput = 'input[name="spreadsheet"]'
const uploadProgressBar = 'div[role="progressbar"]'
const validationErrorsHeader = locate('h2').withText('Validation Errors').as('Validation Errors Header')
const uploadError = '.upload-error'

module.exports = {
  url,
  dealPicker,
  legalEntityPicker,
  legalEntitySelector,
  dealSelector,
  nextButton,
  nextButtonWhenDisabled,
  saveButton,
  uploadSpreadsheetInput,
  uploadProgressBar,
  validationErrorsHeader,
  uploadError,

  validateErrorShown: async (field, stringsToExpect) => {
    const element = `.${field}`
    I.seeElement(element)
    stringsToExpect.forEach(stringToExpect => I.see(stringToExpect, element))
  },

  uploadSpreadsheet: async (authPage, spreadsheet) => {
    I.amOnPage(url)

    await authPage.logInToSecureAuthIfRequired()

    I.waitForText('Select Deal', 10)

    // since we're not restarting the browser for each test, this resets the upload if it's already been ran before
    const disabledResetButtonText = await I.grabTextFrom(`${resetButton}:disabled`)
    if (disabledResetButtonText.length === 0) {
      I.click(resetButton)
      I.waitForElement(dealPicker, 5)
    }
    I.seeElement(dealPicker)
    I.click(dealPicker)

    I.waitForElement(dealSelector, 3)
    I.click(dealSelector)

    I.seeElement(legalEntityPicker)
    I.click(legalEntityPicker)

    I.waitForElement(legalEntitySelector, 3)
    I.click(legalEntitySelector)

    I.waitForElement(nextButton, 3)
    I.click(nextButton)

    I.see('Upload the GFS Pricing spreadsheet')
    I.attachFile(uploadSpreadsheetInput, spreadsheet)

    I.waitForElement(uploadProgressBar, 5)
  },

  waitForValidationToComplete() {
    I.waitForInvisible(uploadProgressBar, 60)
  },

  waitForErrorsToFullyShow() {
    I.waitForElement(validationErrorsHeader, 20)
    I.seeElement(nextButtonWhenDisabled)
    I.waitForInvisible(uploadProgressBar, 60)
  }
}
