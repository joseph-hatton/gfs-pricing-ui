/* eslint camelcase: "off" */
const fs = require('fs')
const path = require('path')
const moment = require('moment')
const {promisify} = require('es6-promisify')
const readFile = promisify(fs.readFile)
const writeFile = promisify(fs.writeFile)
const mkdirp = promisify(require('mkdirp'))
const agent = require('superagent')
const jwt = require('jsonwebtoken')
const {authUrl, clientId, clientSecret, userId, password} = require('./devServerConfig')

const fullName = (id, first, last) => {
  if (first && last) {
    return `${first} ${last} (dev)`
  } else {
    return last || first || id
  }
}

const createUser = ({access_token, id_token, expires_in}) => {
  const {sub, name, family_name, profile} = jwt.decode(id_token)

  // noinspection JSUnusedGlobalSymbols
  return {
    id: sub.toLowerCase(),
    firstName: name,
    lastName: family_name,
    fullName: fullName(sub, name, family_name),
    groups: (profile || '').split(',').map(g => g.trim().toLowerCase()).filter(Boolean),
    session: {
      token: access_token,
      expires: moment(Date.now() + 1000 * expires_in).toISOString(),
      stale: false
    }
  }
}

const cacheFileDir = [
  process.env.HOME,
  '.gfs-pricing-ui'
].join(path.sep)

const cacheFilePath = `${cacheFileDir}${path.sep}cached-user.json`

// if token expires in the next 6 minutes get a new one
const stalePeriod = 6 * 60 * 1000

const isExpired = expiry => !expiry || (moment(expiry).valueOf() - Date.now()) < stalePeriod

const validateUser = user => {
  if (user.session && !isExpired(user.session.expires)) {
    console.log(`using user cached in ${cacheFilePath} - delete if you're having issues`)
    return user
  } else {
    console.log('cached user session has expired - getting a new user from secureauth')
  }
}

const getCachedUser = () =>
  readFile(cacheFilePath, {encoding: 'utf8'})
    .then(json => validateUser(JSON.parse(json)))
    .catch(() => undefined)

const cacheUser = user =>
  mkdirp(cacheFileDir)
    .then(() =>
      writeFile(cacheFilePath, JSON.stringify(user), {encoding: 'utf8'})
    )
    .then(() => user)

const getNewUser = () =>
  agent.post(authUrl)
    .type('form')
    .send({
      client_id: clientId,
      client_secret: clientSecret,
      grant_type: 'password',
      username: userId,
      password,
      scope: 'openid profile'
    })
    .then(({body}) => cacheUser(createUser(body)))

module.exports = () =>
  getCachedUser()
    .then(user => user || getNewUser())
