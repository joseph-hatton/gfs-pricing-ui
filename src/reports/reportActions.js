
import http from '../actions/http'

import { reset } from 'redux-form'

export const getPricingReports = (REPORT_API) => (dispatch, getState) => {
    return dispatch({
      type: 'LOAD_ALL_REPORTS',
      payload: http.get(REPORT_API)
    })
}
export const setCurrentReportNull = () => (dispatch, getState) => {
    return dispatch({
      type: 'CURRENT_REPORT_NULL'
    })
}

export const openReportDialog = (title) => (dispatch, getState) => {
   return dispatch({
      type: 'OPEN_REPORT_DIALOG',
      payload: title
  })
}
export const closeReportDialog = () => (dispatch, getState) => {
    dispatch(reset('addEditReportForm'))
    dispatch({type: 'CLOSE_REPORT_DIALOG'})
}
export const setCategory = (event, value) => async (dispatch, getState) => {
 const { report: {pricingReports} } = getState()
 const reportingNameOptions = pricingReports.reduce((carry, item) => {
        if (item.category === value) carry.push(item['reportName'])
        return carry
    }, [])

 dispatch({
        type: 'SET_CATEGORY',
        payload: { currentCategory: value, reportingNameOptions: reportingNameOptions }
    })
}

export const getCurrentReport = (event, value) => (dispatch, getState) => {
    const { report: {pricingReports, currentCategory} } = getState()
    const currentReport = value ? pricingReports.filter(item => item.category === currentCategory && item.reportName === value)[0] : {category: currentCategory}
    return dispatch({
        type: 'CURRENT_REPORT',
        payload: currentReport
    })
}

export const saveReport = () => (dispatch, getState) => {
    const { form: { addEditReportForm: { values: { category, reportName, description, tableauLink } } }, report: {currentReport, reportDialog: {title}}, ref: {allRefs} } = getState()
    const REPORT_API = allRefs ? allRefs.urlRef : null
    const reportToSave = {category, reportName, description, tableauLink, app: 'GFS Pricing', tableauFlag: true}
    const action = title === 'New Report' ? 'post' : 'put'
    const url = title === 'New Report' ? REPORT_API : REPORT_API + '/' + currentReport.uuid
    return dispatch({
        type: 'SAVE_REPORT',
        payload: http[action](url, reportToSave)
    })
}
export const deleteReport = () => (dispatch, getState) => {
    const { report: { currentReport: { uuid } }, ref: {allRefs} } = getState()
    const REPORT_API = allRefs ? allRefs.urlRef : null
    return dispatch({
        type: 'DELETE_REPORT',
        payload: http.delete(REPORT_API + '/' + uuid)
    })
}

export const handleCloseSpeedDial = () => (dispatch, getState) => {
    return dispatch({
        type: 'CLOSE_SPEED_DIAL'
    })
}

export const handleOpenSpeedDial = () => (dispatch, getState) => {
    const { report: { speedDial: { hidden } } } = getState()
    if (!hidden) {
        return dispatch({
            type: 'OPEN_SPEED_DIAL'
        })
    }
}

export const handleClickSpeedDial = () => (dispatch, getState) => {
    const { report: { speedDial: { open } } } = getState()
    return dispatch({
        type: 'CLICK_SPEED_DIAL',
        payload: !open
    })
}

export const handleVisibilitySpeedDial = () => (dispatch, getState) => {
    const {report: {speedDial: {hidden}}} = getState()
    const payload = {open: false, hidden: !hidden}
   return dispatch({
        type: 'VISIBILITY_SPEED_DIAL',
        payload: payload
    })
}
