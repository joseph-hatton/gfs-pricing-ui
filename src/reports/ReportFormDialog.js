import React from 'react'
import T from 'prop-types'
import RaisedButton from 'material-ui/RaisedButton'
import Dialog from '@material-ui/core/Dialog'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import { closeReportDialog, saveReport, getPricingReports } from './reportActions'
import { connect } from 'react-redux'
import { Field, reduxForm, reset } from 'redux-form'
import { TextField } from 'redux-form-material-ui'
import { Required } from '../components/form/Validators'
export class ReportFormDialog extends React.Component {
  render () {
   const {handleSubmit, open, closeReportDialog, title, saveReport} = this.props
    return (
            <Dialog open={open} onClose={closeReportDialog} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title"> {title} </DialogTitle>
            <DialogContent>
            <form id='addEditReportForm' onSubmit={handleSubmit(saveReport)}>
                <Field component={TextField} validate={Required} name="category" floatingLabelText="Category" fullWidth/>
                <Field component={TextField} validate={Required} name="reportName" floatingLabelText="Report Name" fullWidth/>
                <Field component={TextField} validate={Required} name="description" floatingLabelText="Description" fullWidth/>
                <Field component={TextField} validate={Required} name="tableauLink" floatingLabelText="Tableau Link" fullWidth/>
                <RaisedButton key="Save" label="Save" primary={true} style={{ marginRight: 20 }} type="submit" form="addEditReportForm"/>
                <RaisedButton key="cancel" label="Cancel" onClick={closeReportDialog}/>
                </form>
            </DialogContent>
            </Dialog>
    )
  }
}

export const afterSubmit = (result, dispatch) => {
  dispatch(reset('addEditReportForm'))
  closeReportDialog()
}

ReportFormDialog.propTypes = {
    handleSubmit: T.func.isRequired,
    afterSubmit: T.func.isRequired,
    closeReportDialog: T.func.isRequired,
    saveReport: T.func.isRequired,
    getPricingReports: T.func.isRequired,
    open: T.bool,
    title: T.string
  }

  export const actions = {
    closeReportDialog,
    saveReport,
    getPricingReports
  }

  export const addEditReportForm = reduxForm({
    form: 'addEditReportForm',
    onSubmitSuccess: afterSubmit,
    enableReinitialize: true
  })(ReportFormDialog)

  export const mapStateToProps = ({report: {reportDialog: {open, title}, currentReport}, form}) => {
  return {
    open,
    title,
    initialValues: title === 'Edit Report' ? currentReport : null
  }
}

export default connect(mapStateToProps, actions)(addEditReportForm)
