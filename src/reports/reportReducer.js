
import _ from 'lodash'

const initialState = {
    reportDialog: { open: false, title: null },
    speedDial: { open: false, hidden: false },
    pricingReports: [],
    currentReport: null,
    currentCategory: null,
    reportingNameOptions: [],
    categories: [],
    reload: false
  }

export const getPricingReports = (allReports) => {
    return allReports.filter(item => item.app === 'GFS Pricing')
}

export const addOrUpdateReport = (pricingReports, saveReport) => {
  const existingReport = pricingReports.find(report => saveReport.uuid === report.uuid)
  if (existingReport) {
    return [saveReport].concat(_.filter(pricingReports, (i) => i.uuid !== saveReport.uuid))
  } else {
    return [saveReport].concat(pricingReports)
  }
}
export const getReportingName = (pricingReport, currentCategory) => {
 return pricingReport.reduce((acc, item) => {
    if (item.category === currentCategory) acc.push(item['reportName'])
    return acc
}, [])
}
export const getCategories = (reportList) => reportList.reduce((carry, item) => {
  if (item['category'] && !~carry.indexOf(item['category'])) carry.push(item['category'])
  return carry
}, [])

export default (state = initialState, action = {type: null}) => {
    switch (action.type) {
      case 'OPEN_REPORT_DIALOG':
      return {
        ...state,
        reportDialog: { open: true, title: action.payload }
      }
      case 'CLOSE_REPORT_DIALOG':
      return {
        ...state,
        reportDialog: { open: false }
      }
      case 'LOAD_ALL_REPORTS_FULFILLED':
      const pricingReports = getPricingReports(action.payload)
      return {
        ...state,
        pricingReports: pricingReports,
        categories: getCategories(pricingReports)
      }
      case 'CURRENT_REPORT':
      return {
        ...state,
        currentReport: action.payload
      }
      case 'LOAD_CATEGORY':
      return {
        ...state,
        categories: action.payload
      }
      case 'SAVE_REPORT_FULFILLED':
      const currentCategory = action.payload.category
      const refresPricingReport = addOrUpdateReport(state.pricingReports, action.payload)
      return {
        ...state,
        reportDialog: { open: false },
        pricingReports: refresPricingReport,
        reload: true,
        categories: getCategories(refresPricingReport),
        currentReport: action.payload,
        reportingNameOptions: getReportingName(refresPricingReport, currentCategory),
        form: { Report: { values: { reportName: action.payload.reportName, category: action.payload.category } } }
      }
      case 'SET_CATEGORY':
      return {
        ...state,
        currentCategory: action.payload.currentCategory,
        reportingNameOptions: action.payload.reportingNameOptions,
        currentReport: {category: action.payload.currentCategory},
        form: { Report: { values: { category: action.payload.currentCategory } } }
      }
      case 'CLOSE_SPEED_DIAL':
      return {
        ...state,
        speedDial: {open: false}
      }
      case 'OPEN_SPEED_DIAL':
      return {
        ...state,
        speedDial: {open: true}
      }
      case 'CLICK_SPEED_DIAL':
      return {
        ...state,
        speedDial: {open: action.payload}
      }
      case 'VISIBILITY_SPEED_DIAL':
      return {
        ...state,
        speedDial: action.payload
      }
      case 'CURRENT_REPORT_NULL':
      return {
        ...state,
        currentReport: null
      }
      case 'DELETE_REPORT_FULFILLED':
      const newPricingReportList = state.pricingReports.filter((r) => r.uuid !== action.payload.uuid)
      return {
        ...state,
        categories: getCategories(newPricingReportList),
        pricingReports: newPricingReportList,
        currentReport: null,
        currentCategory: null,
        reportingNameOptions: []
      }
      default:
        return state
    }
 }
