import React from 'react'
import { PropTypes as T } from 'prop-types'
import Grid from '@material-ui/core/Grid'
import { connect } from 'react-redux'
import { CommonPicker } from '../pickers/CommonPicker'
import { reduxForm } from 'redux-form'
import { getPricingReports, getCurrentReport, setCategory, setCurrentReportNull } from './reportActions'
import SpeedDialReport from './SpeedDialReport'
import ReportFormDialog from './ReportFormDialog'

export class Reports extends React.Component {
  componentWillUnmount () {
    this.props.setCurrentReportNull()
    }

  render () {
    if (!this.props.urlRef) {
      return <div>loading ...</div>
    } else {
    const { optionsCategory, getCurrentReport, setCategory, currentReport, reportingNameOptions, urlRef } = this.props
    if (optionsCategory.length === 0) {
      this.props.getPricingReports(urlRef)
    }
    return (
   <form id='Report'>
      <Grid container spacing={0}>
        <CommonPicker nullable onChange= {setCategory} name="category" floatingLabelText="Category" options={optionsCategory} />
        <CommonPicker nullable onChange= {getCurrentReport} name="reportName" floatingLabelText="Report Name" options={reportingNameOptions} />
      </Grid>
      { currentReport && (
        <iframe style={{ position: 'absolute', height: '70%', border: 'none' }} width="80%"src={currentReport.tableauLink}>ggg</iframe>
      )}
    <ReportFormDialog/>
    <SpeedDialReport/>
    </form>
    )
  }
  }
}

Reports.propTypes = {
  getPricingReports: T.func.isRequired,
  getCurrentReport: T.func.isRequired,
  setCategory: T.func.isRequired,
  pricingReports: T.array,
  optionsCategory: T.array,
  currentReport: T.object,
  reportingNameOptions: T.array,
  initialValues: T.object,
  currentCategory: T.string,
  setCurrentReportNull: T.func.isRequired,
  urlRef: T.string
}

export const ReportForm = reduxForm({
    form: 'Report',
    enableReinitialize: true
  })(Reports)

const actions = {
  getPricingReports,
  getCurrentReport,
  setCategory,
  setCurrentReportNull
  }

export const mapStateToProps = ({report: {categories, pricingReports, currentReport = null, reportingNameOptions, currentCategory}, ref: {allRefs}}) => {
  return {
  pricingReports,
  optionsCategory: categories,
  currentReport,
  reportingNameOptions,
  currentCategory,
  initialValues: currentReport,
  urlRef: allRefs ? allRefs.urlRef : null
}
}

export default connect(mapStateToProps, actions)(ReportForm)
