import React from 'react'
import { PropTypes as T } from 'prop-types'
import { withStyles, MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import { deleteReport, openReportDialog, handleCloseSpeedDial, handleVisibilitySpeedDial, handleClickSpeedDial, handleOpenSpeedDial } from './reportActions'
import SpeedDial from '@material-ui/lab/SpeedDial'
import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon'
import SpeedDialAction from '@material-ui/lab/SpeedDialAction'
import DeleteIcon from '@material-ui/icons/Delete'
import AddIcon from '@material-ui/icons/Add'
import EditIcon from '@material-ui/icons/Edit'
import {connect} from 'react-redux'
import { openConfirmationAlert } from '../components/redux-dialog'

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#33c9dc',
      contrastText: '#fff'
    }
  }
})
const styles = theme => ({
  root: {
    height: 380
  },
  speedDial: {
    position: 'absolute',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 3
  }
})

export class SpeedDialReport extends React.Component {
  render () {
    const { currentReport, hidden, open, classes, handleCloseSpeedDial, handleClickSpeedDial, handleOpenSpeedDial, openReportDialog, deleteReport, openConfirmationAlert } = this.props
    let isTouch
    if (typeof document !== 'undefined') {
      isTouch = 'ontouchstart' in document.documentElement
    }

    return (
        <div className={classes.root}>
        <MuiThemeProvider theme={theme}>
        <SpeedDial ariaLabel="Report SpeedDial" className={classes.speedDial} hidden={hidden} icon={<SpeedDialIcon />}
          onBlur={handleCloseSpeedDial} onClick={handleClickSpeedDial} onClose={handleCloseSpeedDial}
          onFocus={isTouch ? undefined : handleOpenSpeedDial}
          onMouseEnter={isTouch ? undefined : handleOpenSpeedDial}
          onMouseLeave={handleCloseSpeedDial}
          open={open}
        >
        <SpeedDialAction label='New Report' key='New Report' icon={<AddIcon />} primaryText='New Report' tooltipTitle='New Report' onClick={() => openReportDialog('New Report') } />
        { currentReport && currentReport.uuid && (<SpeedDialAction key='Edit Report' icon={<EditIcon />} tooltipTitle='Edit Report' onClick={() => openReportDialog('Edit Report') } />)}
        { currentReport && currentReport.uuid && (<SpeedDialAction key='Delete Report' icon={<DeleteIcon />} tooltipTitle='Delete Report' onClick={() => {
          openConfirmationAlert({ message: 'Are you sure you want to delete this Report?',
                onOk: () => deleteReport() })
          }
                } />)}

        </SpeedDial>
        </MuiThemeProvider>
        </div>
    )
  }
}

SpeedDialReport.propTypes = {
  classes: T.object,
  handleCloseSpeedDial: T.func.isRequired,
  openConfirmationAlert: T.func.isRequired,
  handleVisibilitySpeedDial: T.func.isRequired,
  handleClickSpeedDial: T.func.isRequired,
  handleOpenSpeedDial: T.func.isRequired,
  openReportDialog: T.func.isRequired,
  deleteReport: T.func.isRequired,
  currentReport: T.object,
  open: T.bool,
  hidden: T.bool
}

export const mapStateToProps = ({report: {currentReport, speedDial: { open = false, hidden = false }}}) => {
    return {
        open,
        hidden,
        currentReport
  }
}

const actions = {
    openReportDialog,
    handleCloseSpeedDial,
    handleVisibilitySpeedDial,
    handleClickSpeedDial,
    handleOpenSpeedDial,
    deleteReport,
    openConfirmationAlert
}

export default connect(mapStateToProps, actions)(withStyles(styles)(SpeedDialReport))
