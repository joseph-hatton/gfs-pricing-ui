import { createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'
import rootReducer from './reducers'
import promiseMiddleware from 'redux-promise-middleware'
import { composeWithDevTools } from 'redux-devtools-extension'
const {NODE_ENV} = process.env

const actionsToIgnore = [
  '@@redux-form/FOCUS',
  '@@redux-form/BLUR',
  '@@redux-form/REGISTER_FIELD'
]
let middleware
if (NODE_ENV === 'production') {
  console.log('production middleware')
  middleware = applyMiddleware(
    thunk,
    promiseMiddleware(),
    createLogger({
      level: 'info',
      diff: true,
      predicate: (getState, action) => !actionsToIgnore.includes(action.type)
    })
  )
} else {
  console.log('dev middleware')
  middleware = composeWithDevTools(
    applyMiddleware(thunk, promiseMiddleware())
  )
}

export default createStore(rootReducer, {}, middleware)
