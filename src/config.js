console.log('API_BASE********', process.env.API_BASE)

export const apiPath = process.env.API_BASE || window.API_BASE || 'https://gfs-pricing-api.rgare.net/api/v1'

export const appVersion = process.env.APP_VERSION
