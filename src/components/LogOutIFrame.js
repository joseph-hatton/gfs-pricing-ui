import React from 'react'
import T from 'prop-types'
import reload from '../reload'

class LogOutIFrame extends React.Component {
  componentDidMount () {
    setTimeout(reload, this.props.reloadDelay)
  }

  render () {
    const {url, height, width} = this.props
    return (
      <iframe src={url} height={height} width={width}>
        <div className='alert alert-danger ml-3 mt-3 mr-3'>
          <strong>Unable to Log out!</strong>
          {' This browser does not support internal frames'}
        </div>
      </iframe>
    )
  }
}

LogOutIFrame.propTypes = {
  url: T.string.isRequired,
  height: T.number.isRequired,
  width: T.number.isRequired,
  reloadDelay: T.number
}

LogOutIFrame.defaultProps = {
  reloadDelay: 1000
}

export default LogOutIFrame
