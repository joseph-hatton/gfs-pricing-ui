import React from 'react'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'
import {RaisedButton} from 'material-ui'
import {red500} from 'material-ui/styles/colors'
import ReduxDialog, {closeDialog} from './redux-dialog'

export const DEFAULT_DIALOG_NAME = 'confirmationAlert'

export const ConfirmationAlert = (props) => {
  const {dialog, children, closeDialog} = props
  const dialogName = props.dialogName || DEFAULT_DIALOG_NAME
  const confirmationAlert = dialog[dialogName] || {}
  const {onOk, onCancel, message, disableCancel = false, contentStyle, style} = confirmationAlert

  const actions = [
    <RaisedButton
      key="Ok"
      label="Ok"
      primary={true}
      style={{marginRight: 20}}
      onClick={() => {
        closeDialog(dialogName)
        onOk()
      }}
    />
  ]
  if (!disableCancel) {
    actions.push(<RaisedButton
      key="Cancel"
      label="Cancel"
      onClick={() => {
        closeDialog(dialogName)
        if (onCancel) {
          onCancel()
        }
      }}
    />)
  }
  return (
    <ReduxDialog
      dialogName={dialogName}
      title='Warning'
      actions={actions}
      modal={true}
      autoScrollBodyContent={true}
      contentStyle={contentStyle}
      style={style}
    >
      {message && (<span style={{color: red500}}> {message} </span>)}
      {children}
    </ReduxDialog>
  )
}

ConfirmationAlert.propTypes = {
  closeDialog: T.func.isRequired,
  onOk: T.func,
  onCancel: T.func,
  children: T.array,
  dialog: T.object,
  dialogName: T.string
}

export const mapStateToProps = ({dialog}) => ({dialog})

export default connect(mapStateToProps, {closeDialog})(ConfirmationAlert)
