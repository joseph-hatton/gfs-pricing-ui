import React from 'react'
import {PropTypes as T} from 'prop-types'
import _ from 'lodash'
import {connect} from 'react-redux'
import Dialog from 'material-ui/Dialog'
import {orange500, green500, yellow500} from 'material-ui/styles/colors'

import {Snackbar as MaterialSnackbar, CircularProgress, RaisedButton} from 'material-ui'

export const openDialog = (name, props) => {
  return {
    type: 'UPDATE_DIALOG_STATE',
    payload: {[name]: props || true}
  }
}

export const openSnackbar = (props) => {
  return {
    type: 'UPDATE_DIALOG_STATE',
    payload: {'snackbar': props}
  }
}

export const openConfirmationAlert = (props, dialogName) => {
  return {
    type: 'UPDATE_DIALOG_STATE',
    payload: {[dialogName || 'confirmationAlert']: props}
  }
}

export const closeDialog = (name) => {
  return {
    type: 'UPDATE_DIALOG_STATE',
    payload: {[name]: false}
  }
}

export const spin = () => {
  return {
    type: 'UPDATE_DIALOG_STATE',
    payload: {spinner: true}
  }
}

export const stop = () => {
  return {
    type: 'UPDATE_DIALOG_STATE',
    payload: {spinner: false}
  }
}

const PROPS_TO_OVERRIDE = ['actions', 'title']

const LEFT_PROP = 'screenX'
const TOP_PROP = 'screenY'

export class ReduxDialog extends React.Component {
  constructor (props) {
    super(props)
    this.state = {left: 0, top: 0, offsetCalibrated: false, offsetLeft: 0, offsetTop: 0}
    this.onDrag = this.onDrag.bind(this)
    this.onEnd = this.onEnd.bind(this)
  }

  cancel () {
    const {dialogName, closeDialog, onCancel} = this.props
    closeDialog(dialogName)
    if (_.isFunction(onCancel)) {
      onCancel()
    }
  }

  stopEventPropagation (event) {
    if (event.stopPropagation) {
      event.stopPropagation()
    }
    if (event.preventDefault) {
      event.preventDefault()
    }
  }

  onStart (event) {
    this.stopEventPropagation(event)
    const {offsetCalibrated} = this.state
    if (!offsetCalibrated) {
      this.setState({offsetLeft: event[LEFT_PROP], offsetTop: event[TOP_PROP], offsetCalibrated: true})
    }
    window.addEventListener('mousemove', this.onDrag)
    window.addEventListener('mouseup', this.onEnd)
  }

  onEnd (event) {
    this.stopEventPropagation(event)
    window.removeEventListener('mousemove', this.onDrag)
    window.removeEventListener('mouseup', this.onEnd)
  }

  onDrag (event) {
    this.stopEventPropagation(event)
    const {offsetLeft, offsetTop} = this.state
    const newState = {left: event[LEFT_PROP] - offsetLeft, top: event[TOP_PROP] - offsetTop}
    this.setState(newState)
  }

  render () {
    const {
      dialogState, dialogName, cancelButton, cancelLabel = 'Cancel', actions, title,
      contentStyle: dialogContentStyle
    } = this.props
    const open = !!dialogState[dialogName]
    const actionButtons = cancelButton ? actions.concat(<RaisedButton key="Cancel" label={cancelLabel}
                                                                      onClick={::this.cancel}/>) : actions

    const {left, top} = this.state
    const contentStyle = {...dialogContentStyle, left, top}

    return (
      <Dialog open={open} {..._.omit(this.props, PROPS_TO_OVERRIDE)} actions={actionButtons} contentStyle={contentStyle}
              bodyStyle={{padding: 0}}>
        {
          open &&
          <div style={{padding: 24, paddingTop: 20}}>
            <div onMouseDown={::this.onStart}>{title}</div>
            <div>
            <br />
              {this.props.children}
            </div>
          </div>
        }
      </Dialog>
    )
  }
}

ReduxDialog.propTypes = {
  children: T.any.isRequired,
  dialogState: T.object,
  dialog: T.object,
  dialogName: T.string,
  title: T.string.isRequired,
  cancelButton: T.bool,
  actions: T.array,
  closeDialog: T.func.isRequired,
  onCancel: T.func,
  cancelLabel: T.string,
  contentStyle: T.object
}

export const mapStateToProps = ({dialog}) => ({dialogState: dialog})

export default connect(mapStateToProps, {openDialog, closeDialog})(ReduxDialog)

export const ERROR = 'error'
export const SUCCESS = 'success'
export const WARNING = 'warning'

const typeColorMap = {
  [ERROR]: orange500,
  [SUCCESS]: green500,
  [WARNING]: yellow500
}

export class ReduxSnackbar extends React.Component {
  render () {
    const {snackbar} = this.props
    const type = snackbar && snackbar.type ? snackbar.type : SUCCESS
    const open = !!snackbar
    return (
      <MaterialSnackbar open={open} message=''
                        bodyStyle={{
                          backgroundColor: typeColorMap[type],
                          color: 'white',
                          maxWidth: 800,
                          fontSize: 15
                        }} {...snackbar}/>
    )
  }
}

ReduxSnackbar.propTypes = {
  snackbar: T.object,
  type: T.string,
  closeDialog: T.func.isRequired
}

export const mapStateToSnackbar = ({dialog: {snackbar}}) => ({snackbar})

export const Snackbar = connect(mapStateToSnackbar, {closeDialog})(ReduxSnackbar)

export const Spinner = ({spin}) => {
  return (
    <div>
      {spin && <div className="spinner-overlay"><CircularProgress className="spinner" size={50} thickness={5}/></div>}
    </div>
  )
}

Spinner.propTypes = {
  spin: T.bool
}

export const mapStateToSpinner = ({dialog}) => ({spin: dialog.spinner || false})

export const MaterialSpinner = connect(mapStateToSpinner)(Spinner)
