import reduxDialog, {
  openDialog,
  closeDialog,
  Snackbar,
  openConfirmationAlert,
  spin,
  stop,
  openSnackbar,
  MaterialSpinner
} from './redux-dialog'
import ConfirmationAlert from './ConfirmationAlert'
import dialogReducer from './reducer'

export {
  openDialog,
  closeDialog,
  Snackbar,
  ConfirmationAlert,
  openConfirmationAlert,
  spin,
  stop,
  openSnackbar,
  MaterialSpinner,
  dialogReducer
}
export default reduxDialog
