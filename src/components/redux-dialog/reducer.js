const CLOSE_ALL = {}

export default (state = CLOSE_ALL, action) => {
  switch (action.type) {
    case 'UPDATE_DIALOG_STATE':
      return {
        ...state,
        ...action.payload
      }
    default:
      return state
  }
}
