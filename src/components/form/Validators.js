import Moment from 'moment'
import numeral from 'numeral'

export const dateFormat = (date) => {
    return date ? Moment(date).format('MM/DD/YYYY') : ''
  }
export const toNumber = (text) => {
  if (text) {
    const val = numeral(text).value()
    return val < 0 ? val * -1 : val
  }
  return null
}
export const exists = value => (value !== undefined && value !== null && value !== '')
export const Required = value => exists(value) ? undefined : 'Required'
