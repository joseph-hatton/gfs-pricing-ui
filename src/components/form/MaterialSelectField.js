import React from 'react'
import {PropTypes as T} from 'prop-types'
import _ from 'lodash'
import {Field} from 'redux-form'
import MenuItem from 'material-ui/MenuItem'
import { SelectField } from 'redux-form-material-ui'

class MaterialSelectField extends React.Component {
  render () {
    const {nullable, options, name} = this.props
    const propsToPassOn = _.omit(this.props, 'options', 'name')
    return (
      <Field
        name={name}
        component={SelectField}
        {...propsToPassOn}
      >
        {
          nullable && <MenuItem key="_empty" value={null} primaryText=""/>
        }
        {
          options.map((item) => (<MenuItem key={item} value={item} primaryText={item}/>))
        }
      </Field>
    )
  }
}

MaterialSelectField.displyName = 'MaterialSelectField'

MaterialSelectField.propTypes = {
  name: T.string,
  options: T.array,
  nullable: T.bool
}

export default MaterialSelectField
