import React from 'react'
import {Link} from 'react-router-dom'
import {PropTypes as T} from 'prop-types'
import Drawer from 'material-ui/Drawer'
import MenuItem from 'material-ui/MenuItem'
import {connect} from 'react-redux'

class NavBar extends React.Component {
  render () {
    const {open, fullName} = this.props
    return (<Drawer containerStyle={{backgroundColor: 'rgb(70, 70, 70)'}} open={open} className='sidebarMenu'>
      <div className='mainTitle'>
        GFS Pricing
      </div>
      <div className='userInfo'>
        <span style={{width: '100%', display: 'inline-block'}}>{fullName}</span>
        <small style={{color: '#ccc', marginTop: 8, display: 'inline-block'}}>Admin</small>
      </div>
      <MenuItem><Link to='/'>Home</Link></MenuItem>
      <MenuItem><Link to='/upload'>Upload Pricing Data</Link></MenuItem>
      <MenuItem><Link to='/reports'>Reports</Link></MenuItem>
    </Drawer>)
  }
}

NavBar.propTypes = {
  open: T.bool,
  fullName: T.string
}

export const mapStateToProps = ({core: {leftDrawerOpen}, currentUser}) => ({
  open: leftDrawerOpen,
  fullName: currentUser.fullName
})

export default connect(mapStateToProps)(NavBar)
