import React from 'react'
import {withRouter} from 'react-router-dom'
import TopBar from './TopBar'
import LeftSideDrawer from './LeftSideDrawer'
import Routes from './Routes'
import {PropTypes as T} from 'prop-types'
import {connect} from 'react-redux'

import { Snackbar, ConfirmationAlert, MaterialSpinner, openConfirmationAlert } from '../redux-dialog'

class App extends React.Component {
  render () {
    const {leftDrawerOpen} = this.props
    const paddingLeft = leftDrawerOpen ? 256 : 0
    const topBarStyle = {paddingLeft: 24 + paddingLeft, transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms'}
    const contentStyle = {padding: 15, paddingLeft: 15 + paddingLeft, transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms'}
    return (<div>
      <TopBar style={topBarStyle} />
      <LeftSideDrawer/>
      <MaterialSpinner/>
      <div style={contentStyle}><Routes/></div>
      <Snackbar />
      <ConfirmationAlert />
    </div>)
  }
}

App.propTypes = {
  toggleLeftDrawer: T.func,
  leftDrawerOpen: T.bool,
  openConfirmationAlert: T.func.isRequired
}

const actions = {
  openConfirmationAlert
}

export const mapStateToProps = ({core: {leftDrawerOpen}}) => ({
  leftDrawerOpen
})

export default withRouter(connect(mapStateToProps, actions)(App))
