export const toggleLeftDrawer = () => (dispatch, getState) => {
  const {core: {leftDrawerOpen}} = getState()
  dispatch({
    type: 'SET_LEFT_DRAWER_OPEN',
    open: !leftDrawerOpen
  })
}
