import React from 'react'
import {PropTypes as T} from 'prop-types'
import AppBar from 'material-ui/AppBar'
import {connect} from 'react-redux'
import {toggleLeftDrawer} from './coreActions'

class TopBar extends React.Component {
  render () {
    const {toggleLeftDrawer, style} = this.props
    return (<div>
      <AppBar style={style} onLeftIconButtonClick={toggleLeftDrawer}/>
    </div>)
  }
}

TopBar.propTypes = {
  toggleLeftDrawer: T.func,
  style: T.object
}

export default connect(null, {toggleLeftDrawer})(TopBar)
