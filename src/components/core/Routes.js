import React from 'react'
import {Route} from 'react-router-dom'

import Home from './Home'
import Reports from '../../reports/Reports'
import UploadSpreadsheet from '../../upload/UploadSpreadsheet'

class Routes extends React.Component {
  render () {
    return (
      <div>
        <Route path={'/'} exact={true} component={Home} />
        <Route path={'/upload'} exact={true} component={UploadSpreadsheet} />
        <Route path={'/reports'} exact={true} component={Reports} />
      </div>
    )
  }
}

Routes.propTypes = {}

export default Routes
