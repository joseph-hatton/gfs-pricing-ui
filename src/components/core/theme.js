import getMuiTheme from 'material-ui/styles/getMuiTheme'

const theme = getMuiTheme({
  fontFamily: 'Roboto, sans-serif'
})

export default theme
