const initialState = {
  leftDrawerOpen: true
}

export default (state = initialState, action = {type: null}) => {
  switch (action.type) {
    case 'SET_LEFT_DRAWER_OPEN':
    return {
      ...state,
      leftDrawerOpen: action.open
    }
    default:
    return state
  }
}
