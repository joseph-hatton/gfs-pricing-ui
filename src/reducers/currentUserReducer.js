
const initialState = {
  id: 'unknown',
  name: 'unknown'
}

export default (state = initialState, action = {type: null}) => {
  if (action.type === 'SET_CURRENT_USER') {
    return {
      ...state,
      ...action.user
    }
  } else if (action.type === 'TIME_OUT_USER') {
    return {
      ...state,
      session: {
        ...state.session,
        timedOut: true
      }
    }
  } else {
    return state
  }
}
