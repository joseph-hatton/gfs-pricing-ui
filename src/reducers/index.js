import {combineReducers} from 'redux'
import {reducer as formReducer} from 'redux-form'
import coreReducer from './coreReducer'
import currentUserReducer from './currentUserReducer'
import uploadReducer from '../upload/uploadReducer'
import reportReducer from '../reports/reportReducer'
import dealReducer from '../deals/dealReducer'
import marketingReducer from '../marketing/marketingReducer'
import refReducer from '../refs/refReducer'
import {dialogReducer} from '../components/redux-dialog'

export default combineReducers({
  form: formReducer,
  deal: dealReducer,
  marketing: marketingReducer,
  ref: refReducer,
  core: coreReducer,
  currentUser: currentUserReducer,
  upload: uploadReducer,
  dialog: dialogReducer,
  report: reportReducer
})
