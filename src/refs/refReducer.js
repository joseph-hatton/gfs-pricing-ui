import _ from 'lodash'
const initialState = {
  allRefs: null,
  productTypes: null,
  countries: null,
  dealStatuses: null,
  stages: null,
  probCloses: null,
  businessTree: null
}

const toBusinessTree = (businessUnitTree) => {
  const pickerObject = {
    businessName: [],
    geoSegment: [],
    legalEntity: [],
    office: [],
    currency: []
  }
  businessUnitTree.map(item => {
    pickerObject.businessName.push(item.businessName)
    pickerObject.geoSegment.push(item.geoSegmentCode)
    pickerObject.legalEntity.push(item.legalEntityCode)
    pickerObject.office.push(item.officeCode)
    pickerObject.currency.push(item.currencyCode)
  })
  pickerObject.businessName = (_.uniq(pickerObject.businessName)).sort()
  pickerObject.geoSegment = (_.uniq(pickerObject.geoSegment)).sort()
  pickerObject.legalEntity = (_.uniq(pickerObject.legalEntity)).sort()
  pickerObject.office = (_.uniq(pickerObject.office)).sort()
  pickerObject.currency = (_.uniq(pickerObject.currency)).sort()
  return pickerObject
}

export default (state = initialState, action = { type: null }) => {
  switch (action.type) {
    case 'LOAD_ALL_REFS_FULFILLED':
      return {
        ...state,
        allRefs: action.payload,
        productTypes: _.uniq(action.payload.productTypes.map(productType => productType.value)).sort(),
        countries: _.uniq(action.payload.countries.map(country => country.countryCode)).sort(),
        dealStatuses: action.payload.dealStatuses,
        stages: action.payload.stages,
        probCloses: action.payload.probCloses,
        businessTree: toBusinessTree(action.payload.businessUnitTree)
      }
    default:
      return state
  }
}
