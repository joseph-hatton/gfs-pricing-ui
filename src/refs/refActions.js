import { apiPath } from '../config'
import http from '../actions/http'

const REFS_HOME = '/refs'

export const loadAllRefs = (dispatch) => {
  dispatch({
    type: 'LOAD_ALL_REFS',
    payload: http.get(`${apiPath}${REFS_HOME}`)
  })
}
