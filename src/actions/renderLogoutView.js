import React from 'react'
import { render } from 'react-dom'
import LogOutIFrame from '../components/LogOutIFrame'

export default ({url, height, width}) =>
  render(
    (<LogOutIFrame url={url} height={height} width={width}/>),
    document.getElementById('main')
  )
