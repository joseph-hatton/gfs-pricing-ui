import {updateCurrentUser} from './getCurrentUser'
import {loadAllRefs} from '../refs/refActions'

const phaseOne = [
  updateCurrentUser
]

const phaseTwo = [
  loadAllRefs
]

export default async (dispatch, getState) => {
  const invoke = actions =>
    Promise.all(actions.map(action => action(dispatch, getState)))

  await invoke(phaseOne)
  await invoke(phaseTwo)
}
