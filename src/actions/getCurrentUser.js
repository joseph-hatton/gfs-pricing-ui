import agent from 'superagent'
import moment from 'moment'
import logOut from './logOut'
import { openConfirmationAlert } from '../components/redux-dialog'

let dispatch, user

const isSessionExpired = () => {
  if (user) {
    const minExpiration = moment().add(30, 'seconds')
    return moment(user.session.expires).isBefore(minExpiration)
  } else {
    return true
  }
}

export const initializeDispatch = initDispatch => {
  dispatch = initDispatch
}

export const updateCurrentUser = async initDispatch => {
  dispatch = dispatch || initDispatch
  try {
    const {body} = await agent.get('/__auth/current-user').redirects(0)
    user = body
    dispatch({type: 'SET_CURRENT_USER', user})
    let stale = user && user.session && Boolean(user.session.stale)
    if (stale) {
      console.log('about to logout')
      dispatch(openConfirmationAlert({
        message: 'Your session will expire soon.  Please click OK to log out and back in',
        onOk: () => logOut(dispatch)
      }))
    } else {
    }
  } catch (error) {
    if (user) {
      // session could not be refreshed so reload
      dispatch({type: 'TIME_OUT_USER'})
    } else {
      throw error
    }
  }
}

const getCurrentUser = async initDispatch => {
  if (isSessionExpired()) {
    await updateCurrentUser(initDispatch)
  }
  return user
}

export default getCurrentUser
