import isFunction from 'lodash/isFunction'
import getCurrentUser from './getCurrentUser'
import fromPairs from 'lodash/fromPairs'
import agent from 'superagent'
import _get from 'lodash/get'
import { spin, stop, openSnackbar } from '../components/redux-dialog'

import store from '../store'
export const UNAUTHORIZED_ERROR_MESSAGE = 'Unauthorized. Contact Help Desk for Access.'
export const STANDARD_ERROR_MESSAGE = 'Error in processing your request, please contact support.'

const displaySuccessMessage = (response, {successMessage}) => {
  if (successMessage) {
    store.dispatch(openSnackbar({
      type: 'success',
      message: isFunction(successMessage) ? successMessage(response) : successMessage,
      autoHideDuration: 5000
    }))
  }
}

const displayErrorMessage = (response, {errorMessage = null} = {}) => {
  store.dispatch(openSnackbar({
    type: 'error',
    message: isFunction(errorMessage) ? errorMessage(response) || STANDARD_ERROR_MESSAGE : errorMessage || STANDARD_ERROR_MESSAGE,
    autoHideDuration: 5000 * 1000
  }))
}

const showSpinner = () => store.dispatch(spin())

const stopSpinner = () => {
  setTimeout(() => {
    store.dispatch(stop())
  }, 500)
}

const stopSpinnerOnError = (err) => {
  stopSpinner()
  let message = null
  if (err.status && err.status === 401) {
    message = {errorMessage: UNAUTHORIZED_ERROR_MESSAGE}
  } else if (err.response && err.response.body && err.response.body.message) {
    let messages = _get(err.response.body.message, 'messages', [])
    messages = messages.join(' ')
    message = {errorMessage: messages || err.response.body.message}
  }
  displayErrorMessage(err, message)
  return err
}

const createRequest = (method, url) => agent[method](url)

const invokeRequestWithAuthorization = async (request) => {
  const {session: {token}} = await getCurrentUser()
  if (token) {
    request.set({Authorization: `Bearer ${token}`})
  }
  return request
}

const createJsonRequest = (method, url) =>
  createRequest(method, url)
    .accept('json')

const get = async (url, query = {}) => {
  showSpinner()
  try {
    const request = createJsonRequest('get', url)
      .query(query)
    const {body} = await invokeRequestWithAuthorization(request)
    stopSpinner()
    return body
  } catch (err) {
    throw stopSpinnerOnError(err)
  }
}

const modify = async (method, url, requestBody, messageConfig = {}) => {
  showSpinner()
  const request = createJsonRequest(method, url)
    .type('json')
    .send(requestBody)
  try {
    const {body} = await invokeRequestWithAuthorization(request)
    stopSpinner()
    displaySuccessMessage(body, messageConfig)
    return body
  } catch (err) {
    throw stopSpinnerOnError(err)
  }
}

const updateMethods = fromPairs(
  [
    'put',
    'post',
    'delete',
    'patch'
  ].map(method => [
    method,
    (url, requestBody, messageConfig) => modify(method, url, requestBody, messageConfig)
  ])
)

const httpErrorMessage = (error) => {
  return _get(error, 'response.body.message')
}

export default {
  httpErrorMessage,
  createRequest,
  invokeRequestWithAuthorization,
  get,
  ...updateMethods
}
