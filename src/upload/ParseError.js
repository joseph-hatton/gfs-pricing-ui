import snakeCase from 'lodash/snakeCase'
import React from 'react'
import {PropTypes as T} from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Button from '@material-ui/core/Button'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import { connect } from 'react-redux'
import { openDialog } from '../components/redux-dialog/redux-dialog'

const styles = {
  card: {
    minWidth: 275
  },
  content: {
    padding: '.75em'
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)'
  },
  title: {
    marginBottom: 16,
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  }
}

export class ParseError extends React.Component {
  render () {
    const {parseError, classes, openDialog} = this.props
    return (
      <Grid item sm={12} md={6} lg={4} xl={3}>
        <Card id={classes.field} className={`${classes.card} upload-error`}>
          <CardContent className={`${classes.content} ${snakeCase(parseError.field)}`}>
            <Typography className={classes.title} color="textSecondary">
              {parseError.tab} {(parseError.company || parseError.accountingBasis) && ` - ${parseError.accountingBasis}`}
            </Typography>
            <Typography variant="headline" component="h2">
              {parseError.field}
            </Typography>
            <Typography className={classes.pos} color="textSecondary">
              {(parseError.row) && `row: ${parseError.row} `}
              {(parseError.col) && `col: ${parseError.col} `}
              {(parseError.duration) && `duration: ${parseError.duration}`}
            </Typography>
            <Typography component="div" className={`${parseError.field}-message`}>
              {parseError.message}
              { parseError.validOptions && (
              <Button id={parseError.field} name={parseError.field}
              onClick={() => openDialog('validOptions', { validOptions: parseError.validOptions, title: parseError.field }) } size="small" color="primary"> Valid Options </Button>
              )}
              {(parseError.inputValue !== undefined || parseError.calculatedValue) && (
                <Grid container direction="row" justify="space-evenly" alignItems="center">
                  <Grid item>{parseError.inputValue !== undefined && (<Typography color="textSecondary">input: {parseError.inputValue || 'empty'}</Typography>)}</Grid>
                  <Grid item>{parseError.calculatedValue && (<Typography color="textSecondary">calc: {parseError.calculatedValue}</Typography>)}</Grid>
              </Grid>)}
            </Typography>
          </CardContent>
      </Card>
    </Grid>
    )
  }
}
const actions = {
  openDialog
}

ParseError.displyName = 'ParseError'

ParseError.propTypes = {
  parseError: T.object.isRequired,
  classes: T.object.isRequired,
  openDialog: T.func.isRequired
}

export default connect(null, actions)(withStyles(styles)(ParseError))
