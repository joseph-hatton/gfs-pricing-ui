import React from 'react'
import { PropTypes as T } from 'prop-types'
import {Field, reduxForm} from 'redux-form'
import Grid from '@material-ui/core/Grid'
import MenuItem from 'material-ui/MenuItem'
import { SelectField, TextField } from 'redux-form-material-ui'
import {connect} from 'react-redux'
import {currentDealSelected, loadAllDeals, resetDealSelected, currentLegalEntitySelected} from '../deals/dealActions'
import {loadAllSpDeals} from '../marketing/marketingActions'
import numeral from 'numeral'
const formId = 'currentDealForm'

export class DealPicker extends React.Component {
    componentDidMount () {
        this.props.resetDealSelected()
        this.props.loadAllDeals()
        this.props.loadAllSpDeals()
      }

  render () {
    const numberFormat = (number) => {
      return number ? numeral(number).format('(0,0)') : ''
    }
    const {nullable, currentDealSelected, deals, currentDeal, currentLegalEntitySelected, legalEntity} = this.props
    return (
    <Grid item xs >
         <Field fullWidth name='id' component={SelectField} onChange={currentDealSelected} floatingLabelText='Please select a Deal'>
        {
          nullable && <MenuItem key="_empty" value={null} primaryText=""/>
        }
        {
          deals.map((item) => (<MenuItem key={item.id} value={item.id} primaryText={item.dealName}/>))
        }
      </Field>
      { currentDeal.legalEntities && (
      <Field fullWidth name='legalEntity.code' component={SelectField} onChange={currentLegalEntitySelected} floatingLabelText='Please select a Legal Entity'>
        {
          nullable && <MenuItem key="_empty" value={null} primaryText=""/>
        }
        {
          currentDeal.legalEntities.map((item) => (<MenuItem key={item.code} value={item.code} primaryText={item.code}/>))
        }
      </Field>
      )}
      {
        legalEntity && (
              <Grid container spacing={16}>
          <Grid item xs={12} sm={6} lg={3}>
          <Field component={TextField} readOnly name="effectiveDate" floatingLabelText="Effective Date"/>
          </Grid>
          <Grid item xs={12} sm={6} lg={3}>
          <Field component={TextField} readOnly name="status" floatingLabelText="Status"/>
          </Grid>
          <Grid item xs={12} sm={6} lg={3}>
          <Field component={TextField} readOnly name="stage" floatingLabelText="Stage"/>
          </Grid>
          <Grid item xs={12} sm={6} lg={3}>
          <Field component={TextField} readOnly name="currency" floatingLabelText="Currency"/>
          </Grid>
          <Grid item xs={12} sm={6} lg={3}>
          <Field component={TextField} readOnly name="productType" floatingLabelText="Product Type"/>
          </Grid>
          <Grid item xs={12} sm={6} lg={3}>
          <Field component={TextField} readOnly name="product1" floatingLabelText="Product 1"/>
          </Grid>
          <Grid item xs={12} sm={6} lg={3}>
          <Field component={TextField} readOnly name="product2" floatingLabelText="Product 2"/>
          </Grid>
          <Grid item xs={12} sm={6} lg={3}>
          <Field component={TextField} readOnly name="product3" floatingLabelText="Product 3"/>
          </Grid>
          <Grid item xs={12} sm={6} lg={3}>
          <Field component={TextField} readOnly name="product4" floatingLabelText="Product 4"/>
          </Grid>
          <Grid item xs={12} sm={6} lg={3}>
          <Field component={TextField} readOnly name="product5" floatingLabelText="Product 5"/>
          </Grid>
          <Grid item xs={12} sm={6} lg={3}>
          <Field component={TextField} readOnly name="nbevDeal" format={numberFormat} floatingLabelText="NBEV"/>
          </Grid>
          <Grid item xs={12} sm={6} lg={3}>
          <Field component={TextField} readOnly fullWidth format={numberFormat}
          name="initCapitalEC" floatingLabelText="Initial Economic Capital"/>
          </Grid>
          <Grid item xs={12} sm={6} lg={3}>
          <Field component={TextField} readOnly name="initCapitalSP" format={numberFormat} floatingLabelText="Initial S&P Capital"/>
          </Grid>
          <Grid item xs={12} sm={6} lg={3}>
          <Field component={TextField} readOnly name="initCapitalLocal" format={numberFormat} floatingLabelText="Initial Local Reg Capital"/>
          </Grid>
          <Grid item xs={12} sm={6} lg={3}>
          <Field component={TextField} readOnly name="FYrev12mth" format={numberFormat} floatingLabelText="FY Revenue 12 mths"/>
          </Grid>
          <Grid item xs={12} sm={6} lg={3}>
          <Field component={TextField} readOnly name="FYrevCalyr" format={numberFormat} floatingLabelText="FY Revenue Curr Cal Yr"/>
          </Grid>
          <Grid item xs={12} sm={6} lg={3}>
          <Field component={TextField} readOnly name="FYptaoi12mth" format={numberFormat} floatingLabelText="FY PTAOI 12 mths"/>
          </Grid>
          <Grid item xs={12} sm={6} lg={3}>
          <Field component={TextField} readOnly name="FYptaoiCalyr" format={numberFormat} floatingLabelText="FY PTAOI Curr Cal Yr"/>
          </Grid>
          <Grid item xs={12}>
          <Field fullWidth component={TextField} readOnly name="dealNotes" floatingLabelText="Deal Notes"/>
          </Grid>
          </Grid>
      )}
    </Grid>
    )
  }
}

DealPicker.propTypes = {
  nullable: T.bool,
  deals: T.array,
  initialValues: T.object,
  currentDealSelected: T.func.isRequired,
  loadAllSpDeals: T.func.isRequired,
  resetDealSelected: T.func.isRequired,
  loadAllDeals: T.func.isRequired,
  currentDeal: T.object,
  legalEntity: T.object,
  currentLegalEntitySelected: T.func.isRequired
}

const actions = {
    currentDealSelected,
    loadAllSpDeals,
    resetDealSelected,
    loadAllDeals,
    currentLegalEntitySelected
  }
DealPicker.displayName = 'DealPicker'

let currentDealForm = reduxForm({
    form: formId,
    enableReinitialize: true
  })(DealPicker)

export const mapStateToProps = ({marketing: {allSpDeals}, deal: {initialValues, currentDeal, legalEntity}}) => {
  const dealSp = allSpDeals.reduce((acc, deal) => {
    acc.push({ id: deal.id, dealName: deal.dealName })
    return acc
    }, [])
    return {
      deals: dealSp,
      initialValues,
      currentDeal,
      legalEntity
    }
  }

export default connect(mapStateToProps, actions)(currentDealForm)
