import moment from 'moment'
import React from 'react'
import { connect } from 'react-redux'
import T from 'prop-types'
import Button from '@material-ui/core/Button'
import { withStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import { resetDealSelected } from '../deals/dealActions'
import { handleReset, saveFinalUpload } from './uploadActions'
import { Field } from 'redux-form'
import { TextField } from 'redux-form-material-ui'
import Grid from '@material-ui/core/Grid'
import numeral from 'numeral'

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: '2em',
        overflowX: 'auto'
    },
    table: {
        padding: 0,
        margin: 0,
        borderSpacing: 0,
        minWidth: 700
    },
    tableHead: {
        background: '#E8E8E8'
    },
    rowDiff: {
        background: '#FEDEDE'
    },
    rowNoDiff: {
        background: 'white'
    }
})

export class SavePreview extends React.Component {
    render () {
        const numberFormat = (number) => {
            return number ? numeral(number).format('(0,0)') : null
          }
        const { classes, excel, currentDeal, saveFinalUpload, resetDealSelected, handleReset, previewValuesNames, newDealNotes } = this.props
        const currentDealArray = Object.entries(currentDeal)
        const valuesFields = ['productType', 'currency', 'nbevDeal', 'initCapitalEC', 'initCapitalSP', 'initCapitalLocal', 'FYrev12mth', 'FYrevCalyr', 'FYptaoi12mth', 'FYptaoiCalyr']
        const table = currentDealArray.reduce((acc, cv) => {
            if (valuesFields.indexOf(cv[0]) >= 0) {
                if (cv[0] === 'effectiveDate') {
                    acc.push({ id: acc.length, name: 'effectiveDate', currentValue: '' + cv[1], excelValue: '' + moment(excel[cv[0]]).format('YYYY-MM-DD'), validation: '' + cv[1] === moment(excel[cv[0]]).format('YYYY-MM-DD') })
                } else {
                    acc.push({ id: acc.length, name: cv[0], currentValue: '' + cv[1], excelValue: '' + excel[cv[0]], validation: cv[1] === excel[cv[0]] })
                }
            }
            return acc
        }, [])
        .map((item) => {
            for (let element of previewValuesNames) {
                if (item.name === element.name) {
                     item.name = element.description
                     item.order = element.order
                     item.format = element.format
                     return item
                }
            }
        })
        .sort((a, b) => {
            return a.order - b.order
          })
        return (
            <div>
                <Grid container spacing={16}>
                <Grid item xs={6}>
                <Field fullWidth component={TextField} readOnly name="dealName" floatingLabelText="Deal Name"/>
                </Grid>
                <Grid item xs={6}>
                <Field fullWidth component={TextField} readOnly name="legalEntity.code" floatingLabelText="Legal Entity"/>
                </Grid>
                <Grid item xs={6}>
                <Field fullWidth component={TextField} readOnly name="dealNotes" floatingLabelText="Previous Deal Notes"/>
                </Grid>
                <Grid item xs={6}>
                <Field fullWidth component={TextField} required name="newDealNotes" floatingLabelText="Please insert current deal notes"/>
                </Grid>
                </Grid>
                <Button onClick={() => { handleReset(); resetDealSelected() }} className={classes.button}>Reset</Button>
                <Button disabled={!newDealNotes} onClick={saveFinalUpload} variant="contained" color="primary" className={`${classes.button} save-button`}>SAVE</Button>
                <Paper className={classes.root}>
                    <Table className={classes.table}>
                        <TableHead className={classes.tableHead}>
                            <TableRow>
                                <TableCell component="th" scope="row">Item</TableCell>
                                <TableCell component="th" scope="row">{currentDeal.legalEntity.code} Version {currentDeal.legalEntity.version}</TableCell>
                                <TableCell component="th" scope="row">{currentDeal.legalEntity.code} Ready to save version {currentDeal.legalEntity.version ? currentDeal.legalEntity.version + 1 : 1}</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {table.map(n => {
                                return (
                                    <TableRow className={!n.validation ? classes.rowDiff : classes.rowNoDiff } key={n.id}>
                                        <TableCell>{n.name}</TableCell>
                                        <TableCell className={`${n.name}_previous`}>{n.format ? numberFormat(n.currentValue) : n.currentValue}</TableCell>
                                        <TableCell className={`${n.name}_new`}>{ n.format ? numberFormat(n.excelValue) : n.excelValue}</TableCell>
                                    </TableRow>
                                )
                            })}
                        </TableBody>
                    </Table>
                </Paper>
            </div>
        )
    }
}

SavePreview.propTypes = {
    excel: T.object,
    currentDeal: T.object,
    saveFinalUpload: T.func,
    getStateConsole: T.func,
    classes: T.object,
    resetDealSelected: T.func,
    handleReset: T.func,
    previewValuesNames: T.array,
    newDealNotes: T.string
}

export const actions = {
    saveFinalUpload,
    resetDealSelected,
    handleReset
}

export const mapStateToProps = ({ form: { UploadSpreadsheetForm: { values: { newDealNotes = null } } }, ref: {allRefs: {previewValuesNames}}, upload: { parsed }, deal: { currentDeal } }) => {
    return {
        excel: parsed,
        currentDeal,
        previewValuesNames,
        newDealNotes
    }
}

export default connect(mapStateToProps, actions)(withStyles(styles)(SavePreview))
