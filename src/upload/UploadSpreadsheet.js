import React from 'react'
import T from 'prop-types'
import { connect } from 'react-redux'
import { reduxForm } from 'redux-form'
import flatten from 'lodash/flatten'
import Button from '@material-ui/core/Button'
import Stepper from '@material-ui/core/Stepper'
import Step from '@material-ui/core/Step'
import StepLabel from '@material-ui/core/StepLabel'
import { loadAllSpDeals, currentSpDealSelected } from '../marketing/marketingActions'
import { resetDealSelected } from '../deals/dealActions'
import DealPicker from './DealPicker'
import SpreadsheetSelection from './SpreadsheetSelection'
import SavePreview from './SavePreview'
import ParseProgress from './ParseProgress'
import { handleNext, handleBack, handleReset } from './uploadActions'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#33c9dc',
      contrastText: '#fff'
    }
  }
})

const formId = 'UploadSpreadsheetForm'

const getSteps = () => {
  return ['Select Deal', 'Select Spreadsheet', 'Validation', 'Save']
}

export class UploadSpreadsheet extends React.Component {
  render () {
    const { hasUploadErrors, activeStep, legalEntity, handleNext, handleReset } = this.props
    const getStepContent = (step) => {
      switch (step) {
        case 0:
          return <DealPicker />
        case 1:
          return <SpreadsheetSelection />
        case 2:
          return <ParseProgress />
        case 3:
          return <div> <SavePreview /> </div>
        default:
          return 'Unknown step'
      }
    }
    const steps = getSteps()
    return (
      <div>
      <MuiThemeProvider theme={theme}>
        <Stepper activeStep={activeStep}>
          {steps.map((label) => {
            const props = {}
            const labelProps = {}
            if (label === 'Validation' && hasUploadErrors) {
              labelProps.error = true
            }
            return (
              <Step key={label} {...props}>
                <StepLabel {...labelProps}>{label}</StepLabel>
              </Step>
            )
          })}
        </Stepper>
        <div>
          <div>
            {(activeStep === 0 || activeStep === 2 || activeStep === 1) && (
              <div>
                <Button disabled={activeStep === 0} onClick={() => { handleReset(); resetDealSelected() }} className="reset-button">Reset</Button>
                <Button disabled={!legalEntity || activeStep === 2 || activeStep === 1} className="next-button" variant="contained" color="primary" onClick={handleNext}>Next</Button>
              </div>
            )}
            <div style={{ padding: 20 }}> {getStepContent(activeStep)} </div>
          </div>
        </div>
        </MuiThemeProvider>
      </div>
    )
  }
}

UploadSpreadsheet.propTypes = {
  hasUploadErrors: T.bool.isRequired,
  deals: T.array,
  loadAllSpDeals: T.func.isRequired,
  resetDealSelected: T.func.isRequired,
  legalEntity: T.object,
  activeStep: T.number,
  handleNext: T.func,
  handleBack: T.func,
  handleReset: T.func
}

const actions = {
  loadAllSpDeals,
  currentSpDealSelected,
  handleNext,
  handleBack,
  handleReset,
  resetDealSelected
}

export const mapStateToProps = ({ upload: { activeStep, steps }, marketing: { allSpDeals = [] }, deal: {initialValues, legalEntity} }) => {
  return {
    hasUploadErrors: flatten(steps.filter(step => step.errors && step.errors.length > 0).map(step => step.errors)).length > 0,
    deals: (allSpDeals.length > 0) ? [{ id: 'empty', dealName: '' }].concat(allSpDeals) : allSpDeals,
    initialValues,
    legalEntity,
    activeStep: activeStep
  }
}
const UploadSpreadsheetForm = reduxForm({
  form: formId,
  enableReinitialize: true
})(UploadSpreadsheet)

export default connect(mapStateToProps, actions)(UploadSpreadsheetForm)
