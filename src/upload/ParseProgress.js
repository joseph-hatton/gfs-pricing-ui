import flatten from 'lodash/flatten'
import React from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import CircularProgress from '@material-ui/core/CircularProgress'
import { withStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import Paper from '@material-ui/core/Paper'
import ParseError from './ParseError'
import ValidOptions from './ValidOptions'

const styles = theme => ({
  paper: {
    padding: '1em'
  },
  progress: {
    textAlign: 'center'
  },
  title: {
    margin: `${theme.spacing.unit * 4}px 0 ${theme.spacing.unit * 2}px`
  },
  show: {
    visibility: 'inherit'
  },
  hide: {
    visibility: 'hidden'
  },
  nested: {
    paddingLeft: theme.spacing.unit * 4
  }
})

export const ParseProgress = ({parsing, errors, classes, currentStep}) => {
  return (
    <Grid container justify="center" spacing={16}>
    {
      (parsing === true) && (<Grid item xs={8}>
        <Paper className={classes.paper}>
          <CircularProgress className={classes.progress} size={50} />
          {(currentStep) ? currentStep.description : ''}
        </Paper>
      </Grid>)
    }
      <Grid item xs={12}>
      {
        (errors.length > 0) && (<div>
          <Typography variant="title" className={classes.title}>
            Validation Errors
          </Typography>

          <Grid container justify="center" spacing={16}>
          {
            errors.map((err, key) => (<ParseError parseError={err} key={key}/>))
          }
          </Grid>
        </div>)
      }
      </Grid>
      <ValidOptions/>
    </Grid>
  )
}

ParseProgress.propTypes = {
  classes: PropTypes.object,
  parsing: PropTypes.bool.isRequired,
  errors: PropTypes.array.isRequired,
  currentStep: PropTypes.object
}

const actions = {
}

export const mapStateToProps = ({upload: {parsing, steps}}) => {
  let currentStep = steps.find(step => !step.continue)
  if (!currentStep) {
    currentStep = steps.find(step => step.status === 'STARTED')
  }
  if (!currentStep) {
    currentStep = steps.find(step => step.status === 'TBD')
  }
  return {
    parsing,
    errors: flatten(steps.map(step => step.errors)).map(err => (typeof err === 'string') ? {message: err} : err),
    currentStep
  }
}

export default connect(mapStateToProps, actions)(withStyles(styles)(ParseProgress))
