import React from 'react'
import T from 'prop-types'
import { connect } from 'react-redux'
import { Field, reduxForm } from 'redux-form'
import FormControl from '@material-ui/core/FormControl'
import Typography from '@material-ui/core/Typography'
import {withStyles} from '@material-ui/core/styles'
import FileInput from '../components/form/FileInput'
import { spreadsheetChanged } from './uploadActions'

const formId = 'SpreadsheetSelectionForm'
const styles = {
  label: {
    paddingBottom: '2em'
  }
}

export class SpreadsheetSelection extends React.Component {
  render () {
    const { classes, spreadsheetChanged } = this.props
    return <div style={{ padding: 20 }}>
      <FormControl fullWidth>
        <Typography variant="subheading" color="inherit" className={classes.label}>
          <label htmlFor="spreadsheet">Upload the GFS Pricing spreadsheet (.xlsx or .xlsm)</label>
        </Typography>
        <Field component={FileInput} name="spreadsheet" onChange={spreadsheetChanged} />
      </FormControl>
    </div>
  }
}

SpreadsheetSelection.propTypes = {
  spreadsheetChanged: T.func.isRequired,
  classes: T.object.isRequired
}

const actions = {
  spreadsheetChanged
}

const SpreadsheetSelectionForm = reduxForm({
  form: formId,
  enableReinitialize: true
})(SpreadsheetSelection)

export default withStyles(styles)(connect(null, actions)(SpreadsheetSelectionForm))
