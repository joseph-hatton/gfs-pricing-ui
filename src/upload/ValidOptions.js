import React from 'react'
import { PropTypes as T } from 'prop-types'
import Button from '@material-ui/core/Button'
import ReduxDialog, { closeDialog } from '../components/redux-dialog/redux-dialog'
import { connect } from 'react-redux'
import { CommonPicker } from '../pickers/CommonPicker'

export class ValidOptions extends React.Component {
  render () {
    const { closeDialog, validOptions } = this.props
    return (
      <ReduxDialog dialogName="validOptions" title="Valid Options" modal={true} autoScrollBodyContent={true}>
        <div>
          <CommonPicker fullWidth multiple name={validOptions.title} floatingLabelText={validOptions.title} options={validOptions.validOptions} />
          <Button color="primary" key="OK" onClick={() => { closeDialog('validOptions') }}>OK</Button>
        </div>
      </ReduxDialog>
    )
  }
}
const actions = {
  closeDialog
}

export const mapStateToProps = ({ dialog: { validOptions = {} } }) => {
  return {
    validOptions
  }
}

ValidOptions.propTypes = {
  closeDialog: T.func.isRequired,
  validOptions: T.object
}

export default connect(mapStateToProps, actions)(ValidOptions)
