import flatten from 'lodash/flatten'
import parseUpload from './parse/parseUpload'
import { apiPath } from '../config'
import http from '../actions/http'

export const getLegalEntityVersionForSave = (legalEntity, legalEntities) => {
  legalEntity.version = legalEntity.version ? legalEntity.version + 1 : 1
  legalEntities.map((item) => item.code === legalEntity.code ? legalEntity : item)
  return {legalEntity, legalEntities}
}

export const spreadsheetChanged = (e, file) => async (dispatch, getState) => {
  dispatch({type: 'SPREADSHEET_PARSE_START', payload: 2})
  const {ref, deal: {currentDeal, legalEntity}} = getState()
  const currentLegalEntitySet = await getLegalEntityVersionForSave(legalEntity, currentDeal.legalEntities)
  currentDeal.legalEntities = currentLegalEntitySet.legalEntities
  currentDeal.legalEntity = currentLegalEntitySet.legalEntity
  const currentProductTypeConfig = currentDeal.productType === 'Asset Intensive' ? ref.allRefs.uploadConfig : ref.allRefs.uploadConfigLongevity
  const {parsed} = await parseUpload(file, currentProductTypeConfig, dispatch, getState, currentDeal)
  const {upload} = getState()
  const steps = upload.steps
  const errors = flatten(steps.map(step => step.errors))
  if (errors && errors.length > 0) {
    dispatch({
      type: 'SPREADSHEET_PARSE_FAIL',
      errors
    })
  } else {
    dispatch({
      type: 'SPREADSHEET_PARSE_SUCCESS',
      payload: parsed
    })
  }
}

export const saveFinalUpload = () => async (dispatch, getState) => {
  const {form: {UploadSpreadsheetForm: {values: {newDealNotes}}}, upload: {parsed}, deal: {currentDeal}} = await getState()
  const url = `${apiPath}/deals`
  dispatch({
    type: 'SAVE_DATA_FINAL',
    payload: http.post(url, {...parsed, id: currentDeal.id, dealNotes: newDealNotes}, {successMessage: 'Deal saved successfully'})
  })
}

export const handleBack = () => (dispatch, getState) => {
  const { upload: { activeStep } } = getState()
  dispatch({
    type: 'HANDLE_BACK',
    payload: {activeStep: activeStep - 1}
  })
}

export const handleReset = () => (dispatch, getState) => {
  dispatch({
    type: 'RESET_STEP',
    payload: {
      activeStep: 0
    }
  })
}

export const handleNext = () => (dispatch, getState) => {
  const { upload: { activeStep } } = getState()
  dispatch({
    type: 'NEXT_STEP',
    payload: {
      activeStep: activeStep + 1
    }
  })
}
