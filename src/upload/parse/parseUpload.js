import parseStepReducers from './steps'
import reduce from 'promise-reduce'

const parseUpload = async (file, uploadConfig, dispatch, getState, currentDeal) => {
  const initial = Promise.resolve({parsed: null, file, currentStep: null, uploadConfig, currentDeal})
  return Promise.resolve(parseStepReducers)
    .then(reduce(async (accPromise, currentReducer) => {
      return new Promise((resolve, reject) => {
         setTimeout(async () => {
          const acc = await accPromise
          if (!acc.currentStep || acc.currentStep.continue) {
            acc.currentStep = {
              id: currentReducer.id,
              completed: false,
              errors: [],
              continue: true
            }
            acc.steps = getState().upload.steps
            dispatch({type: 'SPREADSHEET_STEP_START', stepId: currentReducer.id})
            const result = await currentReducer.run(acc)
            result.currentStep.completed = true

            if ((result.currentStep.errors && result.currentStep.errors.length > 0) || result.currentStep.continue === false) {
              dispatch({type: 'SPREADSHEET_STEP_FAIL', stepId: currentReducer.id, errors: result.currentStep.errors, continue: result.currentStep.continue})
            } else {
              dispatch({type: 'SPREADSHEET_STEP_SUCCESS', stepId: currentReducer.id})
            }
            resolve(result)
          } else {
            resolve(acc)
          }
        }, 500)
      })
  }, initial))
}

export default parseUpload
