import {Decimal} from 'decimal.js'
import getPropertyValue from './getPropertyValue'

const toDecimal = (duration, defaultValue = 0) => propertyName => new Decimal(getPropertyValue(duration, propertyName, defaultValue))

export default toDecimal
