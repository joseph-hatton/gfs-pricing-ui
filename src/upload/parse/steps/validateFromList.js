import {buildScalarError, buildScalarErrorWithInputAndCalculated} from './buildUploadError'

const validateFromList = (uploadConfig, validList, fieldName, parsedField) => {
  let error = null
  if (validList.length > 0) {
    const fieldValue = (typeof parsedField.value === 'string') ? parsedField.value.toLowerCase() : parsedField.value
    const validOption = validList.find((validOption) => (typeof validOption === 'string') ? validOption.toLowerCase() === fieldValue : validOption === fieldValue)
    if (validOption) {
      parsedField.value = validOption
    } else {
      error = buildScalarErrorWithInputAndCalculated(uploadConfig, fieldName, `invalid ${fieldName}`, parsedField, parsedField.value)
      error.validOptions = validList
    }
  } else {
    error = buildScalarError(uploadConfig, fieldName, 'not configured properly', parsedField)
  }
  return error
}

export default validateFromList
