import validateFileTypeStep from './validateFileTypeStep'
import readWorkbookStep from './readWorkbookStep'
import hasScalarSheetStep from './hasScalarSheetStep'
import parseScalarTabStep from './parseScalarTabStep'
import validateScalarFieldsStep from './validateScalarFieldsStep'

import hasVectorSheetWhenFinalStep from './hasVectorSheetWhenFinalStep'
import parseVectorTabStep from './parseVectorTabStep'
import hasVectorsWhenPreviouslyHadVectorsStep from './hasVectorsWhenPreviouslyHadVectorsStep'
import validateVectorCompanyAccountingBasisStep from './validateVectorCompanyAccountingBasisStep'
import validateVectorDataIsNumericStep from './validateVectorDataIsNumericStep'
import validateRequiredVectorsStep from './validateRequiredVectorsStep'
import validateVectorFormulasStep from './validateVectorFormulasStep'
import validateNbevsStep from './validateNbevsStep'

import checkForErrorsToPreventSaveStep from './checkForErrorsToPreventSaveStep'
import buildObjectToSaveStep from './buildObjectToSaveStep'

const steps = [
  validateFileTypeStep,
  readWorkbookStep,
  hasScalarSheetStep,
  parseScalarTabStep,
  validateScalarFieldsStep,

  hasVectorSheetWhenFinalStep,
  parseVectorTabStep,
  hasVectorsWhenPreviouslyHadVectorsStep,
  validateVectorCompanyAccountingBasisStep,
  validateVectorDataIsNumericStep,
   validateRequiredVectorsStep,
   validateVectorFormulasStep,
  validateNbevsStep,
  checkForErrorsToPreventSaveStep,
   buildObjectToSaveStep
]

export default steps
