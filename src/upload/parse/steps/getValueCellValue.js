import moment from 'moment'
const dateAsUtc = (sheet, cellLocation) => {
  const momentDate = moment((sheet[cellLocation]).w)
  return (momentDate.isValid()) ? momentDate.toDate() : (sheet[cellLocation]).v
}
const justV = (sheet, cellLocation) => (sheet[cellLocation]).v

const valueFormatters = {
  effectiveDate: dateAsUtc,
  expCloseDate: dateAsUtc,
  pricingDate: dateAsUtc
}

const getValueCellValue = (sheet, cellLocation, propertyName = null, defaultValue = null) => {
  if ((sheet[cellLocation])) {
    return (propertyName && valueFormatters[propertyName]) ? valueFormatters[propertyName](sheet, cellLocation) : justV(sheet, cellLocation)
  }
  return defaultValue
}

export default getValueCellValue
