import validateScalarEntries from './validateScalarEntries'
import validateScalarCompanyAccountingBases from './validateScalarCompanyAccountingBases'

const validateScalarFieldsStep = {
    id: 'validateScalarFieldsStep',
    description: 'Validating scalar data',
    run: (acc) => {
        const dealStatus = acc.currentDeal.nextStatus ? acc.currentDeal.nextStatus : acc.currentDeal.status
        const errors = validateScalarEntries(acc.uploadConfig, acc.parsed, dealStatus, acc.uploadConfig.scalar.fields.dealDetail, ['companyAccountingBases', 'vectors'])

      validateScalarCompanyAccountingBases(acc.uploadConfig, acc.parsed, dealStatus, errors)

      if (errors && errors.length > 0) {
        errors.forEach(err => acc.currentStep.errors.push(err))
        acc.currentStep.continue = true
      }
      return acc
    }
  }

export default validateScalarFieldsStep
