import flatten from 'lodash/flatten'
import omit from 'lodash/omit'
import xlsx from 'xlsx'
import getValueCellValue from './getValueCellValue'
import applyDurationDates from './applyDurationDates'

export const getFieldsWithCompanyAndAccBasis = (vectorSheet) => (fieldCell) => {
    const basisCell = fieldCell[0].replace(2, 1)
    const columnLetter = fieldCell[0].replace(2, '')
    const nullAccBasis = !getValueCellValue(vectorSheet, basisCell)
    return { fieldName: fieldCell[1].v, accountingBasis: getValueCellValue(vectorSheet, basisCell), letter: columnLetter, nullAccBasis }
  }

export const getFieldsWithValues = (vectorSheet) => (fieldCompanyAccBasis) => {
    const range = xlsx.utils.decode_range(vectorSheet['!ref'])
    const durationRowNumbers = Array.from({length: range.e.r - 2}, (v, k) => k + 4)
    const durationValues = durationRowNumbers.map(durationRowNumber => {
      const duration = getValueCellValue(vectorSheet, 'A' + durationRowNumber)
      const value = {
        rowNumber: durationRowNumber,
        column: fieldCompanyAccBasis.letter,
        value: getValueCellValue(vectorSheet, fieldCompanyAccBasis.letter + durationRowNumber)
      }
      return { accountingBasis: fieldCompanyAccBasis.accountingBasis, fieldName: fieldCompanyAccBasis.fieldName, duration, value }
    })
    return durationValues
  }

export const createObjectFormat = (acc, data) => {
  let accountingBasis = acc.vectors.find(item => item.accountingBasis === data.accountingBasis)
  if (!accountingBasis) {
    accountingBasis = { accountingBasis: data.accountingBasis, durations: [] }
    acc.vectors.push(accountingBasis)
  }
  let durationValue = null

  if (data.duration === null) {
    durationValue = accountingBasis.durations.find(duration => duration.rowNumber === data.value.rowNumber)
  } else {
    durationValue = accountingBasis.durations.find(duration => duration.duration === data.duration)
  }
  if (!durationValue) {
    durationValue = {duration: data.duration, rowNumber: data.value.rowNumber}
    accountingBasis.durations.push(durationValue)
  }
  durationValue[data.fieldName] = data.value
  return acc
}

export default (wb, uploadConfig, vectorInit, vectorInitCount, vectorUlt, effectiveDate, legalEntity) => {
  const vectorSheet = wb.Sheets[uploadConfig.vector.tabName]
  const errAccBasis = Object.entries(vectorSheet)
  .filter((entry) => RegExp(`.*[A-Z]2$`).test(entry[0]))
  .filter((entry) => entry[1].v !== 'Field Name:')
  .map(getFieldsWithCompanyAndAccBasis(vectorSheet))
  .filter((entry) => entry.nullAccBasis)

  const fieldsWithValues = Object.entries(vectorSheet)
    .filter((entry) => RegExp(`.*[A-Z]2$`).test(entry[0]))
    .filter((entry) => entry[1].v !== 'Field Name:')
    .map(getFieldsWithCompanyAndAccBasis(vectorSheet))
    .map(getFieldsWithValues(vectorSheet))

  const flattened = flatten(fieldsWithValues)
  const {vectors} = flattened
    .reduce(createObjectFormat, {vectors: []})
  vectors.forEach(vector => {
    const durationsWithoutRowNumber = vector.durations.map(duration => omit(duration, 'rowNumber'))
    vector.durations = durationsWithoutRowNumber
    vector.legalEntity = legalEntity
  })
  applyDurationDates(vectors, vectorInit, vectorInitCount, vectorUlt, effectiveDate)
  return { vectors, errAccBasis }
}
