import flatten from 'lodash/flatten'
import moment from 'moment'

const checkForErrorsToPreventSaveStep = {
  id: 'checkForErrorsToPreventSaveStep',
  description: 'Preventing Save if errors were found',
  run: (acc) => {
    if (acc) {
      acc.parsed = { ...acc.parsed,
        // sharepoint
        dealName: acc.currentDeal.dealName,
        effectiveDate: moment(acc.currentDeal.effectiveDate).format('YYYY-MM-DD'),
        pricingDate: moment(acc.currentDeal.pricingDate).format('YYYY-MM-DD'),
        stage: acc.currentDeal.stage,
        legalEntities: (acc.currentDeal.legalEntities),
        legalEntity: acc.currentDeal.legalEntity
      }
      const errors = flatten(acc.steps.map(step => step.errors))
      if (errors && errors.length > 0) {
        acc.currentStep.continue = false
      }
    }

    return acc
  }
}

export default checkForErrorsToPreventSaveStep
