import omit from 'lodash/omit'
import isNumber from 'lodash/isNumber'
import isInteger from 'lodash/isInteger'
import flatten from 'lodash/flatten'
import buildUploadError, {buildVectorErrorWithInputAndCalculated} from './buildUploadError'

const errorMessage = 'not the expected type (number)'
const validateAllValuesNumeric = (uploadConfig, vectors) =>
  vectors.reduce((errors, vector) => {
    flatten(vector.durations.map(duration =>
      Object.entries(omit(duration, 'duration', 'durationDate', 'monthlyDuration'))
        .filter(entry => entry[1] && entry[1].value !== false && entry[1].value !== null && !isNumber(entry[1].value))
        .map(invalidEntry => {
          return buildVectorErrorWithInputAndCalculated(uploadConfig, vector.company, vector.accountingBasis, invalidEntry[0], errorMessage, invalidEntry[1], duration.duration, invalidEntry[1].value)
        })
    ))
    .filter(err => err !== null)
    .forEach(err => errors.push(err))
    return errors
  }, [])

const validateAllDurationsNumeric = (uploadConfig, vectors) =>
  ((vectors && vectors.length > 0) ? vectors[0].durations : [])
  .filter(duration => !(isInteger(duration.duration) && duration.duration >= 0))
  .map(invalidDuration => {
    let hasRowNumber = Object.values(invalidDuration).find(value => value && value.rowNumber)

    return buildUploadError({
      tab: uploadConfig.vector.tabName,
      field: 'duration',
      message: errorMessage,
      duration: invalidDuration.duration || 'empty',
      parsedField: {
        rowNumber: (hasRowNumber) ? hasRowNumber.rowNumber : null,
        column: 'A'
      }
    })
  })

const validateVectorDataIsNumericStep = {
  id: 'validateVectorDataIsNumericStep',
  description: 'Validating vector data is numeric',
  run: (acc) => {
    if (acc.hasVectors) {
      const pushErrors = err => acc.currentStep.errors.push(err)
      const durationErrors = validateAllDurationsNumeric(acc.uploadConfig, acc.parsed.vectors)
      durationErrors.forEach(pushErrors)
      const errors = validateAllValuesNumeric(acc.uploadConfig, acc.parsed.vectors)
      errors.forEach(pushErrors)
    }
    return acc
  }
}

export default validateVectorDataIsNumericStep
