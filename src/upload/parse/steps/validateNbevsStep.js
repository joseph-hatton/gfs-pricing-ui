import getPropertyValue from './getPropertyValue'
import buildUploadError from './buildUploadError'
import {Decimal} from 'decimal.js'
import areDecimalsWithin from './areDecimalsWithin'
import durationToDecimal from './durationToDecimal'
import isNumber from 'lodash/isNumber'

const notNull = something => something !== null

const buildNbevError = (uploadConfig, accountingBasis, field, message, parsedField, inputValue, calculatedValue) =>
  buildUploadError({
    tab: uploadConfig.scalar.tabName,
    field,
    message,
    parsedField,
    accountingBasis,
    inputValue,
    calculatedValue
  })

const validateNbevWithRate = (uploadConfig, vector) => (nbev) => {
  const template = uploadConfig.scalar.template
  const ratePlusOne = (new Decimal(1)).add(nbev.rateValue)
  const negative12 = new Decimal(-12)
  console.log(`validating nbev for ${vector.accountingBasis} ${nbev.rateProperty}: ${nbev.rateValue}, now do it for each duration.... `)

  const sumOfPresentValueOfDistEarned = vector.durations
    .map(duration => {
      const discountFactor = ratePlusOne.pow((new Decimal(duration.monthlyDuration).div(negative12)))
      const distributableEarning = durationToDecimal(duration)('distEarn')
      const presentValueOfDistEarn = discountFactor.mul(distributableEarning)
      return presentValueOfDistEarn
    })
    .reduce((acc, aPresentValueOfDistEarn) => acc.add(aPresentValueOfDistEarn), new Decimal(0))
  const areDecimalsClose = areDecimalsWithin(sumOfPresentValueOfDistEarned, nbev.nbevValue, 1.0000)
  // we are skiping the validation until Zig has the final
  if (template === 'Longevity') { return null } else {
  return (!areDecimalsClose)
    ? buildNbevError(uploadConfig, vector.accountingBasis, nbev.nbevProperty, 'NBEV input value does not equal PV of Distributable Earnings', nbev.nbevField, nbev.nbevValue.toDP(0).toNumber(), sumOfPresentValueOfDistEarned.toDP(0).toNumber())
    : null
  }
}

const validateNbevVectors = (uploadConfig, parsed) =>
  parsed.vectors.reduce((errors, vector) => {
    const nbevsToTry = ['1', '2', '3']
    const {accountingBasis} = vector
    const companyAccountBasis = parsed.companyAccountingBases.find(cab => cab.accountingBasis === accountingBasis)
    if (companyAccountBasis) {
      nbevsToTry.map(nbevNumber => {
        const rateProperty = `nbevrate${nbevNumber}`
        const nbevProperty = `nbev${nbevNumber}`
        const rateValue = getPropertyValue(companyAccountBasis, rateProperty)
        const nbevValue = getPropertyValue(companyAccountBasis, nbevProperty)
        const nbevField = companyAccountBasis[nbevProperty]
        return (rateValue && nbevValue) ? {rateProperty, nbevProperty, rateValue: new Decimal(rateValue), nbevValue: new Decimal(nbevValue), nbevField} : null
      })
      .filter(notNull)
      .map(validateNbevWithRate(uploadConfig, vector))
      .filter(notNull)
      .forEach(err => errors.push(err))
    }
    return errors
  }, [])

const validateGaapEcNbevIsEightPercentNbevDeal = (uploadConfig, parsed) => {
  let nbevDealError = null
  const nbevDeal = getPropertyValue(parsed, 'nbevDeal')
  const gaapEcVector = parsed.vectors.find(vector => vector.accountingBasis === 'GAAP EC')
  if (nbevDeal && gaapEcVector) {
    nbevDealError = validateNbevWithRate(uploadConfig, gaapEcVector)({rateProperty: null, nbevProperty: 'nbevDeal', rateValue: new Decimal(0.08), nbevValue: new Decimal(nbevDeal), nbevField: parsed['nbevDeal']})
  }
  return nbevDealError
}

const validateNbevs = (uploadConfig, parsed) => {
  const vectorWithNoMonthlyDuration = parsed.vectors.find(vector => vector.durations.find(duration => !isNumber(duration.duration)))
  if (vectorWithNoMonthlyDuration) {
    return [buildUploadError({
      tab: uploadConfig.vector.tabName,
      field: 'duration',
      message: 'Unable to validate NBEVs with invalid durations'
    })]
  } else {
    const errors = validateNbevVectors(uploadConfig, parsed)

    const nbevDealError = validateGaapEcNbevIsEightPercentNbevDeal(uploadConfig, parsed)
    if (nbevDealError) {
      errors.push(nbevDealError)
    }
    return errors
  }
}

const validate = {
  id: 'validateNbevsStep',
  description: 'Validating NBEVs',
  run: (acc) => {
    if (acc.hasVectors) {
      const errors = validateNbevs(acc.uploadConfig, acc.parsed)
      errors.forEach(err => acc.currentStep.errors.push(err))
    }
    return acc
  }
}

export default validate
