import isString from 'lodash/isString'
import isDate from 'lodash/isDate'
import isInteger from 'lodash/isInteger'
import isNumber from 'lodash/isNumber'

const normalizers = {
  date: (v) => isDate(v) ? v : v,
  string: (v) => isString(v) ? v : v.toString(),
  integer: (v) => isInteger(v) ? v : v * 1,
  number: (v) => isNumber(v) ? v : v * 1
}

const normalizeValue = (configFields, propertyName, value) => {
  let newValue = value
  const configField = configFields[propertyName]
  if (configField && value !== null) {
    const normalizer = normalizers[configField.type]
    if (normalizer) {
      newValue = normalizer(value)
    }
  }
  return newValue
}

export default normalizeValue
