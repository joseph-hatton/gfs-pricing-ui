import get from 'lodash/get'
import getPropertyValue from './getPropertyValue'
import buildUploadError from './buildUploadError'

const hasVectorSheetWhenFinalStep = {
  id: 'hasVectorSheetWhenFinalStep',
  description: 'Validating the workbook has Vector Sheet when the deal status is "Final"',
  run: (acc) => {
    const sheetName = get(acc, 'uploadConfig.vector.tabName', null)

    const dealStatus = getPropertyValue(acc.parsed, 'status')
    acc.hasVectors = acc.wb.SheetNames.includes(sheetName)
    if (!acc.hasVectors && dealStatus === 'Final') {
      acc.currentStep.errors.push(buildUploadError({
        tab: sheetName,
        field: `Workbook Sheet ${sheetName}`,
        message: 'is required when Deal "status" is Final'
      }))
      acc.currentStep.continue = false
    }
    return acc
  }
}
export default hasVectorSheetWhenFinalStep
