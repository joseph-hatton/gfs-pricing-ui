import isString from 'lodash/isString'

const validateRequired = (field) => (field && field.value && isString(field.value) && field.value.trim().length === 0) || (!field || !field.value)

export default validateRequired
