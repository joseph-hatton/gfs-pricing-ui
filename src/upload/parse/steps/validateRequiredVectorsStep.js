import getPropertyValue from './getPropertyValue'
import {buildVectorError} from './buildUploadError'

const validateRequiredVectors = (uploadConfig, dealStatus, vectors) =>
  Object.entries(uploadConfig.vector.fields.vector).filter(entry => entry[1] && (entry[1].required || entry[1].requiredWithStatus === dealStatus)).map(entry => entry[0])
  .map(requiredField => {
    const nonNullDuration = vectors.find(vector => vector.durations.find(duration => duration[requiredField] !== null && duration[requiredField] !== undefined))
    return (!nonNullDuration) ? buildVectorError(uploadConfig, null, null, requiredField, 'required field not found') : null
  })
  .filter(err => err !== null)

const validateRequiredVectorsStep = {
  id: 'validateRequiredVectorsStep',
  description: 'Validating required vector data',
  run: (acc) => {
    if (acc.hasVectors) {
      const dealStatus = getPropertyValue(acc.parsed, 'status')

      validateRequiredVectors(acc.uploadConfig, dealStatus, acc.parsed.vectors).forEach(err => acc.currentStep.errors.push(err))
    }
    return acc
  }
}

export default validateRequiredVectorsStep
