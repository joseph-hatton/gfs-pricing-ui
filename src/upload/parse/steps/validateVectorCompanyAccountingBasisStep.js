import omit from 'lodash/omit'
import buildUploadError from './buildUploadError'
import getPropertyValue from './getPropertyValue'
import validateFromList from './validateFromList'

const validateAccountinBasis = (uploadConfig, accountingBasis) => {
  const err = accountingBasis ? validateFromList(uploadConfig, uploadConfig.scalar.fields.companyAccountingBasis.accountingBasis.valid, 'accountingBasis', {value: accountingBasis}) : null
  if (err) err.tab = uploadConfig.vector.tabName
  return err
}

const validateCompanyAccountingBases = (uploadConfig, vectors, scalarCompanyAccountingBases) =>
  vectors.reduce((errors, vector) => {
    const accountingBasisError = validateAccountinBasis(uploadConfig, vector.accountingBasis)
    Object.values(omit(vector.durations[0], 'duration', 'monthlyDuration', 'durationDate'))
    .filter(duration => duration).filter(duration => duration.column).map(duration => {
      const metadata = {}
      if (duration.column) metadata.col = duration.column
      return metadata
    })
    .forEach(metadataForError => {
      if (accountingBasisError) {
        errors.push({
          ...accountingBasisError,
          ...metadataForError,
          row: '1'
        })
      }
      if (!accountingBasisError) {
        const field = `${vector.accountingBasis}`
        const match = scalarCompanyAccountingBases.find(scab => vector.accountingBasis === scab.accountingBasis)
        if (!match && vector.accountingBasis) {
          const error = buildUploadError({
            tab: uploadConfig.scalar.tabName,
            field,
            accountingBasis: vector.accountingBasis,
            message: 'Vector Accounting Basis not found in scalars',
            parsedField: {
              column: metadataForError.col
            }
          })
          errors.push(error)
        }
      }
    })
    return errors
  }, [])

const scalarCompanyAccountingBasesExistOnVectorWhenFinal = (acc) => {
  const errors = []
  const dealStatus = getPropertyValue(acc.parsed, 'status')
  if (dealStatus === 'Final') {
    acc.parsed.companyAccountingBases.filter(scab => {
      const accountingBasisError = validateAccountinBasis(acc.uploadConfig, scab.accountingBasis)
      return !accountingBasisError
    })
    .forEach(scab => {
      const field = `${scab.accountingBasis}`
      const match = acc.parsed.vectors.find(vector => vector.accountingBasis === scab.accountingBasis)
      if (!match) {
        const metadataRows = Object.values(scab).filter(value => value && value.rowNumber).map(({ rowNumber }) => ({ rowNumber }))
        const error = buildUploadError({
          tab: acc.uploadConfig.vector.tabName,
          field,
          accountingBasis: scab.accountingBasis,
          message: 'Scalar Legal Entity/Accounting Basis not found in vectors'
        })
        metadataRows.forEach(({ rowNumber }) => {
          errors.push({
            ...error,
            row: rowNumber
          })
        })
      }
    })
  }
  return errors
}

const validateVectorCompanyAccountingBasisStep = {
  id: 'validateVectorCompanyAccountingBasisStep',
  description: 'Validating vector Accounting Basis',
  run: (acc) => {
    if (acc.hasVectors) {
      const pushError = err => acc.currentStep.errors.push(err)
      const invalidErrors = validateCompanyAccountingBases(acc.uploadConfig, acc.parsed.vectors, acc.parsed.companyAccountingBases)
      invalidErrors.forEach(pushError)

      const scalarErrors = scalarCompanyAccountingBasesExistOnVectorWhenFinal(acc)
      scalarErrors.forEach(pushError)
    }
    return acc
  }
}

export default validateVectorCompanyAccountingBasisStep
