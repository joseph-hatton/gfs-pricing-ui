import isString from 'lodash/isString'
import omit from 'lodash/omit'
import isDate from 'lodash/isDate'
import isInteger from 'lodash/isInteger'
import isFinite from 'lodash/isFinite'
import {buildScalarError, buildScalarErrorWithInputAndCalculated} from './buildUploadError'
import validateFromList from './validateFromList'
import validateRequired from './validateRequired'

const validators = {
  date: isDate,
  string: isString,
  integer: isInteger,
  number: isFinite
}

const validateRequiredAndType = (uploadConfig, objectToValidate, dealStatus, fieldsToOmit) => (currentErrors, fieldEntry) => {
  const fieldName = fieldEntry[0]
  const {type, required, requiredWithStatus, valid} = fieldEntry[1]
  let parsedField = objectToValidate[fieldName]
  const isRequired = required || requiredWithStatus === dealStatus
  const isRequiredAndMissing = isRequired && validateRequired(parsedField)
  if (fieldsToOmit && fieldsToOmit.indexOf(fieldName) >= 0) {
    return currentErrors
  }
  if ((fieldName === 'accountingBasis') && typeof parsedField === 'string') {
      parsedField = {value: objectToValidate[fieldName]}
  }

  if (parsedField && parsedField.value !== null) {
    if (valid) {
      const error = validateFromList(uploadConfig, valid, fieldName, parsedField)
      if (error) {
        currentErrors.push(error)
      }
    } else {
      const validator = validators[type]
      if (validator) {
        if (!validator(parsedField.value) && fieldName !== 'status') {
          currentErrors.push(buildScalarErrorWithInputAndCalculated(uploadConfig, fieldName, `invalid data type: ${type} expected`, parsedField, parsedField.originalValue))
        }
      } else {
        currentErrors.push(buildScalarErrorWithInputAndCalculated(uploadConfig, fieldName, `unable to be validated due to an unknown type: ${type}`, parsedField, parsedField.value))
      }
    }
  }
  if (isRequired) {
    if (parsedField) {
      if (isRequiredAndMissing) {
        currentErrors.push(buildScalarError(uploadConfig, fieldName, 'no value provided for required field', parsedField))
      }
    } else {
      currentErrors.push(buildScalarError(uploadConfig, fieldName, 'required field not found'))
    }
  }

  return currentErrors
}

const validateUnexpectedFields = (uploadConfig, objectToValidate, fieldsToOmit, configFields) => {
  const allButOmitted = Object.entries(omit(objectToValidate, fieldsToOmit))
  return allButOmitted.reduce((currentErrors, oneNotOmitted) => {
    const isValidField = configFields[oneNotOmitted[0]]
    if (!isValidField) {
      currentErrors.push(buildScalarError(uploadConfig, oneNotOmitted[0], 'invalid field name', oneNotOmitted[1]))
    }
    return currentErrors
  }, [])
}

const validateScalarEntries = (uploadConfig, objectToValidate, dealStatus, configFields, fieldsToOmit) => {
  const fieldEntries = Object.entries(configFields)
  const errors = fieldEntries.reduce(validateRequiredAndType(uploadConfig, objectToValidate, dealStatus, fieldsToOmit), [])

  validateUnexpectedFields(uploadConfig, objectToValidate, fieldsToOmit, configFields).forEach(err => errors.push(err))

  return errors
}

export default validateScalarEntries
