import validateScalarEntries from './validateScalarEntries'
import validateFromList from './validateFromList'

const validateCompanyAccountingBases = (uploadConfig, parsed, dealStatus, errors) => {
  parsed.companyAccountingBases.reduce((currentErrors, parsedScalar) => {
    const {accountingBasis} = parsedScalar

    const accountingBasisError = validateFromList(uploadConfig, uploadConfig.scalar.fields.companyAccountingBasis.accountingBasis.valid, 'accountingBasis', {value: accountingBasis})
    if (accountingBasisError) {
      Object.values(parsedScalar).filter(s => s).filter(s => s.rowNumber || s.column).map(vmd => {
        const metadata = {}
        if (vmd.rowNumber) metadata.row = vmd.rowNumber
        if (vmd.column) metadata.col = vmd.column
        return metadata
      })
      .forEach(metadataForError => {
        if (accountingBasisError) {
          currentErrors.push({
            ...accountingBasisError,
            ...metadataForError,
            col: uploadConfig.scalar.accountingBasisColumn
          })
        }
      })
    }
    const scalarEntryErrors = validateScalarEntries(uploadConfig, parsedScalar, dealStatus, uploadConfig.scalar.fields.companyAccountingBasis, ['accountingBasis'])
    scalarEntryErrors.forEach(err => currentErrors.push({...err, accountingBasis}))

    return currentErrors
  }, errors)

  return errors
}

export default validateCompanyAccountingBases
