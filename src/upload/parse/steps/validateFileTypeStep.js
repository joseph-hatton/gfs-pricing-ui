const validateFileTypeStep = {
  id: 'validateFileTypeStep',
  description: 'Validating the uploaded file is either type .xlsm or .xlsx',
  run: (acc) => {
    const name = acc.file.name.toLowerCase()
    if (!name.endsWith('.xlsm') && !name.endsWith('.xlsx')) {
      acc.currentStep.errors.push('Uploaded file must be of type .xlsm or .xlsx')
      acc.currentStep.continue = false
    }
    return acc
  }
}
export default validateFileTypeStep
