export default (parsed, property, defaultValue = null) => (parsed && parsed[property] && parsed[property].value) ? parsed[property].value : defaultValue
