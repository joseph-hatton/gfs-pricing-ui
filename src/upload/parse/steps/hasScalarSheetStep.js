const hasScalarSheetStep = {
  id: 'hasScalarSheetStep',
  description: 'Validating the workbook has the Scalar sheet',
  run: (acc) => {
    const sheetName = acc.uploadConfig.scalar.tabName
    if (!acc.wb.SheetNames.includes(sheetName)) {
      acc.currentStep.errors.push(`${sheetName} sheet not found`)
      acc.currentStep.continue = false
    }
    return acc
  }
}
export default hasScalarSheetStep
