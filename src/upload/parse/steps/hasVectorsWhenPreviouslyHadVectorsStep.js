import get from 'lodash/get'
import buildUploadError from './buildUploadError'

const hasVectorsWhenPreviouslyHadVectorsStep = {
  id: 'hasVectorsWhenPreviouslyHadVectorsStep',
  description: 'Validating there are vectors if the previous upload had vectors',
  run: (acc) => {
    const sheetName = get(acc, 'uploadConfig.vector.tabName', null)
    const {hasVectors, currentDeal, parsed} = acc

    if (currentDeal.hasVectors) {
      const firstVectorWithDurations = parsed.vectors ? parsed.vectors.find(vector => vector.durations.length > 0) : null
      if (!hasVectors || !firstVectorWithDurations) {
        acc.currentStep.errors.push(buildUploadError({
          tab: sheetName,
          field: 'Vectors',
          message: 'are required when the previous revision had Vector data'
        }))
      }
    }
    return acc
  }
}
export default hasVectorsWhenPreviouslyHadVectorsStep
