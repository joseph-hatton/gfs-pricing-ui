const buildError = ({tab, field, company, accountingBasis, duration, message, validOptions, inputValue, calculatedValue, parsedField}) => {
  const errObj = {
    tab,
    field,
    message
  }
  if (validOptions) errObj.validOptions = validOptions
  if (company) errObj.company = company
  if (accountingBasis) errObj.accountingBasis = accountingBasis
  if (duration) errObj.duration = duration
  if (inputValue !== undefined) errObj.inputValue = inputValue
  if (calculatedValue) errObj.calculatedValue = calculatedValue

  if (parsedField && parsedField.rowNumber) {
    errObj.row = parsedField.rowNumber
  }
  if (parsedField && parsedField.column) {
    errObj.col = parsedField.column
  }
  return errObj
}

export const buildScalarError = (uploadConfig, field, message, parsedField) =>
  buildError({tab: uploadConfig.scalar.tabName, field, message, parsedField})

export const buildScalarErrorWithInputAndCalculated = (uploadConfig, field, message, parsedField, inputValue, calculatedValue) =>
  buildError({tab: uploadConfig.scalar.tabName, field, message, parsedField, inputValue, calculatedValue})

export const buildVectorError = (uploadConfig, company, accountingBasis, field, message, parsedField, duration = null) =>
  buildError({
    tab: uploadConfig.vector.tabName,
    field,
    message,
    parsedField,
    company,
    accountingBasis,
    duration
  })

export const buildVectorErrorWithInputAndCalculated = (uploadConfig, company, accountingBasis, field, message, parsedField, duration = null, inputValue, calculatedValue) =>
  buildError({
    tab: uploadConfig.vector.tabName,
    field,
    message,
    parsedField,
    company,
    accountingBasis,
    duration,
    inputValue,
    calculatedValue
  })

export default buildError
