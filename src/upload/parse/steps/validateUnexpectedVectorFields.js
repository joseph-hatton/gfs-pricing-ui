import omit from 'lodash/omit'
import {buildVectorError} from './buildUploadError'

const validateUnexpectedFields = (uploadConfig, vector, duration) => {
  const configFields = uploadConfig.vector.fields.vector
  const allButOmitted = Object.entries(omit(duration, ['duration', 'monthlyDuration', 'durationDate']))

  return allButOmitted.reduce((currentErrors, oneNotOmitted) => {
    const isValidField = configFields[oneNotOmitted[0]]
    if (!isValidField) {
      currentErrors.push(buildVectorError(uploadConfig, vector.company, vector.accountingBasis, oneNotOmitted[0], 'invalid field name', {column: oneNotOmitted[1].column, rowNumber: '3'}))
    }
    return currentErrors
  }, [])
}

export default validateUnexpectedFields
