import {Decimal} from 'decimal.js'
import durationToDecimal from './durationToDecimal'
import {buildVectorErrorWithInputAndCalculated} from './buildUploadError'

const validateIncreaseOfCapital = (uploadConfig, vector, duration, previousDuration) => {
  const cap0 = durationToDecimal(duration)('capital')
  const cap1 = (previousDuration) ? durationToDecimal(previousDuration)('capital') : new Decimal(0)
  const target = durationToDecimal(duration)('incrCap')
  const calculated = cap0.minus(cap1)
  const matches = calculated.minus(target).absoluteValue().lessThanOrEqualTo(new Decimal(0.0001))
  if (!matches) {
    return buildVectorErrorWithInputAndCalculated(uploadConfig, vector.company, vector.accountingBasis, 'incrCap', 'Increase of Capital input value does not equal calculated value', duration.incrCap, duration.duration, target.toDP(0).toNumber(), calculated.toDP(0).toNumber())
  }
  return null
}

export default validateIncreaseOfCapital
