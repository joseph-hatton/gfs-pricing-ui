import moment from 'moment'

const calculateMonthlyDuration = (vectorInit, vectorInitCount, vectorUlt, duration) => {
  let monthlyDuration
  if (!vectorInitCount || duration <= vectorInitCount) {
      monthlyDuration = duration * vectorInit
  } else if (vectorInitCount && duration > vectorInitCount) {
      monthlyDuration = (vectorInit * vectorInitCount) + (duration - vectorInitCount) * vectorUlt
  }
  return monthlyDuration
}

const applyDurationDates = (vectors = [], vectorInit = 1, vectorInitCount, vectorUlt = 1, effectiveDate) => {
  if (effectiveDate && vectors && vectors.length > 0) {
    vectors.forEach(vector =>
      vector.durations.forEach(duration => {
          const monthlyDuration = calculateMonthlyDuration(vectorInit, vectorInitCount, vectorUlt, duration.duration)
          duration.monthlyDuration = monthlyDuration
          duration.durationDate = moment(effectiveDate).add(monthlyDuration, 'months').startOf('month').toDate()
          duration.durationDate = moment(duration.durationDate).format('YYYY-MM-DD')
      }))
  }

  return null
}

export default applyDurationDates
