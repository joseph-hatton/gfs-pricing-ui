import toPairs from 'lodash/toPairs'
import fromPairs from 'lodash/fromPairs'
import isArray from 'lodash/isArray'

const stripRowColumnMetadata = obj =>
    fromPairs(toPairs(obj)
    .map(pair => {
        if (isArray(pair[1])) {
            const newValues = pair[1].map(arrayObj => stripRowColumnMetadata(arrayObj))
            return [pair[0], newValues]
        } else if (pair[1] && pair[1].value !== undefined) {
            return [pair[0], pair[1].value]
        } else {
            return pair
        }
    }))

const buildParsedToSave = (acc) => stripRowColumnMetadata(acc.parsed)

const buildObjectToSaveStep = {
    id: 'buildObjectToSaveStep',
    description: 'Building object to save',
    run: (acc) => {
        acc.parsed = buildParsedToSave(acc)
        return acc
    }
}

export default buildObjectToSaveStep
