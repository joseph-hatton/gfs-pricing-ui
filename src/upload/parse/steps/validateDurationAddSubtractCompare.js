import getPropertyValue from './getPropertyValue'
import {Decimal} from 'decimal.js'
import {buildVectorErrorWithInputAndCalculated} from './buildUploadError'
import areDecimalsWithin from './areDecimalsWithin'

const validateDurationAddSubtractCompare = (uploadConfig, vector, duration, addColumns, subtractedColumns, compareColumn, errorMessage) => {
  const toValue = addColumn => new Decimal(getPropertyValue(duration, addColumn, 0))
  const sumReducer = (accumulator, currentValue) => accumulator.plus(currentValue)
  const toAdd = addColumns.map(toValue).reduce(sumReducer, new Decimal(0))
  const toSubtract = subtractedColumns.map(toValue).reduce(sumReducer, new Decimal(0))
  const target = new Decimal(getPropertyValue(duration, compareColumn, 0))
  const calculated = toAdd.minus(toSubtract)
  const matches = areDecimalsWithin(calculated, target, 1)
  if (!matches) {
    return buildVectorErrorWithInputAndCalculated(uploadConfig, vector.company, vector.accountingBasis, compareColumn, errorMessage, duration[compareColumn], duration.duration, target.toDP(0).toNumber(), calculated.toDP(0).toNumber())
  }
  return null
}

export default validateDurationAddSubtractCompare
