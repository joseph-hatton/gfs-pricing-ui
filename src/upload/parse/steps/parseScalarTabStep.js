import getValueCellValue from './getValueCellValue'
import normalizeValue from './normalizeValue'
import _ from 'lodash'

const getaccBasisNumber = (scalarSheet) => {
  let row
  Object.keys(scalarSheet).forEach((key) => {
    if (scalarSheet[key].v === 'acctBasis') row = key.replace('B', '')
  })
  return row
}

const fieldNameCellsToAcctBasisProperties = (scalarSheet, uploadConfig) => (fieldNameCell) => {
  const accBasisRow = getaccBasisNumber(scalarSheet)
  const row = fieldNameCell[0].replace(/[A-Za-z]/, '')
  const valueCellLocation = `${uploadConfig.scalar.valueColumn}${row}`
  const propertyName = fieldNameCell[1].v
  const accountingBasisColumns = ['D', 'E', 'F']
  let accBassis = []
  let properties

 if (row < accBasisRow * 1) {
  properties = {
   propertyName,
   value: getValueCellValue(scalarSheet, valueCellLocation, propertyName),
   accountingBasis: null,
   rowNumber: row * 1,
   columnLetter: `${uploadConfig.scalar.accountingBasisColumn}`
 }
 return properties
} else {
  for (let element of accountingBasisColumns) {
     properties = {
      propertyName,
      value: getValueCellValue(scalarSheet, `${element}${row}`, propertyName),
      accountingBasis: getValueCellValue(scalarSheet, `${element}${accBasisRow}`, 'accountingBasis'),
      rowNumber: row * 1,
      columnLetter: element
    }
    accBassis.push(properties)
  }
  return accBassis
}
}

const onlyFieldNameColumns = (fieldNameColumn) => (entry) => {
return RegExp(`^${fieldNameColumn}\\d+`).test(entry[0]) && entry[0] !== `${fieldNameColumn}1`
}
const addAccountingBasis = (acc, accountingBasis, propertyName, value, rowNumber) => {
  let matchingScalar = acc.companyAccountingBases.find((scalar) => scalar.accountingBasis === accountingBasis)
  if (!matchingScalar) {
    matchingScalar = {accountingBasis}
    acc.companyAccountingBases.push(matchingScalar)
  }
  matchingScalar[propertyName] = {value, rowNumber}
}

const reduceCompanyAcctBasisPropertiesToADeal = (uploadConfig) => (acc, {propertyName, value, accountingBasis, rowNumber, columnLetter}) => {
  if (propertyName && uploadConfig.scalar.columnsToIgnore.includes(propertyName.toLowerCase())) {
    console.log({propertyName, value}, 'ignoring a scalar property due to config')
  } else if (!accountingBasis) {
    const newValue = normalizeValue(uploadConfig.scalar.fields.dealDetail, propertyName, value)
    acc[propertyName] = {value: newValue, rowNumber, columnLetter, originalValue: value}
  } else {
    const newValue = normalizeValue(uploadConfig.scalar.fields.companyAccountingBasis, propertyName, value)
    addAccountingBasis(acc, accountingBasis, propertyName, newValue, rowNumber)
  }
  return acc
}

const parseScalar = (wb, uploadConfig) => {
  const initial = { companyAccountingBases: [] }
  const scalarSheet = wb.Sheets[uploadConfig.scalar.tabName]
  return _(Object.entries(scalarSheet))
    .filter(onlyFieldNameColumns(uploadConfig.scalar.fieldNameColumn))
    .map(fieldNameCellsToAcctBasisProperties(scalarSheet, uploadConfig))
    .flatten()
    .value()
    .reduce(reduceCompanyAcctBasisPropertiesToADeal(uploadConfig), initial)
}

const parseScalarTabStep = {
  id: 'parseScalarTabStep',
  description: 'Parsing the scalar tab',
  run: (acc) => {
    acc.parsed = parseScalar(acc.wb, acc.uploadConfig)
    acc.parsed.status = acc.currentDeal.nextStatus ? acc.currentDeal.nextStatus : acc.currentDeal.status
    return acc
  }
}

export default parseScalarTabStep
