import parseVector from './parseVector'
import getPropertyValue from './getPropertyValue'

const parseVectorTabStep = {
    id: 'parseVectorTabStep',
    description: 'Parsing the vector tab',
    run: (acc) => {
      if (acc.hasVectors) {
        const vectorInit = getPropertyValue(acc.parsed, 'vectorInit')
        const vectorInitCount = getPropertyValue(acc.parsed, 'vectorInitCount')
        const vectorUlt = getPropertyValue(acc.parsed, 'vectorUlt')
        const stepParseVector = parseVector(acc.wb, acc.uploadConfig, vectorInit, vectorInitCount, vectorUlt, acc.currentDeal.effectiveDate, acc.currentDeal.legalEntity)
        acc.parsed.vectors = stepParseVector.vectors
        acc.currentStep.errors = stepParseVector.errAccBasis ? stepParseVector.errAccBasis.reduce((errorList, item) => {
          errorList.push(
            {validOptions: acc.uploadConfig.scalar.fields.companyAccountingBasis.accountingBasis.valid, tab: 'UploadVector', field: 'accountingBasis', message: 'invalid accountingBasis', inputValue: null, row: 1, col: item.letter})
          return errorList
        }, []) : []
      }
      return acc
    }
  }
export default parseVectorTabStep
