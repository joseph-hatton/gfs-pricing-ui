import {Decimal} from 'decimal.js'

export default (decimalA, decimalB, within = 1) => {
  return decimalA
  .minus(decimalB)
  .absoluteValue()
  .lessThanOrEqualTo(new Decimal(within))
}
