import XLSX from 'xlsx'

const readWorkbookStep = {
    id: 'readWorkbookStep',
    description: 'Reading the Excel workbook',
    run: async (acc) => {
      return new Promise((resolve, reject) => {
        var reader = new FileReader() // eslint-disable-line no-undef
        reader.onload = (upload) => {
          const result = atob(upload.target.result.substr(upload.target.result.indexOf(',') + 1, upload.target.result.length)) // eslint-disable-line no-undef
          try {
            acc.wb = XLSX.read(result, {type: 'binary', cellDates: false})
          } catch (err) {
            acc.currentStep.errors.push('Unable to parse the uploaded file.  Check contents.')
            acc.currentStep.continue = false
          }
          resolve(acc)
        }
        reader.readAsDataURL(acc.file)
      })
    }
  }

  export default readWorkbookStep
