import flatten from 'lodash/flatten'
import validateDurationAddSubtractCompare from './validateDurationAddSubtractCompare'
import validateIncreaseOfCapital from './validateIncreaseOfCapital'
import validateUnexpectedVectorFields from './validateUnexpectedVectorFields'

const validateVectorFormulas = (uploadConfig, vectors) =>
  vectors.reduce((errors, vector) => {
    flatten(vector.durations.map((duration, index) => {
      const durationErrors = []
      if (index === 0) {
        validateUnexpectedVectorFields(uploadConfig, vector, duration).forEach(err => durationErrors.push(err))
      }
      durationErrors.push(validateIncreaseOfCapital(uploadConfig, vector, duration, (index > 0) ? vector.durations[index - 1] : null))
      const { gaap = null, localUs = null } = (uploadConfig && uploadConfig.vector && uploadConfig.vector.validation && uploadConfig.vector.validation.addSubtractCompare) ? uploadConfig.vector.validation.addSubtractCompare : { gaap: null, localUs: null }
      let validationsToUse = null
      if (vector.accountingBasis && vector.accountingBasis.toLowerCase().startsWith('gaap')) {
        validationsToUse = gaap
      } else if (vector.accountingBasis && vector.accountingBasis.toLowerCase() === 'local reg') {
        validationsToUse = localUs
      } else {
        if (index === 0) {
          console.warn(`accountingBasis ${vector.company}/${vector.accountingBasis} is not configured with formula validations`)
        }
      }
      if (validationsToUse) {
        Object.values(validationsToUse).map(validation => validateDurationAddSubtractCompare(uploadConfig, vector, duration, validation.add, validation.subtract, validation.target, validation.error)).map(err => durationErrors.push(err))
      }

      return durationErrors
    }))
    .filter(err => err !== null)
    .forEach(err => errors.push(err))
    return errors
  }, [])

const validateVectorFormulasStep = {
  id: 'validateVectorFormulasStep',
  description: 'Validating vector formulas',
  run: (acc) => {
    if (acc.hasVectors) {
      const errors = validateVectorFormulas(acc.uploadConfig, acc.parsed.vectors)
      errors.forEach(err => acc.currentStep.errors.push(err))
    }
    return acc
  }
}

export default validateVectorFormulasStep
