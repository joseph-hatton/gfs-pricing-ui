import React from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

import Grid from '@material-ui/core/Grid'
import Paper from '@material-ui/core/Paper'
import ReactJson from 'react-json-view'

const styles = theme => ({
  card: {
    minWidth: 275
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)'
  },
  title: {
    marginBottom: 16,
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  }
})

export const ParsedPreview = ({dealName, parsed, classes}) => {
  if (!parsed) {
    return (<div></div>)
  } else {
    return (
      <Grid container className={classes.root} spacing={16}>
        <Grid item xs={12}>
          <Paper className={classes.control}>
            <div> {dealName}</div>
            <div> <ReactJson src={parsed} collapsed /></div>
          </Paper>
        </Grid>
      </Grid>
    )
  }
}

ParsedPreview.propTypes = {
  classes: PropTypes.object.isRequired
}

const actions = {
}

const mapStateToProps = ({upload: {dealName, parsed}}) => {
  return {
    dealName,
    parsed
  }
}

export default connect(mapStateToProps, actions)(withStyles(styles)(ParsedPreview))
