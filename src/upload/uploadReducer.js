import cloneDeep from 'lodash/cloneDeep'
import parseStepReducers from './parse/steps/index'

const initialState = {
  parsing: false,
  steps: parseStepReducers.map(parseStep => ({id: parseStep.id, description: parseStep.description, errors: [], status: 'TBD', continue: true})),
  activeStep: 0
}

const replaceStepStatus = (stateSteps, stepId, newStatus) => {
  let steps = cloneDeep(stateSteps)
  steps.find((step) => step.id === stepId).status = newStatus
  return steps
}

export default (state = initialState, action = {type: null}) => {
  let step, steps
  switch (action.type) {
    case 'SPREADSHEET_PARSE_START':
    return {
      ...initialState,
      steps: cloneDeep(initialState.steps),
      parsing: true,
      parsed: null,
      activeStep: action.payload
    }
    case 'SPREADSHEET_PARSE_SUCCESS':
    return {
      ...state,
      parsing: false,
      parsed: action.payload,
      activeStep: 3
    }
    case 'SAVE_DATA_FINAL_FULFILLED':
    return {
      ...state,
      activeStep: 0
    }
    case 'SPREADSHEET_PARSE_FAIL':
    return {
      ...state,
      errors: action.errors,
      parsing: false,
      activeStep: 2
    }
    case 'SPREADSHEET_STEP_START':
      return {
        ...state,
        steps: replaceStepStatus(state.steps, action.stepId, 'STARTED')
      }
    case 'SPREADSHEET_STEP_SUCCESS':
      return {
        ...state,
        steps: replaceStepStatus(state.steps, action.stepId, 'SUCCESS')
      }
    case 'SPREADSHEET_STEP_FAIL':
      steps = cloneDeep(state.steps)
      step = steps.find((step) => step.id === action.stepId)
      step.status = 'FAIL'
      step.errors = action.errors
      step.continue = action.continue
      return {
        ...state,
        steps
      }
    case 'NEXT_STEP':
    return {
      ...state,
      activeStep: action.payload.activeStep
    }
    case 'HANDLE_BACK':
    return {
      ...state,
      activeStep: action.payload.activeStep
    }
    case 'RESET_STEP':
    return {
      ...initialState
    }
    default:
      return state
  }
}
