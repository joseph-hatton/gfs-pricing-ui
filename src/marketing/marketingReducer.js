const initialState = {
  allSpDeals: [],
  currentDeal: {}
}

export default (state = initialState, action = {type: null}) => {
  switch (action.type) {
    case 'LOAD_ALL_SP_DEALS_FULFILLED':
    return {
      ...state,
      allSpDeals: action.payload
    }
    case 'CURRENT_SP_DEAL':
    return {
      ...state,
      currentDeal: action.payload,
      initialValues: action.payload
    }
    case 'RESET_SP_DEAL':
    return {
      ...state,
      currentDeal: {},
      initialValues: null
    }
    default:
    return state
  }
}
