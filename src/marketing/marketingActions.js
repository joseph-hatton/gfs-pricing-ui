import { apiPath } from '../config'
import http from '../actions/http'

export const currentSpDealSelected = (event, value) => (dispatch, getState) => {
    const { marketing: { allSpDeals } } = getState()
    const matchingDeal = allSpDeals.find(deal => deal.id === value)
    // if (matchingDeal) {
    //  matchingDeal.effectiveDate = moment(matchingDeal.effectiveDate).utc().format('YYYY-MM-DD')
    // }
    dispatch({
      type: 'CURRENT_SP_DEAL',
      payload: matchingDeal
    })
  }
export const resetSpDealSelected = () => (dispatch) => {
    dispatch({
        type: 'RESET_SP_DEAL'
    })
}
export const loadAllSpDeals = () => {
    return {
      type: 'LOAD_ALL_SP_DEALS',
      payload: http.get(`${apiPath}/marketing/bulk`)
    }
}
