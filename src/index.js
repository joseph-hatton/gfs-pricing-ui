import React from 'react'
import { Provider } from 'react-redux'
import ReactDOM from 'react-dom'
import App from './components/core/App'
import store from './store'
import {BrowserRouter as Router} from 'react-router-dom'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import theme from './components/core/theme'
import invokeStartupActions from './actions/invokeStartupActions'
import styles from './styles/styles.scss' // eslint-disable-line no-unused-vars

invokeStartupActions(store.dispatch, store.getState).then(() => {
  ReactDOM.render(
    <Provider store={store}>
      <Router>
        <MuiThemeProvider muiTheme={theme}>
          <App/>
        </MuiThemeProvider>
      </Router>
    </Provider>,
    document.getElementById('root')
  )
})
