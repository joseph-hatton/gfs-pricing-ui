import React from 'react'
import { PropTypes as T } from 'prop-types'
import MaterialSelectField from '../components/form/MaterialSelectField'
import _ from 'lodash'
import Grid from '@material-ui/core/Grid'

export class CommonPicker extends React.Component {
  render () {
    return (
    <Grid item xs >
      <MaterialSelectField {..._.omit(this.props)} name={this.props.name} options={this.props.options}/>
    </Grid>)
  }
}

CommonPicker.propTypes = {
  name: T.string,
  options: T.array
}

CommonPicker.displayName = 'CommonPicker'

export default CommonPicker
