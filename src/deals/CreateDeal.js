import React from 'react'
import T from 'prop-types'
import Grid from '@material-ui/core/Grid'
import { connect } from 'react-redux'
import RaisedButton from 'material-ui/RaisedButton'
import { Field, reduxForm, reset } from 'redux-form'
import { TextField, DatePicker } from 'redux-form-material-ui'
import { CommonPicker } from '../pickers/CommonPicker'
import { saveDeal } from '../deals/dealActions'
import { Required, dateFormat, toNumber } from '../components/form/Validators'
export class CreateDeal extends React.Component {
  render () {
    const { handleSubmit, saveDeal, businessTree, productTypeOptions, probOfCloseOptions, stageOptions, statusOptions, countryOptions } = this.props
    if (!productTypeOptions) {
      return <div>loading ...</div>
    } else {
      return (
        <form id='createDealForm' onSubmit={handleSubmit(saveDeal)}>
          <div style={{ padding: 20 }}>
            <Grid container spacing={0}>
              <Grid item xs={12} md={12}>
                <Field name="dealName" component={TextField} floatingLabelText="Deal Name" validate={Required} />
              </Grid>
              <CommonPicker name="productType" floatingLabelText="Product Type" options={productTypeOptions} validate={Required} />
              <CommonPicker name="legalEntity" floatingLabelText="Legal Entity" options={businessTree['legalEntity']} validate={Required} />
              <CommonPicker name="currency" floatingLabelText="Currency" options={businessTree['currency']} validate={Required} />
              <CommonPicker name="office" floatingLabelText="Office" options={businessTree['office']} validate={Required} />
              <CommonPicker name="geoSegment" floatingLabelText="Geo Segment" options={businessTree['geoSegment']} validate={Required} />
              <CommonPicker name="country" floatingLabelText="Country" options={countryOptions} validate={Required} />
              <CommonPicker name="status" floatingLabelText="Status" options={statusOptions} validate={Required} />
              <CommonPicker name="stage" floatingLabelText="Stage" options={stageOptions} validate={Required} />
              <Grid item xs>
                <Field autoOk name="effectiveDate" component={DatePicker} floatingLabelText="* Effective Date" formatDate={dateFormat} validate={Required} />
              </Grid>
              <Grid item xs>
                <Field autoOk name="expCloseDate" component={DatePicker} floatingLabelText="Target Close Date" formatDate={dateFormat} validate={Required} />
              </Grid>
              <CommonPicker name="probClose" floatingLabelText="Probability of Close" options={probOfCloseOptions} validate={Required} />
              <Grid item xs>
                <Field name="competitors" component={TextField} parse={toNumber} floatingLabelText="Number Of Competitors" validate={Required} />
              </Grid>
              <Grid item xs>
                <Field name="nbevDeal" component={TextField} parse={toNumber} floatingLabelText="NBEV" validate={Required} />
              </Grid>
              <Grid item xs>
                <Field name="levDeal" component={TextField} parse={toNumber} floatingLabelText="LEV" validate={Required} />
              </Grid>
              <Grid item xs>
                <Field name="FYrev12mth" component={TextField} parse={toNumber} floatingLabelText="FY Revenue (12 months)" validate={Required} />
              </Grid>
              <Grid item xs>
                <Field name="FYptaoi12mth" component={TextField} parse={toNumber} floatingLabelText="FY PTAOI (12 months)" validate={Required} />
              </Grid>
              <Grid item xs>
                <Field name="FYrevCalyr" component={TextField} parse={toNumber} floatingLabelText="FY Revenue (curr Cal Yr)" validate={Required} />
              </Grid>
              <Grid item xs>
                <Field name="FYptaoiCalyr" component={TextField} parse={toNumber} floatingLabelText="FY PTAOI (curr Cal Yr)" validate={Required} />
              </Grid>
            </Grid>
          </div>
          <div style={{ padding: 40 }}>
            <Grid container spacing={0}>
              <RaisedButton
                key="Save"
                label="Save"
                primary={true}
                style={{ marginRight: 20 }}
                type="submit"
                form="createDealForm"
              />
              <RaisedButton
                key="Reset"
                label="Reset"
                onClick={this.props.resetDealForm}
              />
            </Grid>
          </div>
        </form>
      )
    }
  }
}

CreateDeal.propTypes = {
  handleSubmit: T.func.isRequired,
  businessTree: T.object,
  productTypeOptions: T.array,
  probOfCloseOptions: T.array,
  countryOptions: T.array,
  stageOptions: T.array,
  statusOptions: T.array,
  saveDeal: T.func.isRequired,
  resetDealForm: T.func.isRequired
}

export const actions = {
  saveDeal,
  resetDealForm: () => (dispatch) => dispatch(reset('createDealForm'))
}

const afterSubmit = (result, dispatch) =>
  dispatch(reset('createDealForm'))

export const createDealForm = reduxForm({
  form: 'createDealForm',
  onSubmitSuccess: afterSubmit
})(CreateDeal)

export const mapStateToProps = ({ref: {businessTree, productTypes, countries, dealStatuses, stages, probCloses}}) => {
  return {
    businessTree,
    productTypeOptions: productTypes,
    probOfCloseOptions: probCloses,
    stageOptions: stages,
    statusOptions: dealStatuses,
    countryOptions: countries
  }
}

export default connect(mapStateToProps, actions)(createDealForm)
