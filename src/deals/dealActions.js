import { apiPath } from '../config'
import http from '../actions/http'
import moment from 'moment'

export const loadAllDeals = () => {
  return {
    type: 'LOAD_ALL_DEALS',
    payload: http.get(`${apiPath}/deals`)
  }
}

export const convertLegalEntityObject = (legalEntities) => {
  console.log(legalEntities[0].code)
return legalEntities[0].code ? legalEntities : legalEntities.reduce((acc, item) => {
  acc.push({code: item, version: 0})
  return acc
}, [])
}

export const currentDealSelected = (event, value) => (dispatch, getState) => {
  const { marketing: {allSpDeals} } = getState()
  const currentDealSelected = allSpDeals.find(deal => deal.id === value)
  currentDealSelected.legalEntities = convertLegalEntityObject(currentDealSelected.legalEntities)
  dispatch({
    type: 'CURRENT_DEAL',
    payload: currentDealSelected
  })
}

export const currentLegalEntitySelected = (event, value) => (dispatch, getState) => {
  const { deal: { allDeals, currentDeal }, marketing: {allSpDeals}, ref: {stages} } = getState()
  const currentDealSelected = allSpDeals.find(deal => deal.id === currentDeal.id)
  let matchingDeal = allDeals.find(deal => deal.id === currentDeal.id && deal.legalEntity.code === value)
  const dealStatus = stages.find((item) => item.stage === currentDealSelected.stage).status
  if (matchingDeal && matchingDeal.effectiveDate) {
  matchingDeal.effectiveDate = moment(matchingDeal.effectiveDate).format('YYYY-MM-DD')
  if (dealStatus !== matchingDeal.status) {
  matchingDeal.nextStatus = dealStatus
  }
  } else {
    matchingDeal =
    { id: currentDealSelected.id,
      dealName: currentDealSelected.dealName,
      effectiveDate: currentDealSelected.effectiveDate = moment(currentDealSelected.effectiveDate).format('YYYY-MM-DD'),
      stage: currentDealSelected.stage,
      legalEntity: currentDealSelected.legalEntities.find(item => item.code === value),
      legalEntities: currentDeal.legalEntities,
      status: dealStatus,
      productType: currentDealSelected.productType,
      nbevDeal: currentDealSelected.fmnbev
    }
  }
  dispatch({
    type: 'CURRENT_LEGAL_ENTITY',
    payload: matchingDeal
  })
}

export const resetDealSelected = () => (dispatch) => {
  dispatch({
    type: 'RESET_DEAL'
  })
}

export const saveDeal = () => (dispatch, getState) => {
  const { form: { createDealForm } } = getState()
  const url = `${apiPath}/deals`
  dispatch({
    type: 'SAVE_DEAL',
    payload: http.post(url, createDealForm.values, { successMessage: 'Deal saved successfully' })
  })
}
