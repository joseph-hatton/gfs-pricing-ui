const initialState = {
  allDeals: [],
  legalEntity: null,
  currentDeal: {
    legalEntities: []
  }
}

export default (state = initialState, action = {type: null}) => {
  switch (action.type) {
    case 'LOAD_ALL_DEALS_FULFILLED':
    return {
      ...state,
      allDeals: action.payload
    }
    case 'SAVE_DEAL_FULFILLED':
    return {
      ...state
    }
    case 'CURRENT_DEAL':
    return {
      ...state,
      currentDeal: action.payload,
      initialValues: action.payload,
      legalEntity: null
    }
    case 'CURRENT_LEGAL_ENTITY':
    return {
      ...state,
      initialValues: action.payload,
      currentDeal: action.payload,
      legalEntity: action.payload.legalEntity
    }
    case 'RESET_DEAL':
    return {
      ...state,
      ...initialState,
      initialValues: null,
      currentDeal: {
        legalEntities: []
      }
    }
    default:
    return state
  }
}
