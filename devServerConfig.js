const config = {
  authUrl: process.env.DEV_AUTH_URL || 'https://stsdev.rgare.com/secureauth31/OidcToken.aspx',
  clientId: process.env.DEV_CLIENT_ID || 'b9776287477e4f6e85f7625a4ca9580d',
  clientSecret: process.env.DEV_CLIENT_SECRET,
  userId: process.env.USERNAME || process.env.USER,
  password: process.env.DEV_PASSWORD
}

const fail = () => {
  console.error(`dev server requires credentials for user ${config.userId}!`)
  console.error('set the DEV_CLIENT_SECRET and DEV_PASSWORD environment variables')
  process.exit(1)
}

Object.keys(config).forEach(property => {
  if (!config[property]) {
    fail()
  }
})

module.exports = config
