import Enzyme from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

if (!global.enzymeConfigured) {
  Enzyme.configure({adapter: new Adapter()})
  global.enzymeConfigured = true
}
