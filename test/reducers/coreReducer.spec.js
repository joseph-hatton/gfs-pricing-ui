import coreReducer from '../../src/reducers/coreReducer'

describe('coreReducer.js', () => {
  it('should set initial state', () => {
    expect(coreReducer()).toEqual({leftDrawerOpen: true})
  })

  describe('set left drawer open', () => {
    it('true', () => {
      expect(coreReducer({}, {type: 'SET_LEFT_DRAWER_OPEN', open: true})).toEqual({leftDrawerOpen: true})
    })

    it('false', () => {
      expect(coreReducer({}, {type: 'SET_LEFT_DRAWER_OPEN', open: false})).toEqual({leftDrawerOpen: false})
    })
  })
})
