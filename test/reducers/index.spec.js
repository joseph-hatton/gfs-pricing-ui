import renderer from 'react-test-renderer' // eslint-disable-line no-unused-vars
import {combineReducers} from 'redux'

import reducer from '../../src/reducers/index' // eslint-disable-line no-unused-vars

jest.mock('redux', () => ({
  combineReducers: jest.fn()
}))

jest.mock('redux-form', () => ({
  reducer: 'form'
}))

jest.mock('../../src/reducers/coreReducer', () => 'coreReducer')
jest.mock('../../src/reducers/currentUserReducer', () => 'currentUserReducer')
jest.mock('../../src/upload/uploadReducer.js', () => 'uploadReducer')
jest.mock('../../src/deals/dealReducer.js', () => 'dealReducer')
jest.mock('../../src/marketing/marketingReducer.js', () => 'marketingReducer')
jest.mock('../../src/components/redux-dialog', () => ({dialogReducer: 'dialogReducer'}))
jest.mock('../../src/refs/refReducer', () => 'refReducer')
jest.mock('../../src/reports/reportReducer.js', () => 'reportReducer')
describe('Reducers index.js', () => {
  it('should setup correctly', () => {
    expect(combineReducers).toHaveBeenCalledWith({
      form: 'form',
      core: 'coreReducer',
      currentUser: 'currentUserReducer',
      upload: 'uploadReducer',
      deal: 'dealReducer',
      marketing: 'marketingReducer',
      dialog: 'dialogReducer',
      ref: 'refReducer',
      report: 'reportReducer'
    })
  })
})
