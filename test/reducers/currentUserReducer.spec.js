
import currentUserReducer from '../../src/reducers/currentUserReducer'

describe('currentUserReducer', () => {
  xit('should setup correctly', () => {
    expect(currentUserReducer(null, {})).toEqual({
      id: 'unknown',
      name: 'unknown'
    })
  })

  it('set current user', () => {
    expect(currentUserReducer(null, {type: 'SET_CURRENT_USER', user: {id: 'sjoe', name: 'joe'}})).toEqual({
      id: 'sjoe',
      name: 'joe'
    })
  })
})
