import React from 'react' // eslint-disable-line no-unused-vars
import renderer from 'react-test-renderer' // eslint-disable-line no-unused-vars
import mockComponent from './MockComponent'
import ReactDOM from 'react-dom'

import index from '../src/index' // eslint-disable-line no-unused-vars

jest.mock('react-dom', () => ({
  render: jest.fn()
}))
// jest.mock('react-redux', () => ({
//   Provider: mockComponent('Provider')
// }))
// jest.mock('react-router-dom', () => ({
//   BrowserRouter: mockComponent('Router')
// }))
// jest.mock('../src/components/request/invokeStartupActions', () => 'invokeStartupActions')
jest.mock('../src/store', () => 'store')
jest.mock('../src/styles/styles.scss', () => 'store')
jest.mock('../src/components/core/theme', () => 'theme')
jest.mock('../src/components/core/App', () => mockComponent('App'))
jest.mock('material-ui/styles/MuiThemeProvider', () => 'MuiThemeProvider')

describe('index', () => {
  it('should render correctly', () => {
    expect(ReactDOM.render).toMatchSnapshot()
//    expect(renderer.create(ReactDOM.render.mock.calls[0][0]).toJSON()).toMatchSnapshot()
    expect(true)
  })
})
