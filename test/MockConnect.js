export default function () {
  const connectChildFn = jest.fn(component => component)
  return {
    connect: jest.fn(() => connectChildFn),
    connectChildFn
  }
}
