import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../../MockComponent'
import mockConnect from '../../MockConnect'

import App, {mapStateToProps} from '../../../src/components/core/App'

jest.mock('react-redux', () => mockConnect())
jest.mock('react-router-dom', () => ({
  withRouter: (comp) => comp
}))
jest.mock('../../../src/components/core/TopBar', () => mockComponent('TopBar'))
jest.mock('../../../src/components/core/LeftSideDrawer', () => mockComponent('LeftSideDrawer'))
jest.mock('../../../src/components/core/Routes', () => mockComponent('Routes'))
jest.mock('../../../src/components/redux-dialog', () => ({
  Snackbar: mockComponent('Snackbar'),
  ConfirmationAlert: mockComponent('ConfirmationAlert'),
  MaterialSpinner: mockComponent('MaterialSpinner'),
  openConfirmationAlert: jest.fn()
}))

describe('App', () => {
  let props
  beforeEach(() => {
    props = {
      leftDrawerOpen: false
    }
  })
  it('should render correctly', () => {
    const comp = renderer.create(<App {...props}/>)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('should render correctly when leftDrawerOpen is true', () => {
    props.leftDrawerOpen = true
    const comp = renderer.create(<App {...props}/>)
    expect(comp.toJSON()).toMatchSnapshot()
  })
  it('mapStateToProps should map correctly', () => {
    expect(mapStateToProps({
      core: {leftDrawerOpen: 'leftDrawerOpen'}
    })).toEqual({
      leftDrawerOpen: 'leftDrawerOpen'
    })
  })
})
