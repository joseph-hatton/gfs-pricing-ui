import {toggleLeftDrawer} from '../../../src/components/core/coreActions'

describe('coreActions', () => {
  let dispatch, getState, state
  beforeEach(() => {
    dispatch = jest.fn()
    state = {
      core: {leftDrawerOpen: false}
    }
    getState = jest.fn().mockReturnValue(state)
  })
  it('toggleLeftDrawer should set left drawer open to true if false', () => {
    toggleLeftDrawer()(dispatch, getState)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'SET_LEFT_DRAWER_OPEN',
      open: true
    })
  })
  it('toggleLeftDrawer should set left drawer open to false if true', () => {
    state.core.leftDrawerOpen = true
    toggleLeftDrawer()(dispatch, getState)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'SET_LEFT_DRAWER_OPEN',
      open: false
    })
  })
})
