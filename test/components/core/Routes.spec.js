import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../../MockComponent'

import Routes from '../../../src/components/core/Routes'

jest.mock('../../../src/components/core/Home', () => 'Home')
jest.mock('../../../src/upload/UploadSpreadsheet', () => 'Upload')
jest.mock('../../../src/deals/CreateDeal', () => 'CreateDeal')
jest.mock('react-router-dom', () => ({
  Route: mockComponent('Route')
}))

describe('LeftSideDrawer', () => {
  it('should render correctly', () => {
    expect(renderer.create(<Routes/>).toJSON()).toMatchSnapshot()
  })
})
