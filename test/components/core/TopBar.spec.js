import React from 'react'
import renderer from 'react-test-renderer'
import mockConnect from '../../MockConnect'
import mockComponent from '../../MockComponent'
import {connect} from 'react-redux'

import TopBar from '../../../src/components/core/TopBar'

jest.mock('react-redux', () => mockConnect())
jest.mock('material-ui/AppBar', () => mockComponent('AppBar'))
jest.mock('../../../src/components/core/coreActions', () => ({
  toggleLeftDrawer: 'toggleLeftDrawer'
}))

describe('TopBar', () => {
  let props
  beforeEach(() => {
    props = {
      toggleLeftDrawer: 'toggleLeftDrawer',
      style: 'style'
    }
  })
  it('should render correctly', () => {
    expect(renderer.create(<TopBar {...props}/>).toJSON()).toMatchSnapshot()
  })
  it('action should be passed into connect', () => {
    expect(connect).toHaveBeenCalledTimes(1)
    expect(connect.mock.calls[0][1]).toEqual({
      toggleLeftDrawer: 'toggleLeftDrawer'
    })
  })
})
