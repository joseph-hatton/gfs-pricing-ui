import React from 'react'
import omit from 'lodash/omit'
import {shallow} from 'enzyme'
import LogOutIFrame from '../../../src/components/LogOutIFrame'

describe('LogOutIFrame', () => {
  let props, component

  beforeEach(() => {
    props = {
      url: 'someurl',
      height: 300,
      width: 400
    }
    component = shallow(<LogOutIFrame {...props}/>, {disableLifecycleMethods: true})
  })

  afterEach(() => {
    delete global.location
  })

  it('renders iframe', () =>
    expect(omit(component.find('iframe').props(), 'children')).toEqual({
      src: props.url,
      height: props.height,
      width: props.width
    })
  )

  it('reloads on timer event', done => {
    const reload = jest.fn()
    expect(global.location).toEqual(undefined)
    global.location = {reload}
    component.setProps({reloadDelay: 0})
    component.instance().componentDidMount()

    setTimeout(() => {
      expect(reload).toHaveBeenCalled
      done()
    }, 1)
  })
})
