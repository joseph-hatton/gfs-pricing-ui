import React from 'react'
import renderer from 'react-test-renderer'
import mockConnect from '../../MockConnect'
import mockComponent from '../../MockComponent'

import LeftSideDrawer from '../../../src/components/core/LeftSideDrawer'
import {toggleLeftDrawer} from '../../../src/components/core/coreActions'

jest.mock('react-redux', () => mockConnect())
jest.mock('material-ui/Drawer', () => mockComponent('Drawer'))
jest.mock('material-ui/MenuItem', () => mockComponent('MenuItem'))
jest.mock('react-router-dom', () => ({
  Link: mockComponent('Link')
}))

describe('LeftSideDrawer', () => {
  let dispatch, getState, state
  beforeEach(() => {
    dispatch = jest.fn()
    state = {
      core: {leftDrawerOpen: false},
      currentUser: {fullName: 'some name'}
    }
    getState = jest.fn().mockReturnValue(state)
  })

  it('toggleLeftDrawer should set left drawer open to true if false', () => {
    toggleLeftDrawer()(dispatch, getState)
    expect(dispatch).toHaveBeenCalledWith({
      type: 'SET_LEFT_DRAWER_OPEN',
      open: true
    })
  })

  it('should render correctly', () => {
    expect(renderer.create(<LeftSideDrawer {...state}/>).toJSON()).toMatchSnapshot()
  })
})
