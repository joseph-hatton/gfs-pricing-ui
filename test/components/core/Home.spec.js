import React from 'react'
import renderer from 'react-test-renderer'

import Home from '../../../src/components/core/Home'

describe('Home', () => {
  it('should render correctly', () => {
    expect(renderer.create(<Home/>).toJSON()).toMatchSnapshot()
  })
})
