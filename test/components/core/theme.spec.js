import getMuiTheme from 'material-ui/styles/getMuiTheme'

import theme from '../../../src/components/core/theme' // eslint-disable-line no-unused-vars

jest.mock('material-ui/styles/getMuiTheme', () => jest.fn())

describe('theme', () => {
  it('should call getMuiTheme and match snapshot', () => {
    expect(getMuiTheme).toHaveBeenCalled()
    expect(getMuiTheme.mock.calls[0][0]).toMatchSnapshot()
  })
})
