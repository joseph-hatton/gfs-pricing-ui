import React from 'react'
import renderer from 'react-test-renderer'
import mockConnect from '../../MockConnect'
import mockComponent from '../../MockComponent'
import { ConfirmationAlert, mapStateToProps } from '../../../src/components/redux-dialog/ConfirmationAlert'

jest.mock('react-redux', () => mockConnect())
jest.mock('../../../src/components/redux-dialog/redux-dialog', () => ({
  __esModule: true,
  default: mockComponent('ReduxDialog'),
  closeDialog: jest.fn()
}))
jest.mock('material-ui', () => ({RaisedButton: mockComponent('RaisedButton')}))

describe('ConfirmationAlert', () => {
  let props
  beforeEach(() => {
    props = {
      closeDialog: jest.fn(),
      onOk: jest.fn(),
      onCancel: jest.fn(),
      children: 'children',
      dialog: {
        dialogName: 'name'
      },
      dialogName: 'name'
    }
  })

  it('should render correctly', () => {
    expect(renderer.create(<ConfirmationAlert {...props}/>).toJSON()).toMatchSnapshot()
  })

  it('map state to props', () => {
    const result = mapStateToProps({dialog: 'yep'})
    expect(result).toEqual({dialog: 'yep'})
  })
})
