import React from 'react'
import renderer from 'react-test-renderer'
import mockConnect from '../../MockConnect'
import mockComponent from '../../MockComponent'
import { openDialog, openSnackbar, openConfirmationAlert, closeDialog, spin, stop, ReduxDialog, mapStateToProps, ReduxSnackbar, mapStateToSnackbar, Spinner, mapStateToSpinner } from '../../../src/components/redux-dialog/redux-dialog'

jest.mock('react-redux', () => mockConnect())
jest.mock('material-ui/Dialog', () => mockComponent('Dialog'))
jest.mock('material-ui', () => ({
  RaisedButton: mockComponent('RaisedButton'),
  Snackbar: mockComponent('Snackbar'),
  CircularProgress: mockComponent('CircularProgress')
}))

describe('redux-dialog', () => {
  let props

  describe('actions', () => {
    it('openDialog', () => {
      expect(openDialog('name', 'props')).toEqual({type: 'UPDATE_DIALOG_STATE', payload: {name: 'props'}})
    })

    it('openSnackbar', () => {
      expect(openSnackbar('props')).toEqual({type: 'UPDATE_DIALOG_STATE', payload: {snackbar: 'props'}})
    })

    it('openConfirmationAlert', () => {
      expect(openConfirmationAlert('props', 'aDialogName')).toEqual({type: 'UPDATE_DIALOG_STATE', payload: {aDialogName: 'props'}})
    })

    it('closeDialog', () => {
      expect(closeDialog('name')).toEqual({type: 'UPDATE_DIALOG_STATE', payload: {name: false}})
    })

    it('spin', () => {
      expect(spin()).toEqual({type: 'UPDATE_DIALOG_STATE', payload: {spinner: true}})
    })

    it('stop', () => {
      expect(stop()).toEqual({type: 'UPDATE_DIALOG_STATE', payload: {spinner: false}})
    })
  })

  describe('ReduxDialog', () => {
    beforeEach(() => {
      props = {
        dialogState: {},
        dialogName: 'aDialog',
        cancelButton: true,
        cancelLabel: 'Cancel It',
        actions: [],
        title: 'aTitle',
        contentStyle: {
          dialogContentStyle: {
            padding: '3px'
          }
        }
      }
    })
    it('should render correctly', () => {
      expect(renderer.create(<ReduxDialog {...props}/>).toJSON()).toMatchSnapshot()
    })

    it('mapStateToSnamapStateToPropsckbar', () => {
      const dialog = {name: 'a dialog'}
      expect(mapStateToProps({dialog})).toEqual({dialogState: dialog})
    })
  })

  describe('ReduxSnackbar', () => {
    beforeEach(() => {
      props = {
        snackbar: {
          type: 'WARNING'
        }
      }
    })
    it('should render correctly', () => {
      expect(renderer.create(<ReduxSnackbar {...props}/>).toJSON()).toMatchSnapshot()
    })

    it('should render with default type', () => {
      props.snackbar = {}
      expect(renderer.create(<ReduxSnackbar {...props}/>).toJSON()).toMatchSnapshot()
    })

    it('mapStateToSnackbar', () => {
      const snackbar = {snackbar: 'some snackbar'}
      expect(mapStateToSnackbar({dialog: snackbar})).toEqual(snackbar)
    })
  })

  describe('Spinner', () => {
    beforeEach(() => {
      props = {
        spin: true
      }
    })
    it('should render correctly', () => {
      expect(renderer.create(<Spinner {...props}/>).toJSON()).toMatchSnapshot()
    })

    it('should render with spin false', () => {
      props.spin = false
      expect(renderer.create(<Spinner {...props}/>).toJSON()).toMatchSnapshot()
    })

    it('mapStateToSpinner', () => {
      expect(mapStateToSpinner({dialog: {spinner: true}})).toEqual({spin: true})
    })

    it('mapStateToSpinner false', () => {
      expect(mapStateToSpinner({dialog: {}})).toEqual({spin: false})
    })
  })
})
