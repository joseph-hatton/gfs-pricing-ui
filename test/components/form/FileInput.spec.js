import React from 'react'
// import mockComponent from '../../MockComponent'
import FileInput from '../../../src/components/form/FileInput'
import renderer from 'react-test-renderer'

describe('FileInput', () => {
  let props
  beforeEach(() => {
    props = {
      input: {
        value: 'omitValue',
        onChange: jest.fn(),
        onBlur: jest.fn()
      },
      meta: 'omitMeta'
    }
  })

  it('renders correctly', () => {
    const component = renderer.create(<FileInput {...props}/>).toJSON()
    expect(component).toMatchSnapshot()
  })
})
