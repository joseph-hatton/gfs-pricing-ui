import {exists, Required, toNumber} from '../../../src/components/form/Validators'
const existingString = 'something'
const errorRequired = 'Required'
describe('Validators Specs', () => {
  describe('exists', () => {
    it('finds existing', () => expect(exists(existingString)).toEqual(true))
  })
  describe('Required', () => {
    it('no error message with existing', () => expect(Required(existingString)).toBeUndefined())
    it('no error message with space', () => expect(Required(' ')).toBeUndefined())
    it('required error message for undefined', () => expect(Required()).toEqual(errorRequired))
    it('required error message for null', () => expect(Required(null)).toEqual(errorRequired))
    it('required error message for empty string', () => expect(Required('')).toEqual(errorRequired))
  })

  describe('toNumber', () => {
    it('returns null if falsy', () => expect(toNumber()).toBeNull())
    it('positive whole number', () => expect(toNumber('23')).toBe(23))
    it('positive number', () => expect(toNumber('23.2')).toBe(23.2))
    it('negative number becomes positive', () => expect(toNumber('-23.2')).toBe(23.2))
  })
})
