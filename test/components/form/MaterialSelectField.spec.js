import React from 'react'
import mockComponent from '../../MockComponent'
import MaterialSelectField from '../../../src/components/form/MaterialSelectField'
import renderer from 'react-test-renderer'

jest.mock('redux-form', () => {
  return {
    Field: mockComponent('Field'),
    reduxForm: () => jest.fn()
  }
})
jest.mock('material-ui/MenuItem', () => mockComponent('MenuItem'))
jest.mock('redux-form-material-ui', () => ({ SelectField: mockComponent('SelectField') }))

describe('MaterialSelectField Specs', () => {
  it('MaterialSelectField Renders Correctly', () => {
    const props = {
      options: ['a', 'b', 'c'],
      nullable: true
    }
    const component = renderer.create(
      <MaterialSelectField {...props}/>
    ).toJSON()
    expect(component).toMatchSnapshot()
  })
})
