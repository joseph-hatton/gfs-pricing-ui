import { createStore, applyMiddleware } from 'redux'
import { createLogger } from 'redux-logger' // eslint-disable-line no-unused-vars
import promiseMiddleware from 'redux-promise-middleware'
import { composeWithDevTools } from 'redux-devtools-extension'

import store from '../src/store' // eslint-disable-line no-unused-vars

jest.mock('../src/reducers', () => 'rootReducer')
jest.mock('redux-thunk', () => 'thunk')
jest.mock('redux-promise-middleware', () => jest.fn())
jest.mock('redux', () => ({
  createStore: jest.fn(),
  applyMiddleware: jest.fn()
}))
jest.mock('redux-logger', () => ({
  createLogger: jest.fn()
}))
jest.mock('redux-devtools-extension', () => ({
  composeWithDevTools: jest.fn()
}))

describe('store', () => {
  it('should setup correctly', () => {
    expect(createStore).toHaveBeenCalled()
    expect(createStore.mock.calls[0][0]).toBe('rootReducer')
    expect(createStore.mock.calls[0][1]).toEqual({})
    expect(applyMiddleware).toHaveBeenCalled()
    expect(applyMiddleware.mock.calls[0][0]).toBe('thunk')
    expect(composeWithDevTools).toHaveBeenCalled()
    expect(promiseMiddleware).toHaveBeenCalled()
  })
})
