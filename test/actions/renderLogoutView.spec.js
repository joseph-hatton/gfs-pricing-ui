import renderLogoutView from '../../src/actions/renderLogoutView'
import { render } from 'react-dom'
import LogOutIFrame from '../../src/components/LogOutIFrame'
import React from 'react'

jest.mock('react-dom', () => ({
  render: jest.fn()
}))

describe('renderLogoutView', () => {
  const apply = () => renderLogoutView({url: 'someUrl', height: 768, width: 1024})

  it('renders logout iframe', async () => {
    await apply()

    expect(render).toHaveBeenCalledWith(<LogOutIFrame height={768} reloadDelay={1000} url="someUrl" width={1024} />, null)
  })
})
