import invokeHttpRequest from '../../src/actions/invokeHttpRequest'
import getCurrentUser from '../../src/actions/getCurrentUser'

jest.mock('../../src/actions/getCurrentUser', () => jest.fn())

describe('invokeHttpRequest', () => {
  let requestSet
  beforeEach(() => {
    requestSet = jest.fn().mockReturnValue('returnFromSet')
    getCurrentUser.mockReturnValue({
      session: {
        token: 'some token'
      },
      customHeaders: {a: 'headerA'}
    })
  })

  const apply = () => invokeHttpRequest({set: requestSet})

  it('request is returned', async () => {
    const returned = await apply()

    expect(returned).toEqual('returnFromSet')
  })

  it('request is returned', async () => {
    await apply()

    expect(requestSet).toHaveBeenCalledWith({
      Authorization: 'Bearer some token',
      a: 'headerA'
    })
  })

  it('no custom headers', async () => {
    getCurrentUser.mockReturnValue({
      session: {
        token: 'some token'
      }
    })
    await apply()

    expect(requestSet).toHaveBeenCalledWith({
      Authorization: 'Bearer some token'
    })
  })

  it('no token', async () => {
    getCurrentUser.mockReturnValue({
      session: {
      },
      customHeaders: {a: 'headerA'}
    })
    await apply()

    expect(requestSet).toHaveBeenCalledWith({
      a: 'headerA'
    })
  })
})
