import http from '../../src/actions/http'
import store from '../../src/store'

jest.mock('../../src/actions/getCurrentUser.js', () => jest.fn().mockReturnValue(Promise.resolve({session: {token: 'tokenA'}})))
jest.mock('../../src/components/redux-dialog', () => ({
  spin: jest.fn().mockReturnValue('spin'),
  stop: jest.fn().mockReturnValue('stop'),
  openSnackbar: jest.fn().mockReturnValue('snackbarOpened')
}))
jest.mock('../../src/store', () => ({dispatch: jest.fn()}))

describe('http', () => {
  let agent
  beforeEach(() => {
    jest.clearAllMocks()
    agent = require('superagent')
    agent.__setMockError(null)
  })

  it('gets response error message', () => {
    const result = http.httpErrorMessage({response: {body: {message: 'found it'}}})
    expect(result).toEqual('found it')
  })

  it('gets undefined when no response error message', () => {
    const result = http.httpErrorMessage({})
    expect(result).toEqual(undefined)
  })

  it('createRequest calls http.get', async () => {
    const result = await http.createRequest('get', 'someUrl1')
    expect(result.body).toEqual({})
    expect(agent.get).toHaveBeenCalledWith('someUrl1')
  })

  it('invokeRequestWithAuthorization', async () => {
    const req = agent.get('someUrl2')
    const result = await http.invokeRequestWithAuthorization(req)
    expect(result.body).toEqual({})
    expect(req.set).toHaveBeenCalledWith({Authorization: 'Bearer tokenA'})
  })

  it('get', async () => {
    jest.useFakeTimers()
    agent.__setMockResponse({body: 'a response'})
    const result = await http.get('someUrl1', {param1: 'param1Value'})

    expect(result).toEqual('a response')
    jest.runAllTimers()

    expect(store.dispatch).toHaveBeenCalledTimes(2)
    expect(store.dispatch.mock.calls[0][0]).toEqual('spin')
    expect(store.dispatch.mock.calls[1][0]).toEqual('stop')
  })

  it('put', async () => {
    jest.useFakeTimers()
    agent.__setMockResponse({body: 'a response'})
    const result = await http.put('someUrl1', {param1: 'param1Value'}, {successMessage: 'did it'})

    expect(result).toEqual('a response')
    jest.runAllTimers()

    expect(store.dispatch).toHaveBeenCalledTimes(3)
    expect(store.dispatch.mock.calls[0][0]).toEqual('spin')
    expect(store.dispatch.mock.calls[1][0]).toEqual('snackbarOpened')
    expect(store.dispatch.mock.calls[2][0]).toEqual('stop')
  })
})
