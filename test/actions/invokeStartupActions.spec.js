import invokeStartupActions from '../../src/actions/invokeStartupActions'
import {updateCurrentUser} from '../../src/actions/getCurrentUser'
import {loadAllRefs} from '../../src/refs/refActions'

jest.mock('../../src/actions/getCurrentUser', () => ({updateCurrentUser: jest.fn().mockReturnValue(Promise.resolve())}))
jest.mock('../../src/refs/refActions', () => ({loadAllRefs: jest.fn().mockReturnValue(Promise.resolve())}))

const dispatch = 'dispatch'
const getState = 'getState'

describe('invokeStartupActions', () => {
  const apply = () => invokeStartupActions(dispatch, getState)

  it('update current user called', async () => {
    await apply()

    expect(updateCurrentUser).toHaveBeenCalled()
  })

  it('load all refs called', async () => {
    await apply()

    expect(loadAllRefs).toHaveBeenCalledWith(dispatch, getState)
  })

  it('load all refs called after update current user', async () => {
    await apply()

    expect(loadAllRefs).toHaveBeenCalledWith(dispatch, getState)
  })
})
