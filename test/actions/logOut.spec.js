import logOut from '../../src/actions/logOut'
import renderLogoutView from '../../src/actions/renderLogoutView'

jest.mock('../../src/actions/renderLogoutView', () => jest.fn())
jest.mock('superagent', () => ({
  del: jest.fn().mockReturnValue(Promise.resolve({body: {logoutUrl: 'someLogoutUrl'}}))
}))

describe('logOut', () => {
  const apply = () => logOut()

  it('render logout view called', async () => {
    await apply()

    expect(renderLogoutView).toHaveBeenCalledWith({height: 768, url: 'someLogoutUrl', width: 1024})
  })
})
