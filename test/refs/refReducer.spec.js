import cloneDeep from 'lodash/cloneDeep'
import refReducer from '../../src/refs/refReducer'

const allRefs = {
  typeBusinesses: ['In-Force', 'New'],
  countries: [{ countryCode: 'c1' }, { countryCode: 'c2' }],
  stages: ['Exploring', 'Analyzing', 'Negotiating', 'Closing', 'Implementing', 'Final', 'TBD'],
  productTypes: [{ code: 'ADB', value: 'Accidental Death Benefit' }, { code: 'ADX', value: 'x Death Benefit' }],
  dealStatuses: ['In Progress', 'Dead', 'Final'],
  probCloses: ['Low', 'Medium', 'High'],
  businessUnitTree: [
    {
      'businessName': 'ARCHA-usd-NC',
      'currencyCode': 'USD',
      'currencyName': 'US Dollar',
      'officeCode': 'RGA_NON_CONSOL',
      'officeName': 'RGA Non-Consolidated',
      'geoSegmentCode': 'CRP',
      'geoSegmentName': 'Corporate',
      'legalEntityCode': 'LE_ARCHA',
      'legalEntityName': 'ARCH SOLUTIONS AGENCY LLC'
    },
    {
      'businessName': 'ARCHG-usd-NC',
      'currencyCode': 'USD',
      'currencyName': 'US Dollar',
      'officeCode': 'RGA_NON_CONSOL',
      'officeName': 'RGA Non-Consolidated',
      'geoSegmentCode': 'CRP',
      'geoSegmentName': 'Corporate',
      'legalEntityCode': 'LE_ARCHG',
      'legalEntityName': 'ARCH SOLUTIONS GROUP LLC'
    },
    {
      'businessName': 'AUROR-usd-US',
      'currencyCode': 'USD',
      'currencyName': 'US Dollar',
      'officeCode': 'AUR',
      'officeName': 'Aurora',
      'geoSegmentCode': 'US_LASA',
      'geoSegmentName': 'United States/Latin America',
      'legalEntityCode': 'LE_AURO',
      'legalEntityName': 'AURORA NATIONAL LIFE ASSURANCE COMPANY'
    }]
}

describe('refReducer', () => {
  let initialState
  beforeEach(() => {
    initialState = {
      allRefs: null,
      productTypes: null,
      countries: null,
      dealStatuses: null,
      stages: null,
      probCloses: null,
      businessTree: null
    }
  })
  it('inital state', () => {
    const result = refReducer()
    expect(result).toEqual(cloneDeep(initialState))
  })

  describe('load all refs fulfilled', () => {
    it('populates allRefs', () => {
      const result = refReducer(cloneDeep(initialState), { type: 'LOAD_ALL_REFS_FULFILLED', payload: allRefs })
      expect(result.allRefs).toEqual(allRefs)
    })

    it('populates productTypes', () => {
      const result = refReducer(cloneDeep(initialState), { type: 'LOAD_ALL_REFS_FULFILLED', payload: allRefs })
      expect(result.productTypes).toEqual(['Accidental Death Benefit', 'x Death Benefit'])
    })

    it('populates countries', () => {
      const result = refReducer(cloneDeep(initialState), { type: 'LOAD_ALL_REFS_FULFILLED', payload: allRefs })
      expect(result.countries).toEqual(['c1', 'c2'])
    })

    it('populates dealStatuses', () => {
      const result = refReducer(cloneDeep(initialState), { type: 'LOAD_ALL_REFS_FULFILLED', payload: allRefs })
      expect(result.dealStatuses).toEqual(allRefs.dealStatuses)
    })

    it('populates stages', () => {
      const result = refReducer(cloneDeep(initialState), { type: 'LOAD_ALL_REFS_FULFILLED', payload: allRefs })
      expect(result.stages).toEqual(allRefs.stages)
    })

    it('populates probCloses', () => {
      const result = refReducer(cloneDeep(initialState), { type: 'LOAD_ALL_REFS_FULFILLED', payload: allRefs })
      expect(result.probCloses).toEqual(allRefs.probCloses)
    })

    it('populates businessTree', () => {
      const result = refReducer(cloneDeep(initialState), { type: 'LOAD_ALL_REFS_FULFILLED', payload: allRefs })
      expect(result.businessTree).toEqual({
        businessName: [
          'ARCHA-usd-NC',
          'ARCHG-usd-NC',
          'AUROR-usd-US'
        ],
        currency: [
          'USD'
        ],
        geoSegment: [
          'CRP',
          'US_LASA'
        ],
        legalEntity: [
          'LE_ARCHA',
          'LE_ARCHG',
          'LE_AURO'
        ],
        office: [
          'AUR',
          'RGA_NON_CONSOL'
        ]
      }
      )
    })
  })
})
