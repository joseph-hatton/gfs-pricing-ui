import {loadAllRefs} from '../../src/refs/refActions'

jest.mock('../../src/actions/http', () => ({get: jest.fn().mockReturnValue('http.get response')}))

describe('refActions', () => {
  let dispatch
  beforeEach(() => {
    dispatch = jest.fn()
  })

  it('loadAllRefs', async () => {
    await loadAllRefs(dispatch)
    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch.mock.calls[0][0]).toEqual({
      type: 'LOAD_ALL_REFS',
      payload: 'http.get response'
    })
  })
})
