import React from 'react'

console.log('Mock Comp Init')

export default (componentName) => {
  return (props) => { // eslint-disable-line react/display-name
    const MockedComponent = componentName
    return (
      <MockedComponent originalComponent={componentName} {...props}>{props.children}</MockedComponent> // eslint-disable-line react/prop-types
    )
  }
}
