import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../MockComponent'

import CommonPicker from '../../src/pickers/CommonPicker'

jest.mock('@material-ui/core/Grid', () => mockComponent('Grid'))
jest.mock('../../src/components/form/MaterialSelectField', () => mockComponent('MaterialSelectField'))

describe('CommonPicker', () => {
  let props
  beforeEach(() => {
    props = {
      a: 'a1'
    }
  })
  it('should render correctly', () => {
    expect(renderer.create(<CommonPicker {...props}/>).toJSON()).toMatchSnapshot()
  })
})
