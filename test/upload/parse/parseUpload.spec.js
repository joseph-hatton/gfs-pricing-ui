import parseUpload from '../../../src/upload/parse/parseUpload'
import steps from '../../../src/upload/parse/steps'

const file = 'aFile'
const selectedDealName = 'dealA'
const uploadConfig = {
  scalar: {
    tabName: 'VectorTab'
  }
}

jest.mock('../../../src/upload/parse/steps', () => ([
  {
    id: 'reducer1',
    run: jest.fn().mockReturnValue(Promise.resolve({currentStep: {}}))
  }
]))

describe('parseUpload', () => {
  let dispatch, getState

  const apply = () => parseUpload(file, uploadConfig, dispatch, getState, selectedDealName)
  beforeEach(() => {
    dispatch = jest.fn()
    getState = jest.fn().mockReturnValue({upload: {steps: [1, 2]}})
  })

  it('expect current step to complete', async () => {
    const result = await apply()

    expect(result.currentStep).toEqual({completed: true})
  })

  it('expect dispatch to have been called', async () => {
    await apply()

    expect(dispatch).toHaveBeenCalledTimes(2)
    expect(dispatch).toHaveBeenNthCalledWith(1, {type: 'SPREADSHEET_STEP_START', stepId: 'reducer1'})
    expect(dispatch).toHaveBeenNthCalledWith(2, {type: 'SPREADSHEET_STEP_SUCCESS', stepId: 'reducer1'})
  })

  it('errors returned', async () => {
    const errors = ['error1']
    steps[0].run.mockReturnValue(Promise.resolve({currentStep: {errors}}))
    await apply()

    expect(dispatch).toHaveBeenCalledTimes(2)
    expect(dispatch).toHaveBeenNthCalledWith(1, {type: 'SPREADSHEET_STEP_START', stepId: 'reducer1'})
    expect(dispatch).toHaveBeenNthCalledWith(2, {type: 'SPREADSHEET_STEP_FAIL', stepId: 'reducer1', errors})
  })

  it('step says to not continue', async () => {
    steps[0].run.mockReturnValue(Promise.resolve({currentStep: {continue: false}}))
    await apply()

    expect(dispatch).toHaveBeenCalledTimes(2)
    expect(dispatch).toHaveBeenNthCalledWith(1, {type: 'SPREADSHEET_STEP_START', stepId: 'reducer1'})
    expect(dispatch).toHaveBeenNthCalledWith(2, {type: 'SPREADSHEET_STEP_FAIL', stepId: 'reducer1', errors: undefined, continue: false})
  })
})
