import getValueCellValue from '../../../../src/upload/parse/steps/getValueCellValue'

const sheet = {
  a1: {
    w: 'a1w',
    v: 'a1v'
  },
  b1: {
    w: '2018-01-01'
  }
}
describe('getValueCellValue', () => {
  it('get value', () => {
    expect(getValueCellValue(sheet, 'a1')).toEqual('a1v')
  })

  it('get value with formatted', () => {
    expect(getValueCellValue(sheet, 'b1', 'effectiveDate')).toEqual(new Date('2018-01-01T06:00:00.000Z'))
  })

  it('get default value when not found', () => {
    expect(getValueCellValue(sheet, 'c1', 'effectiveDate', 'someDefault')).toEqual('someDefault')
  })
})
