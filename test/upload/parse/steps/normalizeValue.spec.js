import normalizeValue from '../../../../src/upload/parse/steps/normalizeValue'

const configFields = {
  field1: {type: 'string'},
  field2: {type: 'integer'},
  field3: {type: 'number'},
  field4: {type: 'date'}
}

describe('normalizeValue', () => {
  let field
  const apply = (value) => normalizeValue(configFields, field, value)

  describe('string type', () => {
    beforeEach(() => {
      field = 'field1'
    })

    it('is string', () => expect(apply('hi')).toEqual('hi'))

    it('is number', () => expect(apply(11)).toEqual('11'))

    it('is null', () => expect(apply(null)).toEqual(null))
  })

  describe('integer type', () => {
    beforeEach(() => {
      field = 'field2'
    })

    it('is integer', () => expect(apply(12)).toEqual(12))

    it('is string', () => expect(apply('13')).toEqual(13))

    it('is null', () => expect(apply(null)).toEqual(null))
  })

  describe('number type', () => {
    beforeEach(() => {
      field = 'field3'
    })

    it('is number', () => expect(apply(12.2)).toEqual(12.2))

    it('is string', () => expect(apply('13.3')).toEqual(13.3))

    it('is null', () => expect(apply(null)).toEqual(null))
  })

  describe('date type', () => {
    beforeEach(() => {
      field = 'field4'
    })

    it('is date', () => {
      const d = new Date()
      expect(apply(d)).toEqual(d)
    })

    it('is null', () => expect(apply(null)).toEqual(null))
  })
})
