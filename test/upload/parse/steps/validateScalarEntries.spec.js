import cloneDeep from 'lodash/cloneDeep'
import omit from 'lodash/omit'
import validateScalarEntries from '../../../../src/upload/parse/steps/validateScalarEntries'

const uploadConfig = {
  scalar: {
    tabName: 'scalar tab',
    fields: {
      dealDetail: {
        dealName: { type: 'string', required: true },
        description: { type: 'string', requiredWithStatus: 'Final' },
        productType: { type: 'string', required: true, valid: ['Asset Intesive'] }
      }
    }
  }
}

describe('validateScalarEntries', () => {
  let objectToValidate

  beforeEach(() => {
    objectToValidate = {
      dealName: {value: 'dealA'},
      description: {value: 'a big deal'},
      productType: {value: 'Asset Intesive'}
    }
  })

  it('no errors', () => {
    const result = validateScalarEntries(uploadConfig, objectToValidate, 'Final', uploadConfig.scalar.fields.dealDetail, [])

    expect(result).toEqual([])
  })

  it('deal name empty', () => {
    objectToValidate.dealName.value = ''

    const result = validateScalarEntries(uploadConfig, objectToValidate, 'Final', uploadConfig.scalar.fields.dealDetail, [])

    expect(result).toEqual([{
      field: 'dealName', message: 'no value provided for required field', tab: 'scalar tab'
    }])
  })

  it('description empty on final', () => {
    objectToValidate.description.value = ''

    const result = validateScalarEntries(uploadConfig, objectToValidate, 'Final', uploadConfig.scalar.fields.dealDetail, [])

    expect(result).toEqual([{
      field: 'description', message: 'no value provided for required field', tab: 'scalar tab'
    }])
  })

  it('description empty on in progress', () => {
    objectToValidate.description.value = ''

    const result = validateScalarEntries(uploadConfig, objectToValidate, 'In Progress', uploadConfig.scalar.fields.dealDetail, [])

    expect(result).toEqual([])
  })

  it('description configured as unknown type', () => {
    const fields = cloneDeep(uploadConfig.scalar.fields.dealDetail)
    fields.description.type = 'donkey'
    const result = validateScalarEntries(uploadConfig, objectToValidate, 'Final', fields, [])

    expect(result).toEqual([{
      field: 'description', message: 'unable to be validated due to an unknown type: donkey', tab: 'scalar tab', inputValue: 'a big deal'
    }])
  })

  it('description required field not found', () => {
    const objectToValidateWithout = omit(objectToValidate, ['description'])
    const result = validateScalarEntries(uploadConfig, objectToValidateWithout, 'Final', uploadConfig.scalar.fields.dealDetail, [])

    expect(result).toEqual([{
      field: 'description', message: 'required field not found', tab: 'scalar tab'
    }])
  })
})
