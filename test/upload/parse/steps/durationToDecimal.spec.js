import durationToDecimal from '../../../../src/upload/parse/steps/durationToDecimal'
import {Decimal} from 'decimal.js'

describe('durationToDecimal', () => {
  it('found value', () => {
    expect(durationToDecimal({propA: {value: 0.001}})('propA')).toEqual(new Decimal(0.001))
  })

  it('default value', () => {
    expect(durationToDecimal({propA: {value: 0.001}})('propB')).toEqual(new Decimal(0))
  })
})
