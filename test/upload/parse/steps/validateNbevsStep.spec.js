import validateNbevsStep from '../../../../src/upload/parse/steps/validateNbevsStep'
import cloneDeep from 'lodash/cloneDeep'

const initial = {
  uploadConfig: {
    vector: {
      tabName: 'vector tab',
      fields: {
        vector: {
          propA: {required: true},
          propB: {requiredWithStatus: 'Final'}
        }
      }
    },
    scalar: {
      tabName: 'UploadScalar'
    }
  },
  hasVectors: true,
  currentStep: {
    errors: []
  },
  parsed: {
    nbevDeal: {value: -14262517.820810490488},
    companyAccountingBases: [
      {
        accountingBasis: 'GAAP EC',
        nbev1: {value: -14262517.820810490488},
        nbevrate1: {value: 0.08}
      }
    ],
    vectors: [
      {
        accountingBasis: 'GAAP EC',
        durations: [
          {duration: 0, monthlyDuration: 0, distEarn: {value: -14564263.7031253}},
          {duration: 1, monthlyDuration: 1, distEarn: {value: 158228.052284287}},
          {duration: 2, monthlyDuration: 13, distEarn: {value: 157096.013892738}}
        ]
      }
    ]
  }
}

describe('validateNbevsStep', () => {
  it('id', () => {
    expect(validateNbevsStep.id).toEqual('validateNbevsStep')
  })

  it('description', () => {
    expect(validateNbevsStep.description).toEqual('Validating NBEVs')
  })

  it('no errors if no vectors', () => {
    const acc = cloneDeep(initial)
    acc.hasVectors = false
    const result = validateNbevsStep.run(acc)
    expect(result.currentStep).toEqual({errors: []})
  })

  it('no errors', () => {
    const result = validateNbevsStep.run(cloneDeep(initial))
    expect(result.currentStep).toEqual({errors: []})
  })

  const validatesAnotherNbev = (nbevWithRateObject) => {
    const acc = cloneDeep(initial)
    const cab = acc.parsed.companyAccountingBases[0]
    cab.nbev2 = {value: -14254280}
    cab.nbevrate2 = {value: 0.03}
    const result = validateNbevsStep.run(acc)
    expect(result.currentStep).toEqual({errors: []})
  }

  it('also validates nbev2', () => {
    validatesAnotherNbev({nbev2: {value: -14254280}, nbevrate2: {value: 0.03}})
  })

  it('also validates nbev3', () => {
    validatesAnotherNbev({nbev2: {value: -14332178}, nbevrate2: {value: 0.83}})
  })

  it('calculation error with no vector durations', () => {
    const acc = cloneDeep(initial)
    acc.parsed.vectors[0].durations = []
    const result = validateNbevsStep.run(acc)
    expect(result.currentStep).toEqual({errors: [
      {accountingBasis: 'GAAP EC', field: 'nbev1', inputValue: -14262518, message: 'NBEV input value does not equal PV of Distributable Earnings', tab: 'UploadScalar'},
      {accountingBasis: 'GAAP EC', field: 'nbevDeal', inputValue: -14262518, message: 'NBEV input value does not equal PV of Distributable Earnings', tab: 'UploadScalar'}
    ]})
  })

  it('nbevDeal error when it doesnt match gaap ec at 8%', () => {
    const acc = cloneDeep(initial)
    acc.parsed.nbevDeal = { value: -1 }
    const result = validateNbevsStep.run(acc)
    expect(result.currentStep).toEqual({errors: [
      {accountingBasis: 'GAAP EC', field: 'nbevDeal', message: 'NBEV input value does not equal PV of Distributable Earnings', tab: 'UploadScalar', calculatedValue: -14262518, inputValue: -1}
    ]})
  })

  it('unable to validate nbevs with invalid durations', () => {
    const acc = cloneDeep(initial)
    acc.parsed.vectors[0].durations[1].duration = 'a'
    const result = validateNbevsStep.run(acc)
    expect(result.currentStep).toEqual({errors: [
      {field: 'duration', message: 'Unable to validate NBEVs with invalid durations', tab: 'vector tab'}
    ]})
  })
})
