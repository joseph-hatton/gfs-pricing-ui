import getPropertyValue from '../../../../src/upload/parse/steps/getPropertyValue'

describe('getPropertyValue', () => {
  it('get value', () => {
    expect(getPropertyValue({prop: {value: 'yep'}}, 'prop')).toEqual('yep')
  })

  it('null when no parsed', () => {
    expect(getPropertyValue(null, 'prop')).toEqual(null)
  })

  it('null when property not there', () => {
    expect(getPropertyValue({}, 'prop')).toEqual(null)
  })
})
