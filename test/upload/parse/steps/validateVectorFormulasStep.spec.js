import validateVectorFormulasStep from '../../../../src/upload/parse/steps/validateVectorFormulasStep'
import cloneDeep from 'lodash/cloneDeep'

const initial = {
  uploadConfig: {
    vector: {
      tabName: 'vector tab',
      fields: {
        vector: {
          posttaxIncome: {},
          invincCap: {},
          taxInvincCap: {},
          incrCap: {},
          capital: {},
          distEarn: {}
        }
      },
      validation: {
        addSubtractCompare: {
          gaap: {
            distributableEarnings: {
              add: ['posttaxIncome', 'invincCap'],
              subtract: ['taxInvincCap', 'incrCap'],
              target: 'distEarn',
              error: 'GAAP Distributable Earnings calculation is incorrect'
            }
          },
          localUs: {
            distributableEarnings: {
              add: ['posttaxIncome', 'invincCap'],
              subtract: ['taxInvincCap', 'incrCap'],
              target: 'distEarn',
              error: 'Local Reg Distributable Earnings calculation is incorrect'
            }
          }
        }
      }
    }
  },
  hasVectors: true,
  currentStep: {
    errors: []
  },
  parsed: {
    companyAccountingBases: [
      {
        accountingBasis: 'GAAP EC'
      }
    ],
    vectors: [
      {
        accountingBasis: 'GAAP EC',
        durations: [
          {duration: 0, posttaxIncome: {value: 43}, invincCap: {value: 20}, taxInvincCap: {value: 8}, incrCap: {value: 3}, capital: {value: 3}, distEarn: {value: 52}}
        ]
      }
    ]
  }
}

describe('validateVectorFormulasStep', () => {
  it('id', () => {
    expect(validateVectorFormulasStep.id).toEqual('validateVectorFormulasStep')
  })

  it('description', () => {
    expect(validateVectorFormulasStep.description).toEqual('Validating vector formulas')
  })

  it('no errors if no vectors', () => {
    const acc = cloneDeep(initial)
    acc.hasVectors = false
    const result = validateVectorFormulasStep.run(acc)
    expect(result.currentStep).toEqual({errors: []})
  })

  it('no errors', () => {
    const result = validateVectorFormulasStep.run(cloneDeep(initial))
    expect(result.currentStep).toEqual({errors: []})
  })

  it('no errors with no vector durations', () => {
    const acc = cloneDeep(initial)
    acc.parsed.vectors[0].durations = []
    const result = validateVectorFormulasStep.run(acc)
    expect(result.currentStep).toEqual({errors: []})
  })

  it('no errors with no vector validation', () => {
    const acc = cloneDeep(initial)
    acc.uploadConfig.vector.validation = null
    const result = validateVectorFormulasStep.run(acc)
    expect(result.currentStep).toEqual({errors: []})
  })

  it('capital did not increase error when capital is off', () => {
    const acc = cloneDeep(initial)
    acc.parsed.vectors[0].durations[0].capital.value = -5000
    const result = validateVectorFormulasStep.run(acc)
    expect(result.currentStep).toEqual({errors: [
      {accountingBasis: 'GAAP EC', field: 'incrCap', message: 'Increase of Capital input value does not equal calculated value', tab: 'vector tab', inputValue: 3, calculatedValue: -5000}
    ]})
  })

  it('gaap distributable earnings error when posttaxincome is off', () => {
    const acc = cloneDeep(initial)
    acc.parsed.vectors[0].durations[0].posttaxIncome.value = -5000
    const result = validateVectorFormulasStep.run(acc)
    expect(result.currentStep).toEqual({errors: [
      {accountingBasis: 'GAAP EC', field: 'distEarn', message: 'GAAP Distributable Earnings calculation is incorrect', tab: 'vector tab', inputValue: 52, calculatedValue: -4991}
    ]})
  })

  it('Local Reg distributable earnings error when posttaxincome is off', () => {
    const acc = cloneDeep(initial)
    acc.parsed.companyAccountingBases[0].accountingBasis = 'Local Reg'
    acc.parsed.vectors[0].accountingBasis = 'Local Reg'
    acc.parsed.vectors[0].durations[0].posttaxIncome.value = -5000
    const result = validateVectorFormulasStep.run(acc)
    expect(result.currentStep).toEqual({errors: [
      {accountingBasis: 'Local Reg', field: 'distEarn', message: 'Local Reg Distributable Earnings calculation is incorrect', tab: 'vector tab', inputValue: 52, calculatedValue: -4991}
    ]})
  })

  it('vector accounting basis is not configured for formulas, no longer throws error', () => {
    const acc = cloneDeep(initial)
    acc.parsed.vectors[0].accountingBasis = 'Donkey'
    const result = validateVectorFormulasStep.run(acc)
    expect(result.currentStep).toEqual({errors: [
    ]})
  })
})
