import validateFileTypeStep from '../../../../src/upload/parse/steps/validateFileTypeStep'
import cloneDeep from 'lodash/cloneDeep'

const initial = {file: {name: 'Some File.jpg'}, currentStep: {errors: []}}

describe('validateFileTypeStep', () => {
  it('id', () => {
    expect(validateFileTypeStep.id).toEqual('validateFileTypeStep')
  })

  it('description', () => {
    expect(validateFileTypeStep.description).toEqual('Validating the uploaded file is either type .xlsm or .xlsx')
  })

  it('adds error if incorrect file type', () => {
    const result = validateFileTypeStep.run(cloneDeep(initial))
    expect(result).toEqual({
      ...initial,
      currentStep: {
        continue: false,
        errors: ['Uploaded file must be of type .xlsm or .xlsx']
      }
    })
  })

  it('no errors if xlsx', () => {
    const hasSheet = {
      ...initial,
      file: {name: 'aspreadsheet.XLSX'}
    }
    const result = validateFileTypeStep.run(hasSheet)
    expect(result).toEqual(hasSheet)
  })

  it('no errors if xlsm', () => {
    const hasSheet = {
      ...initial,
      file: {name: 'aspreadsheet.XLSm'}
    }
    const result = validateFileTypeStep.run(hasSheet)
    expect(result).toEqual(hasSheet)
  })
})
