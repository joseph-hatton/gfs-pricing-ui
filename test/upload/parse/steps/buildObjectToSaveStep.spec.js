import buildObjectToSaveStep from '../../../../src/upload/parse/steps/buildObjectToSaveStep'
import cloneDeep from 'lodash/cloneDeep'

const apply = (parsed) => buildObjectToSaveStep.run(cloneDeep({parsed}))

describe('buildObjectToSaveStep', () => {
  it('id', () => {
    expect(buildObjectToSaveStep.id).toEqual('buildObjectToSaveStep')
  })

  it('description', () => {
    expect(buildObjectToSaveStep.description).toEqual('Building object to save')
  })

  it('only keeps value', () => {
    const result = apply({a: {value: 'yes', else: false}})
    expect(result).toEqual({parsed: {a: 'yes'}})
  })

  it('returns non-value objects', () => {
    const result = apply({a: 'yes'})
    expect(result).toEqual({parsed: {a: 'yes'}})
  })

  it('works on arrays', () => {
    const result = apply({a: [{b: {value: 'yes', else: false}}]})
    expect(result).toEqual({
      parsed: {
        a: [
          {b: 'yes'}
        ]
      }
    })
  })
})
