import hasVectorsWhenPreviouslyHadVectorsStep from '../../../../src/upload/parse/steps/hasVectorsWhenPreviouslyHadVectorsStep'
import cloneDeep from 'lodash/cloneDeep'

const vectorMissingError = {
  field: 'Vectors',
  message: 'are required when the previous revision had Vector data',
  tab: 'VectorSheet'
}

describe('hasVectorsWhenPreviouslyHadVectorsStep', () => {
  let initial
  beforeEach(() => {
    initial = {uploadConfig: {vector: {tabName: 'VectorSheet'}}, hasVectors: true, currentDeal: {hasVectors: true}, parsed: {vectors: [{durations: [{duration: 0}]}]}, currentStep: {errors: []}}
  })
  it('id', () => {
    expect(hasVectorsWhenPreviouslyHadVectorsStep.id).toEqual('hasVectorsWhenPreviouslyHadVectorsStep')
  })

  it('description', () => {
    expect(hasVectorsWhenPreviouslyHadVectorsStep.description).toEqual('Validating there are vectors if the previous upload had vectors')
  })

  it('no errors if had vectors and still does', () => {
    const result = hasVectorsWhenPreviouslyHadVectorsStep.run(cloneDeep(initial))
    expect(result.currentStep).toEqual({
      errors: []
    })
  })

  it('adds error if no longer has vector sheet', () => {
    const hasSheet = {
      ...initial,
      hasVectors: false
    }
    const result = hasVectorsWhenPreviouslyHadVectorsStep.run(hasSheet)
    expect(result.currentStep).toEqual({
      errors: [vectorMissingError]
    })
  })

  it('adds error if no longer has vectors with any durations', () => {
    const hasSheet = {
      ...initial,
      parsed: {vectors: []}
    }
    const result = hasVectorsWhenPreviouslyHadVectorsStep.run(hasSheet)
    expect(result.currentStep).toEqual({
      errors: [vectorMissingError]
    })
  })
})
