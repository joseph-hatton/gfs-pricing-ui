import parseScalarTabStep from '../../../../src/upload/parse/steps/parseScalarTabStep'
import cloneDeep from 'lodash/cloneDeep'

const initial = {
  currentDeal: {nextStatus: 'In Process'},
  uploadConfig: {
    scalar: {
      tabName: 'ScalarTab',
      legalEntityColumn: 'A',
      accountingBasisColumn: 'B',
      fieldNameColumn: 'C',
      valueColumn: 'D',
      columnsToIgnore: ['revno', 'createddate', 'userid'],
      fields: {
        dealDetail: {
        },
        companyAccountingBasis: {
        }
      }
    }
  },
  wb: {
    Sheets: {
      ScalarTab: {
      }
    }
  },
  currentStep: {errors: []}}

describe('parseScalarTabStep', () => {
  it('id', () => {
    expect(parseScalarTabStep.id).toEqual('parseScalarTabStep')
  })

  it('description', () => {
    expect(parseScalarTabStep.description).toEqual('Parsing the scalar tab')
  })

  it('defaults to empty company accounting bases', async () => {
    const acc = cloneDeep(initial)
    const result = await parseScalarTabStep.run(acc)

    expect(result.parsed).toEqual({
      'status': 'In Process',
      companyAccountingBases: []
    })
  })
})
