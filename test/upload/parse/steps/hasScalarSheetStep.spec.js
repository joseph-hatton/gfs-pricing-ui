import hasScalarSheetStep from '../../../../src/upload/parse/steps/hasScalarSheetStep'
import cloneDeep from 'lodash/cloneDeep'

const initial = {uploadConfig: {scalar: {tabName: 'UploadScalar'}}, wb: {SheetNames: []}, currentStep: {errors: []}}
describe('hasScalarSheetStep', () => {
  it('id', () => {
    expect(hasScalarSheetStep.id).toEqual('hasScalarSheetStep')
  })

  it('description', () => {
    expect(hasScalarSheetStep.description).toEqual('Validating the workbook has the Scalar sheet')
  })

  it('adds error if sheet not found', () => {
    const result = hasScalarSheetStep.run(cloneDeep(initial))
    expect(result).toEqual({
      ...initial,
      currentStep: {
        continue: false,
        errors: ['UploadScalar sheet not found']
      }
    })
  })

  it('no errors if sheet found', () => {
    const hasSheet = {
      ...initial,
      wb: {
        SheetNames: ['scalar tab']
      }
    }
    const result = hasScalarSheetStep.run(hasSheet)
    expect(result).toEqual(hasSheet)
  })
})
