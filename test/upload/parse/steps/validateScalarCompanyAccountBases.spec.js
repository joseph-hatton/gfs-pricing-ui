import validateScalarCompanyAccountingBases from '../../../../src/upload/parse/steps/validateScalarCompanyAccountingBases'
import validateScalarEntries from '../../../../src/upload/parse/steps/validateScalarEntries'
jest.mock('../../../../src/upload/parse/steps/validateScalarEntries', () => jest.fn())

const uploadConfig = {
  scalar: {
    tabName: 'scalar tab',
    fields: {
      dealDetail: {
        dealName: { type: 'string', required: true },
        description: { type: 'string', requiredWithStatus: 'Final' },
        productType: { type: 'string', required: true, valid: ['Asset Intesive'] },
        legalEntity: { type: 'string', required: true, valid: ['LE_AME'] }
      },
      companyAccountingBasis: {
        legalEntity: { type: 'string', required: true, valid: ['LE_AME'] },
        accountingBasis: { type: 'string', valid: ['Something'] }
      }
    }
  }
}

describe('validateScalarCompanyAccountingBases', () => {
  let objectToValidate, errors

  beforeEach(() => {
    validateScalarEntries.mockReturnValue([])
    errors = []
    objectToValidate = {
      companyAccountingBases: [
        {accountingBasis: 'Something', property: {value: 'propertyValue', rowNumber: '5', column: 'E'}}
      ],
      dealName: {value: 'dealA'},
      description: {value: 'a big deal'},
      productType: {value: 'Asset Intesive'}
    }
  })

  it('no errors', () => {
    const result = validateScalarCompanyAccountingBases(uploadConfig, objectToValidate, 'Final', errors)

    expect(result).toEqual([])
  })

  it('scalar errors', () => {
    validateScalarEntries.mockReturnValue([{msg: 'some error'}])

    const result = validateScalarCompanyAccountingBases(uploadConfig, objectToValidate, 'Final', errors)

    expect(result).toEqual([{
      accountingBasis: 'Something',
      msg: 'some error'
    }])
  })

  it('missing accounting basis', () => {
    objectToValidate.companyAccountingBases[0].accountingBasis = null

    const result = validateScalarCompanyAccountingBases(uploadConfig, objectToValidate, 'Final', errors)

    expect(result).toEqual([{
      field: 'accountingBasis',
      message: 'invalid accountingBasis',
      tab: 'scalar tab',
      col: undefined,
      inputValue: null,
      row: '5',
      validOptions: ['Something']
    }])
  })
})
