import validateDurationAddSubtractCompare from '../../../../src/upload/parse/steps/validateDurationAddSubtractCompare'

const uploadConfig = {
  vector: {
    tabName: 'the vector tab'
  }
}
const vector = {
  company: 'someEntity',
  accountingBasis: 'someAccountingBasis'
}

const duration = {
  duration: 1,
  propA: {
    value: 50
  },
  propB: {
    value: 60
  },
  propC: {
    value: 10
  },
  propD: {
    value: 500
  },
  propE: {
    value: 100
  }
}

describe('validateDurationAddSubtractCompare', () => {
  it('error', () => {
    const error = validateDurationAddSubtractCompare(uploadConfig, vector, duration, ['propA', 'propB'], ['propC'], 'propD', 'A + B - C does not equal D')

    expect(error).toEqual({
      accountingBasis: 'someAccountingBasis',
      company: 'someEntity',
      duration: 1,
      field: 'propD',
      message: 'A + B - C does not equal D',
      tab: 'the vector tab',
      inputValue: 500,
      calculatedValue: 100
    })
  })

  it('no error', () => {
    const error = validateDurationAddSubtractCompare(uploadConfig, vector, duration, ['propA', 'propB'], ['propC'], 'propE', 'A + B - C does not equal E')

    expect(error).toEqual(null)
  })
})
