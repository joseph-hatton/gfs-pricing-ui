import index from '../../../../src/upload/parse/steps'

jest.mock('../../../../src/upload/parse/steps/validateFileTypeStep', () => 'validateFileTypeStep')
jest.mock('../../../../src/upload/parse/steps/readWorkbookStep', () => 'readWorkbookStep')
jest.mock('../../../../src/upload/parse/steps/hasScalarSheetStep', () => 'hasScalarSheetStep')
jest.mock('../../../../src/upload/parse/steps/parseScalarTabStep', () => 'parseScalarTabStep')
jest.mock('../../../../src/upload/parse/steps/validateScalarFieldsStep', () => 'validateScalarFieldsStep')
jest.mock('../../../../src/upload/parse/steps/hasVectorSheetWhenFinalStep', () => 'hasVectorSheetWhenFinalStep')

jest.mock('../../../../src/upload/parse/steps/parseVectorTabStep', () => 'parseVectorTabStep')
jest.mock('../../../../src/upload/parse/steps/hasVectorsWhenPreviouslyHadVectorsStep', () => 'hasVectorsWhenPreviouslyHadVectorsStep')
jest.mock('../../../../src/upload/parse/steps/validateVectorCompanyAccountingBasisStep', () => 'validateVectorCompanyAccountingBasisStep')
jest.mock('../../../../src/upload/parse/steps/validateVectorDataIsNumericStep', () => 'validateVectorDataIsNumericStep')
jest.mock('../../../../src/upload/parse/steps/validateRequiredVectorsStep', () => 'validateRequiredVectorsStep')
jest.mock('../../../../src/upload/parse/steps/validateVectorFormulasStep', () => 'validateVectorFormulasStep')
jest.mock('../../../../src/upload/parse/steps/validateNbevsStep', () => 'validateNbevsStep')

jest.mock('../../../../src/upload/parse/steps/checkForErrorsToPreventSaveStep', () => 'checkForErrorsToPreventSaveStep')
jest.mock('../../../../src/upload/parse/steps/buildObjectToSaveStep', () => 'buildObjectToSaveStep')

describe('upload parse steps index', () => {
  it('should have the correct steps in order', () => {
    expect(index).toEqual([
      'validateFileTypeStep',

      'readWorkbookStep',
      'hasScalarSheetStep',
      'parseScalarTabStep',
      'validateScalarFieldsStep',

      'hasVectorSheetWhenFinalStep',
      'parseVectorTabStep',
      'hasVectorsWhenPreviouslyHadVectorsStep',
      'validateVectorCompanyAccountingBasisStep',
      'validateVectorDataIsNumericStep',
      'validateRequiredVectorsStep',
      'validateVectorFormulasStep',
      'validateNbevsStep',

      'checkForErrorsToPreventSaveStep',
      'buildObjectToSaveStep'
    ])
  })
})
