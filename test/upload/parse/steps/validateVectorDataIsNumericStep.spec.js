import validateVectorDataIsNumericStep from '../../../../src/upload/parse/steps/validateVectorDataIsNumericStep'
import cloneDeep from 'lodash/cloneDeep'

const initial = {
  uploadConfig: {
    vector: {
      tabName: 'vector tab'
    }
  },
  hasVectors: true,
  currentStep: {
    errors: []
  },
  parsed: {
    vectors: [
      {
        company: 'companyA',
        accountingBasis: 'accountingBasisA',
        durations: [
          {duration: 0, propA: {value: 10}},
          {duration: 1, propB: {value: 12.1}}
        ]
      }
    ]
  }
}

describe('validateVectorDataIsNumericStep', () => {
  it('id', () => {
    expect(validateVectorDataIsNumericStep.id).toEqual('validateVectorDataIsNumericStep')
  })

  it('description', () => {
    expect(validateVectorDataIsNumericStep.description).toEqual('Validating vector data is numeric')
  })

  it('no errors', () => {
    const result = validateVectorDataIsNumericStep.run(cloneDeep(initial))
    expect(result).toEqual(initial)
  })

  it('no errors if no vectors', () => {
    const acc = cloneDeep(initial)
    acc.hasVectors = false
    const result = validateVectorDataIsNumericStep.run(acc)
    expect(result).toEqual({
      ...initial,
      hasVectors: false
    })
  })

  it('adds error if a duration is not numeric', () => {
    const acc = cloneDeep(initial)
    acc.parsed.vectors[0].durations[1].propB.value = 'donkey'
    const result = validateVectorDataIsNumericStep.run(acc)
    expect(result.currentStep).toEqual({
      errors: [{accountingBasis: 'accountingBasisA', company: 'companyA', duration: 1, field: 'propB', inputValue: 'donkey', message: 'not the expected type (number)', tab: 'vector tab'}]
    })
  })
})
