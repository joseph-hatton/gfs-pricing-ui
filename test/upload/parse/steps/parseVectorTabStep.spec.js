import cloneDeep from 'lodash/cloneDeep'
import parseVectorTabStep from '../../../../src/upload/parse/steps/parseVectorTabStep'
import parseVector from '../../../../src/upload/parse/steps/parseVector'

jest.mock('../../../../src/upload/parse/steps/parseVector', () => jest.fn())

const initial = {
  uploadConfig: {
    scalar: {
      tabName: 'VectorTab',
      fields: {companyAccountingBasis: {accountingBasis: {valid: ['aaa']}}}
    }
  },
  hasVectors: true,
  currentDeal: {legalEntity: {code: 'A', version: 1}, effectiveDate: '2018-07-24T19:19:19.010Z'},
  parsed: {
    vectorInit: {value: 1},
    vectorInitCount: {value: 12},
    vectorUlt: {value: 24}
  },
  wb: 'workbook',
  currentStep: {errors: []}}

describe('parseVectorTabStep', () => {
  beforeEach(() => {
    parseVector.mockReturnValue({ vectors: 'parsed', errAccBasis: [] })
  })

  it('id', () => {
    expect(parseVectorTabStep.id).toEqual('parseVectorTabStep')
  })

  it('description', () => {
    expect(parseVectorTabStep.description).toEqual('Parsing the vector tab')
  })

  it('parse vector called', () => {
    parseVectorTabStep.run(cloneDeep(initial))
    const legalEntity = {code: 'A', version: 1}
    expect(parseVector).toHaveBeenCalledWith(initial.wb, initial.uploadConfig, 1, 12, 24, '2018-07-24T19:19:19.010Z', legalEntity)
  })

  it('returns parse vector response', () => {
    const response = parseVectorTabStep.run(cloneDeep(initial))

    expect(response.parsed.vectors).toEqual('parsed')
  })
})
