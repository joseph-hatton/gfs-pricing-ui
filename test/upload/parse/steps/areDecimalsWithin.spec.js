import areDecimalsWithin from '../../../../src/upload/parse/steps/areDecimalsWithin'
import {Decimal} from 'decimal.js'

const apply = (a, b, within) => areDecimalsWithin(new Decimal(a), new Decimal(b), within)

describe('areDecimalsWithin', () => {
  it('same', () => {
    const result = apply(0.1, 0.1, 0.01)
    expect(result).toEqual(true)
  })

  it('same-ish', () => {
    const result = apply(0.11, 0.12, 0.01)
    expect(result).toEqual(true)
  })

  it('different', () => {
    const result = apply(0.10, 0.12, 0.01)
    expect(result).toEqual(false)
  })

  it('change within', () => {
    const result = apply(0.10, 0.12, 500)
    expect(result).toEqual(true)
  })

  it('same-ish with default within', () => {
    const result = areDecimalsWithin(new Decimal(0.11), new Decimal(0.11001))
    expect(result).toEqual(true)
  })
})
