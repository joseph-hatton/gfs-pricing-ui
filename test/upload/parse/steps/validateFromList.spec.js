import validateFromList from '../../../../src/upload/parse/steps/validateFromList'

const uploadConfig = {
  scalar: {
    tabName: 'scalar tab'
  }
}

describe('validateFromList', () => {
  it('value not found in', () => {
    const result = validateFromList(uploadConfig, ['a', 'b'], 'c', {value: 'hi'})

    expect(result).toEqual({
      field: 'c',
      message: 'invalid c',
      tab: 'scalar tab',
      validOptions: ['a', 'b'],
      inputValue: 'hi'
    })
  })

  it('value found', () => {
    const result = validateFromList(uploadConfig, ['a', 'b'], 'c', {value: 'a'})

    expect(result).toEqual(null)
  })
})
