import validateRequiredVectorsStep from '../../../../src/upload/parse/steps/validateRequiredVectorsStep'
import cloneDeep from 'lodash/cloneDeep'

const initial = {
  uploadConfig: {
    vector: {
      tabName: 'vector tab',
      fields: {
        vector: {
          propA: {required: true},
          propB: {requiredWithStatus: 'Final'}
        }
      }
    }
  },
  hasVectors: true,
  currentStep: {
    errors: []
  },
  parsed: {
    status: {
      value: 'Final'
    },
    companyAccountingBases: [
      {
        company: 'companyA',
        accountingBasis: 'accountingBasisA'
      }
    ],
    vectors: [
      {
        company: 'companyA',
        accountingBasis: 'accountingBasisA',
        durations: [
          {duration: 0, propA: {value: 10}},
          {duration: 1, propB: {value: 12.1}}
        ]
      }
    ]
  }
}

describe('validateRequiredVectorsStep', () => {
  it('id', () => {
    expect(validateRequiredVectorsStep.id).toEqual('validateRequiredVectorsStep')
  })

  it('description', () => {
    expect(validateRequiredVectorsStep.description).toEqual('Validating required vector data')
  })

  it('no errors', () => {
    const result = validateRequiredVectorsStep.run(cloneDeep(initial))
    expect(result.currentStep).toEqual({errors: []})
  })

  it('no errors if no vectors', () => {
    const acc = cloneDeep(initial)
    acc.hasVectors = false
    const result = validateRequiredVectorsStep.run(acc)
    expect(result.currentStep).toEqual({errors: []})
  })

  it('no errors with no vector durations', () => {
    const acc = cloneDeep(initial)
    acc.parsed.vectors[0].durations = []
    const result = validateRequiredVectorsStep.run(acc)
    expect(result.currentStep).toEqual({errors: [
      {field: 'propA', message: 'required field not found', tab: 'vector tab'},
      {field: 'propB', message: 'required field not found', tab: 'vector tab'}
    ]})
  })

  it('no error if status is different', () => {
    const acc = cloneDeep(initial)
    acc.parsed.status.value = 'In Progress'
    const result = validateRequiredVectorsStep.run(acc)
    expect(result.currentStep).toEqual({errors: []})
  })

  it('adds error if required and missing', () => {
    const acc = cloneDeep(initial)
    acc.parsed.vectors[0].durations = [acc.parsed.vectors[0].durations[1]]
    const result = validateRequiredVectorsStep.run(acc)
    expect(result.currentStep).toEqual({
      errors: [{field: 'propA', message: 'required field not found', tab: 'vector tab'}]
    })
  })

  it('adds error if required with status and missing', () => {
    const acc = cloneDeep(initial)
    acc.parsed.vectors[0].durations = [acc.parsed.vectors[0].durations[0]]
    const result = validateRequiredVectorsStep.run(acc)
    expect(result.currentStep).toEqual({
      errors: [{field: 'propB', message: 'required field not found', tab: 'vector tab'}]
    })
  })
})
