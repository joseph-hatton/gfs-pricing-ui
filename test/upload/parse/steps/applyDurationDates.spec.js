import applyDurationDates from '../../../../src/upload/parse/steps/applyDurationDates'
import cloneDeep from 'lodash/cloneDeep'
import moment from 'moment'

const refVectors = [
  {
    durations: [
      {duration: 0},
      {duration: 1},
      {duration: 2}
    ]
  }
]

const effectiveDate = '2018-07-24T19:19:19.010Z'

describe('applyDurationDates', () => {
  it('no vectors', () => {
    const vectors = []
    const result = applyDurationDates(vectors, 1, 12, 2, effectiveDate)
    expect(result).toEqual(null)
    expect(vectors.length).toEqual(0)
  })

  it('all vectors within vectorInitCount', () => {
    const vectors = cloneDeep(refVectors)
    applyDurationDates(vectors, 1, 12, 2, effectiveDate)
    expect(vectors).toEqual([
      {
        durations: [
          {duration: 0, monthlyDuration: 0, durationDate: moment('2018-07-01').utc().format('YYYY-MM-DD')},
          {duration: 1, monthlyDuration: 1, durationDate: moment('2018-08-01').utc().format('YYYY-MM-DD')},
          {duration: 2, monthlyDuration: 2, durationDate: moment('2018-09-01').utc().format('YYYY-MM-DD')}
        ]
      }
    ])
  })

  it('some vectors outside vectorInitCount', () => {
    const vectors = cloneDeep(refVectors)
    applyDurationDates(vectors, 1, 1, 6, effectiveDate)
    expect(vectors).toEqual([
      {
        durations: [
          {duration: 0, monthlyDuration: 0, durationDate: moment('2018-07-01').utc().format('YYYY-MM-DD')},
          {duration: 1, monthlyDuration: 1, durationDate: moment('2018-08-01').utc().format('YYYY-MM-DD')},
          {duration: 2, monthlyDuration: 7, durationDate: moment('2019-02-01').utc().format('YYYY-MM-DD')}
        ]
      }
    ])
  })
})
