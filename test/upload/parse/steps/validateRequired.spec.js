import validateRequired from '../../../../src/upload/parse/steps/validateRequired'

describe('validateRequired', () => {
  it('null', () => {
    expect(validateRequired(null)).toEqual(true)
  })

  it('empty string', () => {
    expect(validateRequired('')).toEqual(true)
  })

  it('padded empty string', () => {
    expect(validateRequired(' ')).toEqual(true)
  })

  it('empty object', () => {
    expect(validateRequired({})).toEqual(true)
  })

  it('value null', () => {
    expect(validateRequired({value: null})).toEqual(true)
  })

  it('value empty string', () => {
    expect(validateRequired({value: ''})).toEqual(true)
  })

  it('value padded empty string', () => {
    expect(validateRequired({value: ' '})).toEqual(true)
  })

  it('number', () => {
    expect(validateRequired({value: 5})).toEqual(false)
  })

  it('string', () => {
    expect(validateRequired({value: 'something'})).toEqual(false)
  })
})
