import hasVectorSheetWhenFinalStep from '../../../../src/upload/parse/steps/hasVectorSheetWhenFinalStep'
import cloneDeep from 'lodash/cloneDeep'

const initial = {parsed: {status: {value: 'Final'}}, uploadConfig: {vector: {tabName: 'vector tab'}}, wb: {SheetNames: []}, currentStep: {errors: []}}

describe('hasVectorSheetWhenFinalStep', () => {
  it('id', () => {
    expect(hasVectorSheetWhenFinalStep.id).toEqual('hasVectorSheetWhenFinalStep')
  })

  it('description', () => {
    expect(hasVectorSheetWhenFinalStep.description).toEqual('Validating the workbook has Vector Sheet when the deal status is "Final"')
  })

  it('adds error if sheet not found', () => {
    const result = hasVectorSheetWhenFinalStep.run(cloneDeep(initial))
    expect(result).toEqual({
      ...initial,
      hasVectors: false,
      currentStep: {
        continue: false,
        errors: [{
          field: 'Workbook Sheet vector tab',
          message: 'is required when Deal "status" is Final',
          tab: 'vector tab'
        }]
      }
    })
  })

  it('no errors if sheet found', () => {
    const hasSheet = {
      ...initial,
      wb: {
        SheetNames: ['vector tab']
      }
    }
    const result = hasVectorSheetWhenFinalStep.run(hasSheet)
    expect(result).toEqual(hasSheet)
  })
})
