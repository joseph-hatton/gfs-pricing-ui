import readWorkbookStep from '../../../../src/upload/parse/steps/readWorkbookStep'
import cloneDeep from 'lodash/cloneDeep'
import XLSX from 'xlsx'

const initial = {file: 'somefile.xlsx', currentStep: {errors: []}}
const wb = {name: 'a workbook'}

jest.mock('xlsx', () => ({read: jest.fn().mockReturnValue({name: 'a workbook'})}))

describe('readWorkbookStep', () => {
  let fileContents, dummyFileReader

  beforeEach(() => {
    fileContents = 'file contents'
    const readAsDataURL = jest.fn(() => dummyFileReader.onload({target: {result: btoa(fileContents)}})) // eslint-disable-line no-undef
    dummyFileReader = {readAsDataURL}
    window.FileReader = jest.fn(() => dummyFileReader)
  })

  it('id', () => {
    expect(readWorkbookStep.id).toEqual('readWorkbookStep')
  })

  it('description', () => {
    expect(readWorkbookStep.description).toEqual('Reading the Excel workbook')
  })

  it('no error if wb parsed', async () => {
    const result = await readWorkbookStep.run(cloneDeep(initial))

    expect(result).toEqual({
      ...initial,
      wb
    })
  })

  it('adds error file cannot be parsed', async () => {
    XLSX.read.mockImplementation(() => {
      throw new Error('wb could not be read')
    })
    const result = await readWorkbookStep.run(cloneDeep(initial))

    expect(result).toEqual({
      ...initial,
      currentStep: {
        continue: false,
        errors: ['Unable to parse the uploaded file.  Check contents.']
      }
    })
  })
})
