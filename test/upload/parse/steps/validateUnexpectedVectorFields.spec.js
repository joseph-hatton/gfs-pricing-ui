import validateUnexpectedVectorFields from '../../../../src/upload/parse/steps/validateUnexpectedVectorFields'

const uploadConfig = {
    scalar: {
      tabName: 'scalar tab'
    },
    vector: {
      tabName: 'vector tab',
      fields: {
        vector: {
          propA: {},
          propB: {}
        }
      }
    }
  }

const vector = {
  company: 'companyA',
  accountingBasis: 'accountingBasisA',
  durations: [
    {duration: 0, propA: {value: 10}},
    {duration: 1, propB: {value: 12.1}}
  ]
}

const duration = vector.durations[0]

describe('validateUnexpectedVectorFields', () => {
  it('no errors', () => {
    const result = validateUnexpectedVectorFields(uploadConfig, vector, duration)
    expect(result).toEqual([])
  })

  it('no errors if no vectors', () => {
    const result = validateUnexpectedVectorFields(uploadConfig, vector, {duration: 2, propC: {value: 'yes'}})
    expect(result).toEqual([
      {
        accountingBasis: 'accountingBasisA',
        company: 'companyA',
        field: 'propC',
        row: '3',
        message: 'invalid field name',
        tab: 'vector tab'
      }
    ])
  })
})
