import validateIncreaseOfCapital from '../../../../src/upload/parse/steps/validateIncreaseOfCapital'

const uploadConfig = {
  vector: {
    tabName: 'the vector tab'
  }
}
const vector = {
  company: 'someCompany',
  accountingBasis: 'someAccountingBasis'
}

const duration1 = {
  duration: 1,
  capital: {
    value: 50
  },
  incrCap: {
    value: 5
  }
}
const duration2 = {
  duration: 2,
  capital: {
    value: 56
  },
  incrCap: {
    value: 6
  }
}

const expectedError = {
  accountingBasis: 'someAccountingBasis',
  company: 'someCompany',
  duration: 2,
  field: 'incrCap',
  message: 'Increase of Capital input value does not equal calculated value',
  tab: 'the vector tab'
}

describe('validateIncreaseOfCapital', () => {
  it('no error', () => {
    const error = validateIncreaseOfCapital(uploadConfig, vector, duration2, duration1)

    expect(error).toEqual(null)
  })

  it('error when duration is wrong', () => {
    duration2.capital.value = 60
    const error = validateIncreaseOfCapital(uploadConfig, vector, duration2, duration1)

    expect(error).toEqual({
      ...expectedError,
      inputValue: 6,
      calculatedValue: 10
    })
  })

  it('error when incrCap is wrong', () => {
    duration2.incrCap.value = 88
    const error = validateIncreaseOfCapital(uploadConfig, vector, duration2, duration1)

    expect(error).toEqual({
      ...expectedError,
      calculatedValue: 10,
      inputValue: 88
    })
  })

  it('zero capital when no previous duration', () => {
    duration2.incrCap.value = 88
    const error = validateIncreaseOfCapital(uploadConfig, vector, duration2)

    expect(error).toEqual({
      ...expectedError,
      calculatedValue: 60,
      inputValue: 88
    })
  })
})
