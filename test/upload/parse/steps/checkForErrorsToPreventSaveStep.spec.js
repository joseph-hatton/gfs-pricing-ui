import checkForErrorsToPreventSaveStep from '../../../../src/upload/parse/steps/checkForErrorsToPreventSaveStep'
import cloneDeep from 'lodash/cloneDeep'

const apply = (steps) => checkForErrorsToPreventSaveStep.run({currentDeal: {dealName: 'abc'}, currentStep: {}, steps: cloneDeep(steps)})

describe('checkForErrorsToPreventSaveStep', () => {
  it('id', () => {
    expect(checkForErrorsToPreventSaveStep.id).toEqual('checkForErrorsToPreventSaveStep')
  })

  it('description', () => {
    expect(checkForErrorsToPreventSaveStep.description).toEqual('Preventing Save if errors were found')
  })
 it('has error', () => {
    const result = apply([{errors: ['an error']}])
    expect(result.currentStep).toEqual({continue: false})
  })

  it('has empty errors', () => {
    const result = apply([{errors: []}])
    expect(result.currentStep).toEqual({})
  })

  it('returns null acc', () => {
    const result = checkForErrorsToPreventSaveStep.run(null)
    expect(result).toBeNull()
  })
})
