import parseVector from '../../../../src/upload/parse/steps/parseVector'
import moment from 'moment'
jest.mock('xlsx', () => ({
  utils: {
    decode_range: jest.fn().mockReturnValue({
      e: {
        r: 4
      }
    })
  }
}))
const wb = {
  Sheets: {
    VectorTab: {
      '!ref': '!ref',
      A4: {v: 0},
      A5: {v: 1},
      B1: {v: 'Local Reg'},
      B2: {v: 'aField'},
      B4: {v: 0},
      B5: {v: 1}
    }
  }
}
const uploadConfig = {
  vector: {
    tabName: 'VectorTab'
  }
}

describe('parseVector', () => {
  beforeEach(() => {
  })

  it('one column', () => {
    const result = parseVector(wb, uploadConfig, 1, 12, 24, '2018-07-01')

    expect(result.vectors).toEqual([{
      accountingBasis: 'Local Reg',
      durations: [
        {aField: {column: 'B', rowNumber: 4, value: 0}, duration: 0, durationDate: moment('2018-07-01').utc().format('YYYY-MM-DD'), monthlyDuration: 0},
        {aField: {column: 'B', rowNumber: 5, value: 1}, duration: 1, durationDate: moment('2018-08-01').utc().format('YYYY-MM-DD'), monthlyDuration: 1}
      ]
    }])
  })
})
