import cloneDeep from 'lodash/cloneDeep'
import validateScalarFieldsStep from '../../../../src/upload/parse/steps/validateScalarFieldsStep'
import validateScalarEntries from '../../../../src/upload/parse/steps/validateScalarEntries'
import validateScalarCompanyAccountingBases from '../../../../src/upload/parse/steps/validateScalarCompanyAccountingBases'

jest.mock('../../../../src/upload/parse/steps/validateScalarEntries', () => jest.fn())
jest.mock('../../../../src/upload/parse/steps/validateScalarCompanyAccountingBases', () => jest.fn())

const initial = {
  uploadConfig: { scalar: {
    tabName: 'scalar tab',
    fields: {
      dealDetail: []
    }
  } },
  currentDeal: {
    dealName: 'dealA'
  },
  parsed: {
    dealName: {value: 'dealA'},
    status: {value: 'Final'}
  },
  currentStep: { errors: [] }
}

describe('validateScalarFieldsStep', () => {
  beforeEach(() => {
    validateScalarEntries.mockReturnValue([])
    validateScalarCompanyAccountingBases.mockReturnValue([])
  })
  it('id', () => {
    expect(validateScalarFieldsStep.id).toEqual('validateScalarFieldsStep')
  })

  it('description', () => {
    expect(validateScalarFieldsStep.description).toEqual('Validating scalar data')
  })

  it('no errors', () => {
    const result = validateScalarFieldsStep.run(cloneDeep(initial))
    expect(result.currentStep).toEqual({
      errors: []
    })
  })

  it('adds validate scalar entries errors', () => {
    validateScalarEntries.mockReturnValue(['error1'])
    const result = validateScalarFieldsStep.run(cloneDeep(initial))

    expect(result.currentStep).toEqual({
      continue: true,
      errors: ['error1']
    })
  })
})
