import validateVectorCompanyAccountingBasisStep from '../../../../src/upload/parse/steps/validateVectorCompanyAccountingBasisStep'
import cloneDeep from 'lodash/cloneDeep'

const initial = {
  uploadConfig: {
    scalar: {
      tabName: 'scalar tab',
      fields: {
        dealDetail: {
          dealName: { type: 'string', required: true },
          description: { type: 'string', requiredWithStatus: 'Final' },
          productType: { type: 'string', required: true, valid: ['Asset Intesive'] },
          legalEntity: { type: 'string', required: true, valid: ['LE_AME', 'VALID'] }
        },
        companyAccountingBasis: {
          legalEntity: { type: 'string', required: true, valid: ['LE_AME', 'VALID'] },
          accountingBasis: { type: 'string', valid: ['GAAP EC'] }
        }
      }
    },
    vector: {
      tabName: 'vector tab'
    }
  },
  hasVectors: true,
  currentStep: {
    errors: []
  },
  parsed: {
    status: {value: 'Final'},
    companyAccountingBases: [
      {
        accountingBasis: 'GAAP EC',
        nbev1: {
          value: 888,
          rowNumber: 4,
          column: 'C'
        }
      }
    ],
    vectors: [
      {
        accountingBasis: 'GAAP EC',
        durations: [
          {duration: 0, propA: {value: 10, column: 'A'}},
          {duration: 1, propB: {value: 12.1, column: 'A'}}
        ]
      }
    ]
  }
}

describe('validateVectorCompanyAccountingBasisExistsOnScalarTabStep', () => {
  it('id', () => {
    expect(validateVectorCompanyAccountingBasisStep.id).toEqual('validateVectorCompanyAccountingBasisStep')
  })

  it('description', () => {
    expect(validateVectorCompanyAccountingBasisStep.description).toEqual('Validating vector Accounting Basis')
  })

  it('no errors', () => {
    const result = validateVectorCompanyAccountingBasisStep.run(cloneDeep(initial))
    expect(result.currentStep).toEqual({errors: []})
  })

  it('no errors if no vectors', () => {
    const acc = cloneDeep(initial)
    acc.hasVectors = false
    const result = validateVectorCompanyAccountingBasisStep.run(acc)
    expect(result.currentStep).toEqual({errors: []})
  })

  it('no errors with no vector durations', () => {
    const acc = cloneDeep(initial)
    acc.parsed.vectors[0].durations = []
    const result = validateVectorCompanyAccountingBasisStep.run(acc)
    expect(result.currentStep).toEqual({errors: []})
  })
})
