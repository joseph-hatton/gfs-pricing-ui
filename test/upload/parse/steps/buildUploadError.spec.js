import buildUploadError, {buildScalarError, buildScalarErrorWithInputAndCalculated, buildVectorError, buildVectorErrorWithInputAndCalculated} from '../../../../src/upload/parse/steps/buildUploadError'

const uploadConfig = {
  scalar: {
    tabName: 'UploadScalar'
  },
  vector: {
    tabName: 'UploadVector'
  }
}

describe('buildUploadError', () => {
  describe('default', () => {
    it('build error with everything', () => {
      const err = buildUploadError({tab: 'tabA', field: 'field0', message: 'message1', parsedField: {value: 'some text', rowNumber: 5, column: 'C'}})
      expect(err).toEqual({
        tab: 'tabA',
        field: 'field0',
        message: 'message1',
        row: 5,
        col: 'C'
      })
    })

    it('build error with just tab, field, message', () => {
      const err = buildUploadError({tab: 'tabA', field: 'field0', message: 'message1'})
      expect(err).toEqual({
        tab: 'tabA',
        field: 'field0',
        message: 'message1'
      })
    })

    it('add valid options', () => {
      const err = buildUploadError({tab: 'tabA', field: 'field0', message: 'message1', validOptions: ['a']})
      expect(err).toEqual({
        tab: 'tabA',
        field: 'field0',
        message: 'message1',
        validOptions: ['a']
      })
    })
  })

  describe('buildScalarErrorWithInputAndCalculated', () => {
    it('build error', () => {
      const result = buildScalarErrorWithInputAndCalculated(uploadConfig, 'fieldA', 'some message', {col: 5, row: 'A'}, 8, 9)
      expect(result).toEqual({
        tab: 'UploadScalar',
        field: 'fieldA',
        message: 'some message',
        inputValue: 8,
        calculatedValue: 9
      })
    })
  })

  describe('buildScalarError', () => {
    it('build error', () => {
      const result = buildScalarError(uploadConfig, 'fieldA', 'some message', {col: 5, row: 'A'})
      expect(result).toEqual({
        tab: 'UploadScalar',
        field: 'fieldA',
        message: 'some message'
      })
    })
  })

  describe('buildVectorError', () => {
    it('build error', () => {
      const result = buildVectorError(uploadConfig, 'legalEntityA', 'accountingBasisA', 'fieldA', 'some message', {col: 5, row: 'A'})
      expect(result).toEqual({
        tab: 'UploadVector',
        field: 'fieldA',
        message: 'some message',
        company: 'legalEntityA',
        accountingBasis: 'accountingBasisA'
      })
    })
  })

  describe('buildVectorErrorWithInputAndCalculated', () => {
    it('build error', () => {
      const result = buildVectorErrorWithInputAndCalculated(uploadConfig, 'legalEntityA', 'accountingBasisA', 'fieldA', 'some message', {col: 5, row: 'A'}, 122, 8, 9)
      expect(result).toEqual({
        tab: 'UploadVector',
        field: 'fieldA',
        message: 'some message',
        company: 'legalEntityA',
        accountingBasis: 'accountingBasisA',
        duration: 122,
        inputValue: 8,
        calculatedValue: 9
      })
    })
  })
})
