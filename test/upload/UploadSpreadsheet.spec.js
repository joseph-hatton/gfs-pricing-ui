import React from 'react'
import renderer from 'react-test-renderer'
import mockConnect from '../MockConnect'
import mockComponent from '../MockComponent'
import { UploadSpreadsheet, mapStateToProps } from '../../src/upload/UploadSpreadsheet'

jest.mock('redux-form', () => ({ Field: mockComponent('Field'), reduxForm: () => (comp) => comp }))
jest.mock('react-redux', () => mockConnect())
jest.mock('@material-ui/core/Button', () => mockComponent('Button'))
jest.mock('@material-ui/core/FormControl', () => mockComponent('FormControl'))
jest.mock('@material-ui/core/Stepper', () => mockComponent('Stepper'))
jest.mock('@material-ui/core/Step', () => mockComponent('Step'))
jest.mock('@material-ui/core/StepLabel', () => mockComponent('StepLabel'))
jest.mock('../../src/components/form/FileInput', () => mockComponent('FileInput'))
jest.mock('../../src/upload/DealPicker', () => mockComponent('DealPicker'))
jest.mock('../../src/upload/SavePreview', () => mockComponent('SavePreview'))
jest.mock('../../src/upload/ParseProgress', () => mockComponent('DealPicker'))

describe('UploadSpreadsheet', () => {
  let props
  beforeEach(() => {
    props = {
      hasUploadErrors: false,
      activeStep: 0,
      initialValues: {
        id: 5,
        dealName: 'some deal'
      },
      classes: {
        root: 'root',
        button: 'button',
        paper: { padding: '1em' }
      },
      handleNext: jest.fn(),
      handleReset: jest.fn(),
      spreadsheetChanged: jest.fn(),
      saveFinalUpload: jest.fn(),
      resetDealSelected: jest.fn()
    }
  })

  it('should render correctly', () => {
    expect(renderer.create(<UploadSpreadsheet {...props} />).toJSON()).toMatchSnapshot()
  })

  it('mapStateToProps', () => {
    const result = mapStateToProps({upload: {activeStep: 'activeStep1', steps: [{id: 'step1'}, {id: 'step2'}]}, deal: {initialValues: {dealName: 'test'}}, marketing: {allSpDeals: [{id: 1, dealName: 'test'}]}})
    expect(result).toEqual({
      activeStep: 'activeStep1',
      deals: [{dealName: '', id: 'empty'}, {dealName: 'test', id: 1}],
      hasUploadErrors: false,
      initialValues: {dealName: 'test'}})
  })

  it('mapStateToProps with errors', () => {
    const result = mapStateToProps({upload: {activeStep: 'activeStep1', steps: [{id: 'step1', errors: ['error1']}, {id: 'step2'}]}, deal: {initialValues: {dealName: 'test'}}, marketing: {initialValues: {dealName: 'test'}, allSpDeals: [{id: 1, dealName: 'test'}]}})
    expect(result).toEqual({
      activeStep: 'activeStep1',
      deals: [{dealName: '', id: 'empty'}, {dealName: 'test', id: 1}],
      hasUploadErrors: true,
      initialValues: {dealName: 'test'}})
  })
})
