import React from 'react'
import renderer from 'react-test-renderer'
import mockConnect from '../MockConnect'
import mockComponent from '../MockComponent'
import { SavePreview, mapStateToProps } from '../../src/upload/SavePreview'
jest.mock('redux-form', () => ({ Field: mockComponent('Field'), reduxForm: () => (comp) => comp }))
jest.mock('@material-ui/core/Grid', () => mockComponent('Grid'))
jest.mock('react-redux', () => mockConnect())
jest.mock('@material-ui/core/Button', () => mockComponent('Button'))
jest.mock('@material-ui/core/Table', () => mockComponent('Table'))
jest.mock('@material-ui/core/TableBody', () => mockComponent('TableBody'))
jest.mock('@material-ui/core/TableCell', () => mockComponent('TableCell'))
jest.mock('@material-ui/core/TableHead', () => mockComponent('TableHead'))
jest.mock('@material-ui/core/TableRow', () => mockComponent('TableRow'))
jest.mock('@material-ui/core/Paper', () => mockComponent('Paper'))
jest.mock('../../src/marketing/marketingActions', () => ({resetSpDealSelected: jest.fn()}))
jest.mock('../../src/deals/dealActions', () => ({resetDealSelected: jest.fn()}))
jest.mock('../../src/upload/uploadActions.js', () => ({handleReset: jest.fn(), saveFinalUpload: jest.fn()}))

describe('SavePreview', () => {
  let props
  beforeEach(() => {
    props = {
      ref: {allRefs: {previewValuesNames: []}},
      form: { UploadSpreadsheetForm: { values: { newDealNotes: 'test' } } },
      excel: {
        dealName: 'newDealName'
      },
      currentDeal: {
        dealName: 'oldDealName',
        legalEntity: {code: 'LE_AME', version: 2}
      },
      saveFinalUpload: jest.fn(),
      resetDealSelected: jest.fn(),
      handleReset: jest.fn(),
      classes: {}
    }
  })

  it('should render correctly', () => {
    expect(renderer.create(<SavePreview {...props}/>).toJSON()).toMatchSnapshot()
  })

  it('map state to props', () => {
    const result = mapStateToProps({form: { UploadSpreadsheetForm: { values: { newDealNotes: 'test' } } }, ref: {allRefs: {previewValuesNames: []}}, upload: {parsed: 'new'}, deal: {currentDeal: 'old'}})
    expect(result).toEqual({newDealNotes: 'test', previewValuesNames: [], excel: 'new', currentDeal: 'old'})
  })
})
