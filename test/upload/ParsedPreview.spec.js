import React from 'react'
import renderer from 'react-test-renderer'
import mockConnect from '../MockConnect'
import mockComponent from '../MockComponent'
import { ParsedPreview } from '../../src/upload/ParsedPreview'

jest.mock('react-redux', () => mockConnect())
jest.mock('@material-ui/core/Grid', () => mockComponent('Grid'))
jest.mock('@material-ui/core/Paper', () => mockComponent('Paper'))
jest.mock('react-json-view', () => mockComponent('ReactJson'))

describe('ParsedPreview', () => {
  let props
  beforeEach(() => {
    props = {
      parsed: true,
      classes: {}
    }
  })

  it('should render correctly', () => {
    expect(renderer.create(<ParsedPreview {...props}/>).toJSON()).toMatchSnapshot()
  })
})
