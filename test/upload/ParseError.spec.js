import React from 'react'
import renderer from 'react-test-renderer'
import mockConnect from '../MockConnect'
import mockComponent from '../MockComponent'
import { ParseError } from '../../src/upload/ParseError'

jest.mock('react-redux', () => mockConnect())
jest.mock('@material-ui/core/Grid', () => mockComponent('Grid'))
jest.mock('@material-ui/core/Card', () => mockComponent('Card'))
jest.mock('@material-ui/core/CardContent', () => mockComponent('CardContent'))
jest.mock('@material-ui/core/Button', () => mockComponent('Button'))
jest.mock('@material-ui/core/Paper', () => mockComponent('Paper'))

describe('ParseError', () => {
  let props
  beforeEach(() => {
    props = {
      parseError: {
        message: 'some message',
        field: 'fieldA',
        row: '12',
        col: 'AA',
        duration: 12,
        company: 'aCompany',
        accountingBasis: 'anAccountingBasis',
        validOptions: ['a', 'b']
      },
      classes: {}
    }
  })

  it('should render correctly', () => {
    expect(renderer.create(<ParseError {...props}/>).toJSON()).toMatchSnapshot()
  })

  it('should render without validOptions', () => {
    props.parseError.validOptions = null
    expect(renderer.create(<ParseError {...props}/>).toJSON()).toMatchSnapshot()
  })

  it('should render without company or accountingBasis', () => {
    props.parseError.company = null
    props.parseError.accountingBasis = null
    expect(renderer.create(<ParseError {...props}/>).toJSON()).toMatchSnapshot()
  })

  it('should render without duration', () => {
    props.parseError.duration = null
    expect(renderer.create(<ParseError {...props}/>).toJSON()).toMatchSnapshot()
  })

  it('should render without col', () => {
    props.parseError.col = null
    expect(renderer.create(<ParseError {...props}/>).toJSON()).toMatchSnapshot()
  })

  it('should render without row', () => {
    props.parseError.row = null
    expect(renderer.create(<ParseError {...props}/>).toJSON()).toMatchSnapshot()
  })
})
