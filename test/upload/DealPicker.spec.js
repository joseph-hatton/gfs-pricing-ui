import React from 'react'
import renderer from 'react-test-renderer'
import mockConnect from '../MockConnect'
import mockComponent from '../MockComponent'
import { DealPicker, mapStateToProps } from '../../src/upload/DealPicker'

jest.mock('redux-form', () => ({ Field: mockComponent('Field'), reduxForm: () => (comp) => comp }))
jest.mock('react-redux', () => mockConnect())
jest.mock('@material-ui/core/Grid', () => mockComponent('Grid'))
jest.mock('material-ui/MenuItem', () => mockComponent('MenuItem'))
jest.mock('redux-form-material-ui', () => ({
  SelectedField: mockComponent('SelectedField'),
  TextField: mockComponent('TextField')
}))

describe('DealPicker', () => {
  let props
  beforeEach(() => {
    props = {
      nullable: true,
      deals: [{id: '123', dealName: 'dealName1'}, {id: '123', dealName: 'dealName2'}],
      currentDeal: {id: '123', dealName: 'dealName1', legalEntities: [{code: 'A', version: 1}, {code: 'B', version: 1}]},
      initialValues: {
        dealName: 'dealName1'
      },
      currentDealSelected: jest.fn(),
      loadAllSpDeals: jest.fn(),
      loadAllDeals: jest.fn(),
      resetDealSelected: jest.fn()
    }
  })

  it('should render correctly', () => {
    expect(renderer.create(<DealPicker {...props} />).toJSON()).toMatchSnapshot()
  })

  it('mapStateToProps', () => {
    const result = mapStateToProps({deal: {initialValues: {dealName: 'deal1'}}, marketing: {allSpDeals: [{id: 1, dealName: 'deal1'}]}})
    expect(result).toEqual({
      deals: [{dealName: 'deal1', id: 1}],
      initialValues: {dealName: 'deal1'}})
  })
})
