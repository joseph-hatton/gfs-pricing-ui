import React from 'react'
import renderer from 'react-test-renderer'
import mockConnect from '../MockConnect'
import mockComponent from '../MockComponent'
import { ParseProgress, mapStateToProps } from '../../src/upload/ParseProgress'

jest.mock('react-redux', () => mockConnect())
jest.mock('@material-ui/core/Grid', () => mockComponent('Grid'))
jest.mock('@material-ui/core/Paper', () => mockComponent('Paper'))
jest.mock('../../src/upload/ParseError', () => mockComponent('ParseError'))
jest.mock('../../src/upload/ValidOptions', () => mockComponent('ValidOptions'))

describe('ParseProgress', () => {
  let props
  beforeEach(() => {
    props = {
      parsing: true,
      errors: ['error1', 'error2'],
      classes: {
        paper: 'paperClass',
        title: 'titleClass',
        progress: 'progressClass'
      },
      currentStep: {
        description: 'doing work'
      }
    }
  })

  it('should render correctly', () => {
    expect(renderer.create(<ParseProgress {...props}/>).toJSON()).toMatchSnapshot()
  })

  it('should render with parsing false', () => {
    props.parsing = false
    expect(renderer.create(<ParseProgress {...props}/>).toJSON()).toMatchSnapshot()
  })

  it('should render with no current step', () => {
    props.currentStep = null
    expect(renderer.create(<ParseProgress {...props}/>).toJSON()).toMatchSnapshot()
  })

  it('should render with no errors', () => {
    props.errors = []
    expect(renderer.create(<ParseProgress {...props}/>).toJSON()).toMatchSnapshot()
  })

  describe('mapStateToProps', () => {
    const step1 = {id: 'first', status: 'STARTED', errors: []}
    const step2 = {id: 'second', status: 'TBD', errors: []}
    const step0 = {id: 'zero', status: 'FAIL', errors: ['error1'], continue: false}

    it('finds first started step', () => {
      const result = mapStateToProps({upload: {parsing: true, steps: [step1]}})

      expect(result).toEqual({currentStep: step1, errors: [], parsing: true})
    })

    it('finds first tbd when no started', () => {
      const result = mapStateToProps({upload: {parsing: true, steps: [step2]}})

      expect(result).toEqual({currentStep: step2, errors: [], parsing: true})
    })

    it('finds first false continue when no started', () => {
      const result = mapStateToProps({upload: {parsing: true, steps: [step0, step1, step2]}})

      expect(result).toEqual({currentStep: step0, errors: [{message: 'error1'}], parsing: true})
    })
  })
})
