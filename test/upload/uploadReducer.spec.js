import uploadReducer from '../../src/upload/uploadReducer'
import parseStepReducers from '../../src/upload/parse/steps/index'
import cloneDeep from 'lodash/cloneDeep'

const getSteps = () =>
  parseStepReducers.map(parseStep => ({id: parseStep.id, description: parseStep.description, errors: [], status: 'TBD', continue: true}))

const initState = {parsed: null, parsing: false}

const stepId = 'validateFileTypeStep'

describe('uploadReducer.js', () => {
  it('spreadsheet parsed start', () => {
    expect(uploadReducer(initState, {type: 'SPREADSHEET_PARSE_START', payload: 2}))
      .toEqual({'activeStep': 2, parsed: null, parsing: true, steps: getSteps()})
  })

  it('spreadsheet parsed success', () => {
    expect(uploadReducer(initState, {type: 'SPREADSHEET_PARSE_SUCCESS', payload: {parsed: 'hey'}}))
      .toEqual({'activeStep': 3, 'parsed': {'parsed': 'hey'}, 'parsing': false})
  })

  it('spreadsheet parsed fail', () => {
    expect(uploadReducer(initState, {type: 'SPREADSHEET_PARSE_FAIL', errors: ['an error']}))
      .toEqual({'activeStep': 2, parsed: null, errors: ['an error'], parsing: false})
  })

  it('spreadsheet step start', () => {
    const state = {...initState, steps: getSteps()}
    const expectedSteps = cloneDeep(state.steps)
    expectedSteps.find((step) => step.id === stepId).status = 'STARTED'
    expect(uploadReducer(state, {type: 'SPREADSHEET_STEP_START', stepId: stepId}))
      .toEqual({...state, steps: expectedSteps})
  })

  it('spreadsheet step success', () => {
    const state = {...initState, steps: getSteps()}
    const expectedSteps = cloneDeep(state.steps)
    expectedSteps.find((step) => step.id === stepId).status = 'SUCCESS'
    expect(uploadReducer(state, {type: 'SPREADSHEET_STEP_SUCCESS', stepId: stepId}))
      .toEqual({...state, steps: expectedSteps})
  })

  it('spreadsheet step fail', () => {
    const state = {...initState, steps: getSteps()}
    const expectedSteps = cloneDeep(state.steps)
    const step = expectedSteps.find((step) => step.id === stepId)
    step.status = 'FAIL'
    step.errors = ['oh no']
    step.continue = false
    expect(uploadReducer(state, {type: 'SPREADSHEET_STEP_FAIL', stepId: stepId, errors: ['oh no'], continue: false}))
      .toEqual({...state, steps: expectedSteps})
  })
})
