import {spreadsheetChanged, saveFinalUpload, handleBack, handleReset, handleNext} from '../../src/upload/uploadActions'
import http from '../../src/actions/http'

jest.mock('../../src/upload/parse/parseUpload', () => () => ({parsed: 'parsedUpload'}))
jest.mock('../../src/actions/http', () => ({post: jest.fn().mockReturnValue('post response')}))

const form = {UploadSpreadsheetForm: {values: {deal: 'a'}}}
const marketing = {allSpDeals: ['a', 'b']}
const deal = {initialValues: {id: 123}, currentDeal: {id: 123, dealName: 'abc'}}
const ref = {allRefs: {uploadConfig: 'hey'}}

describe('uploadActions', () => {
  let dispatch, getState
  beforeEach(() => {
    dispatch = jest.fn()
    getState = jest.fn()
  })

  it('spreadsheetChanged', async () => {
    const currentDeal = {id: 123, legalEntities: [{code: 'A', version: 1}, {code: 'B', version: 1}]}
    getState.mockReturnValue({form, deal: {currentDeal, legalEntity: {code: 'A', version: 1}}, marketing, ref, upload: {steps: [{errors: []}]}})
    await spreadsheetChanged(null, 'file')(dispatch, getState)
    expect(dispatch).toHaveBeenCalledTimes(2)
    expect(dispatch.mock.calls[0][0]).toEqual({
      type: 'SPREADSHEET_PARSE_START',
      payload: 2
    })
    expect(dispatch.mock.calls[1][0]).toEqual({
      type: 'SPREADSHEET_PARSE_SUCCESS',
      payload: 'parsedUpload'
    })
  })

  it('spreadsheetChanged failed', async () => {
    const currentDeal = {id: 123, legalEntities: [{code: 'A', version: 1}, {code: 'B', version: 1}]}
    getState.mockReturnValue({form, deal: {currentDeal, legalEntity: {code: 'A', version: 1}}, marketing, ref, upload: {steps: [{errors: ['oh no']}]}})
    await spreadsheetChanged(null, 'file')(dispatch, getState)
    expect(dispatch).toHaveBeenCalledTimes(2)
    expect(dispatch.mock.calls[0][0]).toEqual({
      type: 'SPREADSHEET_PARSE_START',
      payload: 2
    })
    expect(dispatch.mock.calls[1][0]).toEqual({
      type: 'SPREADSHEET_PARSE_FAIL',
      errors: ['oh no']
    })
  })

  it('saveFinalUpload', async () => {
    getState.mockReturnValue({form: {UploadSpreadsheetForm: {values: {newDealNotes: 'test'}}}, deal, marketing: {currentDeal: {id: 123}}, upload: {parsed: '1'}})
    await saveFinalUpload()(dispatch, getState)

    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch.mock.calls[0][0]).toEqual({
      type: 'SAVE_DATA_FINAL',
      payload: 'post response'
    })
    expect(http.post).toHaveBeenCalledWith('https://gfs-pricing-api.rgare.net/api/v1/deals', {'0': '1', 'id': 123, dealNotes: 'test'}, {successMessage: 'Deal saved successfully'})
  })

  it('handleBack', async () => {
    getState.mockReturnValue({upload: {activeStep: 2}})
    await handleBack()(dispatch, getState)

    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch.mock.calls[0][0]).toEqual({
      type: 'HANDLE_BACK',
      payload: {
        activeStep: 1
      }
    })
  })

  it('handleReset', async () => {
    await handleReset()(dispatch, getState)

    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch.mock.calls[0][0]).toEqual({
      type: 'RESET_STEP',
      payload: {
        activeStep: 0
      }
    })
  })

  it('handleNext', async () => {
    getState.mockReturnValue({upload: {activeStep: 2}})
    await handleNext()(dispatch, getState)

    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch.mock.calls[0][0]).toEqual({
      type: 'NEXT_STEP',
      payload: {
        activeStep: 3
      }
    })
  })
})
