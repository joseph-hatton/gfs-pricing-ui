import React from 'react'
import renderer from 'react-test-renderer'
import mockConnect from '../MockConnect'
import mockComponent from '../MockComponent'
import { ValidOptions, mapStateToProps } from '../../src/upload/ValidOptions'

jest.mock('react-redux', () => mockConnect())
jest.mock('../../src/components/redux-dialog/redux-dialog', () => ({
  __esModule: true,
  default: mockComponent('ReduxDialog'),
  closeDialog: jest.fn()
}))
jest.mock('../../src/pickers/CommonPicker', () => ({CommonPicker: mockComponent('CommonPicker')}))
jest.mock('@material-ui/core/Button', () => mockComponent('Button'))

describe('ValidOptions', () => {
  let props
  beforeEach(() => {
    props = {
      closeDialog: jest.fn(),
      validOptions: {
        title: 'options1',
        validOptions: 'dog'
      }
    }
  })

  it('should render correctly', () => {
    expect(renderer.create(<ValidOptions {...props}/>).toJSON()).toMatchSnapshot()
  })

  it('map state to props', () => {
    const result = mapStateToProps({dialog: {validOptions: props.validOptions}})
    expect(result).toEqual({validOptions: props.validOptions})
  })
})
