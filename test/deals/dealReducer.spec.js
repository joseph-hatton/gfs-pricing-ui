import dealReducer from '../../src/deals/dealReducer'

describe('dealReducer', () => {
  let initial

  beforeEach(() => {
    initial = {
      allDeals: [],
      currentDeal: {
        legalEntities: []
      },
      legalEntity: null
    }
  })

  it('returns initial', () => {
    const result = dealReducer()

    expect(result).toEqual(initial)
  })

  it('load all deals fulfilled', () => {
    const result = dealReducer(initial, {type: 'LOAD_ALL_DEALS_FULFILLED', payload: 'a payload'})

    expect(result).toEqual({...initial, allDeals: 'a payload'})
  })

  it('save deal fulfilled', () => {
    const result = dealReducer(initial, {type: 'SAVE_DEAL_FULFILLED', payload: 'a payload'})

    expect(result).toEqual(initial)
  })

  it('current deal', () => {
    const result = dealReducer(initial, {type: 'CURRENT_DEAL', payload: 'a payload'})

    expect(result).toEqual({...initial, currentDeal: 'a payload', initialValues: 'a payload'})
  })

  it('reset deal', () => {
    const result = dealReducer(initial, {type: 'RESET_DEAL', payload: 'a payload'})

    expect(result).toEqual({...initial, currentDeal: {legalEntities: []}, legalEntity: null, initialValues: null})
  })
})
