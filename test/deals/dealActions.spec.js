import {loadAllDeals, currentDealSelected, resetDealSelected, saveDeal} from '../../src/deals/dealActions'
import http from '../../src/actions/http'

// jest.mock('../../src/upload/parse/parseUpload', () => () => ({parsed: 'parsedUpload'}))
jest.mock('../../src/actions/http', () => ({
  get: jest.fn().mockReturnValue('get response'),
  post: jest.fn().mockReturnValue('post response')
}))

describe('dealActions', () => {
  let dispatch, getState
  beforeEach(() => {
    dispatch = jest.fn()
    getState = jest.fn()
  })

  it('loadAllDeals', () => {
    const result = loadAllDeals()

    expect(result).toEqual({
      type: 'LOAD_ALL_DEALS',
      payload: 'get response'
    })
    expect(http.get).toHaveBeenCalledWith('https://gfs-pricing-api.rgare.net/api/v1/deals')
  })

  it('currentDealSelected matching deal found', () => {
    getState.mockReturnValue({ref: {stages: [{stage: 'Implementing', status: 'In Process'}]}, marketing: {allSpDeals: [{legalEntities: ['a'], id: 0, dealName: 'abc', stage: 'Implementing'}]}, deal: {allDeals: [{legalEntities: [{code: 'ABC', version: 2}], id: 0, uuid: 'uuid1', effectiveDate: '2018-07-24T19:19:19.010Z'}]}})

    currentDealSelected(null, 0)(dispatch, getState)

    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch.mock.calls[0][0]).toEqual({
      type: 'CURRENT_DEAL',
      payload: { dealName: 'abc', stage: 'Implementing', legalEntities: [{code: 'a', version: 0}], 'id': 0 }
    })
  })

  it('currentDealSelected matching deal not found', () => {
    getState.mockReturnValue({
      ref: {stages: [{stage: 'Implementing', status: 'In Process'}]},
      marketing: {allSpDeals: [{legalEntities: ['A', 'B'], stage: 'Implementing', id: 3, dealName: 'abc', effectiveDate: '2018-07-24T19:19:19.010Z'}]},
    deal: {allDeals: [{id: 1, uuid: 'uuid1', effectiveDate: '2018-07-24T19:19:19.010Z', stage: 'a', legalEntities: [{code: 'A', version: 1}, {code: 'B', version: 1}]}]}})

    currentDealSelected(null, 3)(dispatch, getState)

    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch.mock.calls[0][0]).toEqual({
      type: 'CURRENT_DEAL',
      payload: {id: 3, dealName: 'abc', effectiveDate: '2018-07-24T19:19:19.010Z', stage: 'Implementing', legalEntities: [{code: 'A', version: 0}, {code: 'B', version: 0}]}
    })
  })

  it('resetDealSelected', () => {
    resetDealSelected()(dispatch)

    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch.mock.calls[0][0]).toEqual({
      type: 'RESET_DEAL'
    })
  })

  it('saveDeal', async () => {
    getState.mockReturnValue({form: {createDealForm: {values: 'createDealFormValues'}}})
    await saveDeal()(dispatch, getState)

    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch.mock.calls[0][0]).toEqual({
      type: 'SAVE_DEAL',
      payload: 'post response'
    })
    expect(http.post).toHaveBeenCalledWith('https://gfs-pricing-api.rgare.net/api/v1/deals', 'createDealFormValues', {successMessage: 'Deal saved successfully'})
  })
})
