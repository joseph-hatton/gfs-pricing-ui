import React from 'react'
import renderer from 'react-test-renderer'
import mockConnect from '../MockConnect'
import mockComponent from '../MockComponent'
import { CreateDeal, mapStateToProps } from '../../src/deals/CreateDeal'

jest.mock('redux-form', () => ({ Field: mockComponent('Field'), reduxForm: () => (comp) => comp }))
jest.mock('react-redux', () => mockConnect())
jest.mock('@material-ui/core/Grid', () => mockComponent('Grid'))
jest.mock('material-ui/RaisedButton', () => mockComponent('RaisedButton'))
jest.mock('redux-form-material-ui', () => ({
  DatePicker: mockComponent('DatePicker'),
  TextField: mockComponent('TextField')
}))
jest.mock('../../src/pickers/CommonPicker', () => ({CommonPicker: mockComponent('CommonPicker')}))

describe('CreateDeal', () => {
  let props
  beforeEach(() => {
    props = {
      businessTree: {
        legalEntity: ['legal1'],
        currency: ['currency1'],
        office: ['office1'],
        geoSegment: ['geoSegment1']
      },
      productTypeOptions: ['productType1'],
      probOfCloseOptions: ['probClose1'],
      stageOptions: ['stage1'],
      statusOptions: ['status1'],
      countryOptions: ['country1'],
      handleSubmit: jest.fn(),
      saveDeal: jest.fn(),
      resetDealForm: jest.fn()
    }
  })

  it('should render correctly', () => {
    expect(renderer.create(<CreateDeal {...props} />).toJSON()).toMatchSnapshot()
  })

  it('mapStateToProps with no ref data', () => {
    const result = mapStateToProps({ref: {}})
    expect(result).toEqual({businessTree: undefined, countryOptions: undefined, probOfCloseOptions: undefined, productTypeOptions: undefined, stageOptions: undefined, statusOptions: undefined})
  })

  it('mapStateToProps with ref data', () => {
    const result = mapStateToProps({ref: {businessTree: props.businessTree, countries: props.countryOptions, probCloses: props.probOfCloseOptions, productTypes: props.productTypeOptions, stages: props.stageOptions, dealStatuses: props.dealStatuses}})
    expect(result).toEqual({businessTree: props.businessTree, countryOptions: props.countryOptions, probOfCloseOptions: props.probOfCloseOptions, productTypeOptions: props.productTypeOptions, stageOptions: props.stageOptions, statusOptions: props.dealStatuses})
  })
})
