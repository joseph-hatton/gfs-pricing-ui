import React from 'react'
import renderer from 'react-test-renderer'
import mockConnect from '../MockConnect'
import mockComponent from '../MockComponent'
import { closeReportDialog, saveReport, getPricingReports } from '../../src/reports/reportActions'
import { ReportFormDialog, mapStateToProps } from '../../src/reports/ReportFormDialog'

jest.mock('material-ui/RaisedButton', () => mockComponent('RaisedButton'))
jest.mock('redux-form', () => ({ Field: mockComponent('Field'), reduxForm: () => (comp) => comp, reset: 'reset' }))
jest.mock('redux-form-material-ui', () => ({TextField: mockComponent('TextField')}))
jest.mock('react-redux', () => mockConnect())
jest.mock('@material-ui/core/Dialog', () => mockComponent('Dialog'))
jest.mock('@material-ui/core/DialogContent', () => mockComponent('DialogContent'))
jest.mock('@material-ui/core/DialogTitle', () => mockComponent('DialogTitle'))
jest.mock('@material-ui/core/Grid', () => mockComponent('Grid'))
jest.mock('@material-ui/core/Card', () => mockComponent('Card'))
jest.mock('@material-ui/core/CardContent', () => mockComponent('CardContent'))
jest.mock('@material-ui/core/Paper', () => mockComponent('Paper'))

describe('ReportFormDialog', () => {
    let props
    beforeEach(() => {
      props = {
        closeReportDialog,
        saveReport,
        getPricingReports,
        open: true,
        title: 'New Report',
        category: 'General',
        reportName: 'This Test',
        handleSubmit: jest.fn()
      }
    })

    it('should render correctly', () => {
      expect(renderer.create(<ReportFormDialog {...props} />).toJSON()).toMatchSnapshot()
    })
    it('componentWillUnmount should be called on unmount', () => {
        const component = renderer.create(<ReportFormDialog {...props}/>)
        component.unmount()
        expect(getPricingReports).toHaveBeenCalled
    })

    it('mapStateToProps', () => {
        const result = mapStateToProps({report:
            {reportDialog: {open: true, title: 'Edit Report'}, currentReport: {uuid: '1111'}},
            form: {Report: {values: {reportName: 'test', category: 'test'}}}})
        expect(result).toEqual({
            open: true,
            title: 'Edit Report',
            initialValues: {uuid: '1111'}
        })
    })
})
