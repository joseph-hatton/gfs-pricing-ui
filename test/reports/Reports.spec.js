import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../MockComponent'
import { getPricingReports, getCurrentReport, setCategory } from '../../src/reports/reportActions'
import { Reports, mapStateToProps } from '../../src/reports/Reports'
jest.mock('@material-ui/core/Grid', () => mockComponent('Grid'))
jest.mock('redux-form', () => ({ reduxForm: () => (comp) => comp }))
jest.mock('../../src/pickers/CommonPicker', () => ({CommonPicker: mockComponent('CommonPicker')}))
jest.mock('../../src/reports/SpeedDialReport', () => mockComponent('SpeedDialReport'))
jest.mock('../../src/reports/ReportFormDialog', () => mockComponent('ReportFormDialog'))

describe('Report', () => {
    let props
    beforeEach(() => {
      props = {
        getPricingReports,
        getCurrentReport,
        setCategory,
        setCurrentReportNull: jest.fn(),
        pricingReports: [{}, {}],
        optionsCategory: ['A', 'B'],
        currentReport: {uuid: '123'},
        reportingNameOptions: ['A', 'B'],
        initialValues: {uuid: '123', category: 'A', reportName: 'A'},
        currentCategory: 'A'
      }
    })
    it('should Report render correctly', () => {
      expect(renderer.create(<Reports {...props}/>).toJSON()).toMatchSnapshot()
    })
    it('should call getpricing reports', () => {
      props.optionsCategory = []
      renderer.create(<Reports {...props}/>)
      expect(getPricingReports).toHaveBeenCalled
    })

    it('componentWillUnmount should be called on unmount', () => {
      const component = renderer.create(<Reports {...props}/>)
      component.unmount()
      expect(component.componentWillUnmount).toHaveBeenCalled
  })
    it('mapStateToProps', () => {
        const result = mapStateToProps({report: {categories: ['a'], pricingReports: [{category: 'a', reportName: 'b'}], currentReport: null, reportingNameOptions: ['b'], currentCategory: 'A'}, ref: {allRefs: {urlRef: 'hht'}}})
        expect(result).toEqual({
            pricingReports: [{category: 'a', reportName: 'b'}],
            optionsCategory: ['a'],
            currentReport: null,
            reportingNameOptions: ['b'],
            currentCategory: 'A',
            initialValues: null,
            urlRef: 'hht'
        })
    })
})
