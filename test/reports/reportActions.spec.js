import {getPricingReports, openReportDialog, closeReportDialog, setCategory, getCurrentReport,
    handleCloseSpeedDial, handleOpenSpeedDial, handleVisibilitySpeedDial, handleClickSpeedDial, deleteReport, saveReport} from '../../src/reports/reportActions'

    jest.mock('../../src/actions/http', () => ({
        get: jest.fn().mockReturnValue('get'),
        put: jest.fn().mockReturnValue('put'),
        post: jest.fn().mockReturnValue('post'),
        delete: jest.fn().mockReturnValue('delete')
    }))
describe('reportActions', () => {
  let dispatch, getState
  beforeEach(() => {
    jest.clearAllMocks()
    dispatch = jest.fn()
    getState = jest.fn()
  })
  it('getPricingReports', async () => {
    await getPricingReports()(dispatch, getState)
    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch.mock.calls[0][0].type).toEqual('LOAD_ALL_REPORTS')
    expect(dispatch.mock.calls[0][0].payload).toEqual('get')
  })

  it('saveReport', async () => {
    getState.mockReturnValue({ form: { addEditReportForm: { values: { category: 'a', reportName: 'b', description: 'c', tableauLink: 'aa' } } },
    report: {currentReport: null, reportDialog: {title: 'New Report'}},
    ref: {allRefs: {urlRef: 'hhh'}}})
    await saveReport()(dispatch, getState)
    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch.mock.calls[0][0].type).toEqual('SAVE_REPORT')
    expect(dispatch.mock.calls[0][0].payload).toEqual('post')
  })

it('deleteReport', async () => {
    getState.mockReturnValue({ report: { currentReport: { uuid: '1111' } },
    ref: {allRefs: {urlRef: 'hhh'}}})
    await deleteReport()(dispatch, getState)
    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch.mock.calls[0][0].type).toEqual('DELETE_REPORT')
    expect(dispatch.mock.calls[0][0].payload).toEqual('delete')
  })

  it('openReportDialog', async () => {
    await openReportDialog('New Report')(dispatch, getState)
    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch.mock.calls[0][0].type).toEqual('OPEN_REPORT_DIALOG')
    expect(dispatch.mock.calls[0][0].payload).toEqual('New Report')
  })

  it('closeReportDialog', async () => {
    getState.mockReturnValue({report: {pricingReports: []}})
    await closeReportDialog()(dispatch, getState)
    expect(dispatch).toHaveBeenCalledTimes(2)
    expect(dispatch.mock.calls[1][0].type).toEqual('CLOSE_REPORT_DIALOG')
  })

  it('setCategory', async () => {
    const pricingReports = [{'category': 'a', 'reportName': 'a'}, {'category': 'a', reportName: 'b'}]
    getState.mockReturnValue({ report: {pricingReports} })
    const e = jest.fn()
    const value = 'a'
    await setCategory(e, value)(dispatch, getState)
    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch.mock.calls[0][0].type).toEqual('SET_CATEGORY')
    expect(dispatch.mock.calls[0][0].payload).toEqual({currentCategory: 'a', reportingNameOptions: ['a', 'b']})
  })

  it('getCurrentReport', async () => {
    const pricingReports = [{'category': 'a', 'reportName': 'aB'}, {'category': 'a', reportName: 'b'}]
    const currentCategory = 'a'
    getState.mockReturnValue({ report: {pricingReports, currentCategory} })
    const e = jest.fn()
    const value = 'aB'
    await getCurrentReport(e, value)(dispatch, getState)
    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch.mock.calls[0][0].type).toEqual('CURRENT_REPORT')
    expect(dispatch.mock.calls[0][0].payload).toEqual({category: 'a', reportName: 'aB'})
  })

  it('handleCloseSpeedDial', async () => {
    await handleCloseSpeedDial()(dispatch, getState)
    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch.mock.calls[0][0].type).toEqual('CLOSE_SPEED_DIAL')
  })

  it('handleOpenSpeedDial', async () => {
    getState.mockReturnValue({ report: { speedDial: { hidden: false } } })
    await handleOpenSpeedDial()(dispatch, getState)
    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch.mock.calls[0][0].type).toEqual('OPEN_SPEED_DIAL')
  })

  it('handleClickSpeedDial', async () => {
    getState.mockReturnValue({ report: { speedDial: { open: false } } })
    await handleClickSpeedDial()(dispatch, getState)
    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch.mock.calls[0][0].type).toEqual('CLICK_SPEED_DIAL')
    expect(dispatch.mock.calls[0][0].payload).toEqual(true)
  })

  it('handleVisibilitySpeedDial', async () => {
    getState.mockReturnValue({report: {speedDial: {hidden: true}}})
    await handleVisibilitySpeedDial()(dispatch, getState)
    expect(dispatch).toHaveBeenCalledTimes(1)
    expect(dispatch.mock.calls[0][0].type).toEqual('VISIBILITY_SPEED_DIAL')
    expect(dispatch.mock.calls[0][0].payload).toEqual({open: false, hidden: false})
  })
})
