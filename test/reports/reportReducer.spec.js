import reportReducer from '../../src/reports/reportReducer'

let initialState = {
      reportDialog: { open: false, title: null },
      speedDial: { open: false, hidden: false },
      pricingReports: [],
      currentReport: null,
      currentCategory: null,
      reportingNameOptions: [],
      categories: [],
      reload: false
    }

describe('reportReducer', () => {
  it('reportReducer ', () => {
    const res = reportReducer(initialState, {type: 'OPEN_REPORT_DIALOG'})
    expect(res.reportDialog.open).toEqual(true)
  })

  it('CLOSE_REPORT_DIALOG ', () => {
    const res = reportReducer(initialState, {type: 'CLOSE_REPORT_DIALOG'})
    expect(res.reportDialog.open).toEqual(false)
  })

  it('LOAD_ALL_REPORTS_FULFILLED ', () => {
    const pricingReports = [{app: 'GFS Pricing'}, {app: 'GFS Pricing'}, {app: 'GFS Pricing'}]
    const res = reportReducer(initialState, {type: 'LOAD_ALL_REPORTS_FULFILLED', payload: pricingReports})
    expect(res.pricingReports).toEqual(pricingReports)
  })
  it('CURRENT_REPORT ', () => {
    const res = reportReducer(initialState, {type: 'CURRENT_REPORT', payload: 'this is the current'})
    expect(res.currentReport).toEqual('this is the current')
  })
  it('LOAD_CATEGORY ', () => {
  const categories = ['a', 'b']
    const res = reportReducer(initialState, {type: 'LOAD_CATEGORY', payload: categories})
    expect(res.categories).toEqual(categories)
  })

  it('SAVE_REPORT_FULFILLED ', () => {
    const pricingReports = [{app: 'GFS Pricing'}, {app: 'GFS Pricing'}, {app: 'GFS Pricing'}]
    initialState.pricingReports = pricingReports
      const res = reportReducer(initialState, {type: 'SAVE_REPORT_FULFILLED', payload: {category: 'A', uuid: '1111', app: 'GFS Pricing'}})
      expect(res.pricingReports.length).toEqual(4)
    })

it('SET_CATEGORY ', () => {
        const res = reportReducer(initialState, {type: 'SET_CATEGORY', payload: {currentCategory: 'A', reportingNameOptions: ['A', 'B']}})
        expect(res.currentCategory).toEqual('A')
        expect(res.reportingNameOptions).toEqual(['A', 'B'])
        expect(res.currentReport).toEqual({category: 'A'})
    })

it('CLOSE_SPEED_DIAL ', () => {
const res = reportReducer(initialState, {type: 'CLOSE_SPEED_DIAL'})
expect(res.speedDial.open).toEqual(false)
})
it('OPEN_SPEED_DIAL ', () => {
const res = reportReducer(initialState, {type: 'OPEN_SPEED_DIAL'})
expect(res.speedDial.open).toEqual(true)
})
it('CLICK_SPEED_DIAL ', () => {
const res = reportReducer(initialState, {type: 'CLICK_SPEED_DIAL', payload: true})
expect(res.speedDial.open).toEqual(true)
})
it('VISIBILITY_SPEED_DIAL ', () => {
const res = reportReducer(initialState, {type: 'VISIBILITY_SPEED_DIAL', payload: {open: true}})
expect(res.speedDial).toEqual({open: true})
})
it('DELETE_REPORT_FULFILLED ', () => {
const pricingReports = [{uuid: '11', app: 'GFS Pricing'}, {uuid: '12', app: 'GFS Pricing'}, {uuid: '1111', app: 'GFS Pricing'}]
initialState.pricingReports = pricingReports
const res = reportReducer(initialState, {type: 'DELETE_REPORT_FULFILLED', payload: {uuid: '1111'}})
expect(res.pricingReports.length).toEqual(2)
})
})
