import React from 'react'
import renderer from 'react-test-renderer'
import mockComponent from '../MockComponent'
import { deleteReport, openReportDialog, handleCloseSpeedDial, handleVisibilitySpeedDial, handleClickSpeedDial, handleOpenSpeedDial } from '../../src/reports/reportActions'
import { SpeedDialReport, mapStateToProps } from '../../src/reports/SpeedDialReport'
jest.mock('@material-ui/lab/SpeedDial', () => mockComponent('SpeedDial'))
jest.mock('@material-ui/lab/SpeedDialIcon', () => mockComponent('SpeedDialIcon'))
jest.mock('@material-ui/lab/SpeedDialAction', () => mockComponent('SpeedDialAction'))
jest.mock('@material-ui/icons/Delete', () => mockComponent('DeleteIcon'))
jest.mock('@material-ui/icons/Add', () => mockComponent('AddIcon'))
jest.mock('@material-ui/icons/Edit', () => mockComponent('EditIcon'))

describe('SpeedDialReport', () => {
    let props
    beforeEach(() => {
      props = {
        classes: {},
        handleCloseSpeedDial,
        handleVisibilitySpeedDial,
        handleClickSpeedDial,
        handleOpenSpeedDial,
        openReportDialog,
        deleteReport,
        currentReport: null,
        open: false,
        hidden: true
      }
    })

    it('should SpeedDialReport render correctly', () => {
      expect(renderer.create(<SpeedDialReport {...props}/>).toJSON()).toMatchSnapshot()
    })
    it('should SpeedDialReport render correctly with current report', () => {
      props.currentReport = {uuid: '11111'}
      expect(renderer.create(<SpeedDialReport {...props}/>).toJSON()).toMatchSnapshot()
    })

    it('mapStateToProps', () => {
      const result = mapStateToProps({report: {currentReport: {},
speedDial:
        { open: false, hidden: false }}})
      expect(result).toEqual({
        open: false,
        hidden: false,
        currentReport: { }
      })
  })
})
