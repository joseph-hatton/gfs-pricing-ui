const assert = require('assert')
const {baseUrl} = require('./config')
Feature('upload integration tests')

const spreadsheetWithErrors = 'test-integration/data/PDW template AI AXA ver 5-errs.xlsm'
const finalWithNoVectors = 'test-integration/data/AI no vectortab.xlsm'
const finalWithNoErrors = 'test-integration/data/AI no errors.xlsm'

Scenario('Upload Pricing Data with errors - AI', async (I, uploadPage, authPage) => {

  await uploadPage.uploadSpreadsheet(authPage, spreadsheetWithErrors)
  
  uploadPage.waitForErrorsToFullyShow()
/*
  I.seeNumberOfElements(uploadPage.uploadError, 181)
  await uploadPage.validateErrorShown('product_type', ['UploadScalar', 'productType', 'required field not found'])
  await uploadPage.validateErrorShown('legal_entity', ['UploadScalar', 'legalEntity', 'row: 8', 'invalid legalEntity', 'input: LE_AEM'])
  await uploadPage.validateErrorShown('geo_segment', ['UploadScalar', 'geoSegment', 'row: 9', 'invalid geoSegment', 'input: US-LASA'])

  await uploadPage.validateErrorShown('legal_entity', ['UploadScalar', 'legalEntity', 'row: 29 col: A', 'invalid legalEntity', 'input: LE_AEM'])
  await uploadPage.validateErrorShown('accounting_basis', ['UploadScalar', 'accountingBasis', 'row: 29 col: B', 'invalid accountingBasis', 'input: GAAPEC'])

  await uploadPage.validateErrorShown('legal_entity', ['UploadScalar', 'legalEntity', 'row: 30 col: A', 'invalid legalEntity', 'input: empty'])
  await uploadPage.validateErrorShown('accounting_basis', ['UploadScalar', 'accountingBasis', 'row: 30 col: B', 'invalid accountingBasis', 'input: empty'])

  await uploadPage.validateErrorShown('init_collateral', ['UploadScalar - LE_AME / Local US', 'initCollateral', 'row: 106', 'no value provided for required field'])

  await uploadPage.validateErrorShown('deal_name', ['UploadScalar', 'dealName', 'row: 2', 'in the file does not match Deal name selected on the screen', 'input: AXA Equitable FDA uhh', 'calc: AXA Equitable FDA'])

  await uploadPage.validateErrorShown('legal_entity', ['UploadVector', 'legalEntity', 'row: 1 col: B', 'invalid legalEntity', 'input: LE_AEM'])
  await uploadPage.validateErrorShown('accounting_basis', ['UploadVector', 'accountingBasis', 'row: 2 col: B', 'invalid accountingBasis', 'input: GAAPEC'])
  await uploadPage.validateErrorShown('legal_entity', ['UploadVector', 'legalEntity', 'row: 1 col: G', 'invalid legalEntity', 'input: empty'])
  await uploadPage.validateErrorShown('accounting_basis', ['UploadVector', 'accountingBasis', 'row: 2 col: G', 'invalid accountingBasis', 'input: empty'])
  await uploadPage.validateErrorShown('posttax_income', ['UploadVector - LE_AME / GAAP EC', 'posttaxIncome', 'row: 23 col: AD duration: 18', 'Post Tax Income input value does not equal calculated value', 'input: 138006', 'calc: 137965'])
  await uploadPage.validateErrorShown('something_unexpected', ['UploadVector - LE_AME / GAAP EC', 'somethingUnexpected', 'row: 3 col: X', 'invalid field name'])
  await uploadPage.validateErrorShown('incr_cap', ['UploadVector - LE_AME / GAAP EC', 'incrCap', 'row: 8 col: AG duration: 3', 'Increase of Capital input value does not equal calculated value', 'input: -88888', 'calc: -87824'])
  await uploadPage.validateErrorShown('dist_earn', ['UploadVector - LE_AME / GAAP EC', 'distEarn', 'row: 8 col: AH duration: 3', 'Distributable Earnings input value does not equal calculated value', 'input: 159372', 'calc: 160436'])
  await uploadPage.validateErrorShown('nbev_1', ['UploadScalar - LE_AME / GAAP EC', 'nbev1', 'row: 36', 'NBEV input value does not equal PV of Distributable Earnings', 'input: 8567883', 'calc: 8562963'])
  await uploadPage.validateErrorShown('nbev_1', ['UploadScalar - LE_AME / GAAP S&P', 'nbev1', 'row: 60', 'NBEV input value does not equal PV of Distributable Earnings', 'input: 3093660', 'calc: 3089660'])
  await uploadPage.validateErrorShown('nbev_2', ['UploadScalar - LE_AME / GAAP S&P', 'nbev2', 'row: 62', 'NBEV input value does not equal PV of Distributable Earnings', 'input: -5557363', 'calc: -6880455'])
  await uploadPage.validateErrorShown('nbev_deal', ['UploadScalar - LE_AME / GAAP EC', 'nbevDeal', 'row: 19', 'NBEV input value does not equal PV of Distributable Earnings', 'input: 9000000', 'calc: 8562963'])
  */
})

Scenario('upload final error spreadsheet with no vector sheet', async (I, uploadPage, authPage) => {
  await uploadPage.uploadSpreadsheet(authPage, finalWithNoVectors)
  uploadPage.waitForErrorsToFullyShow()

  // I.seeNumberOfElements(uploadPage.uploadError, 50)

 //  uploadPage.validateErrorShown('workbook_sheet_upload_vector', ['UploadVector', 'Workbook Sheet UploadVector', 'is required when Deal "status" is Final'])
})

Scenario('upload final spreadsheet with no errors', async (I, uploadPage, authPage) => {
  await uploadPage.uploadSpreadsheet(authPage, finalWithNoErrors)
  uploadPage.waitForValidationToComplete()

  /* I.dontSeeElement(`${uploadPage.saveButton}:disabled`)
  I.see('Final', '.stage_new')
  I.see('US STL', '.office_new')
  I.see('Final', '.status_new')
  I.see('USA', '.country_new')
  I.see('AXA Equitable FDA', '.dealName_new')
  I.see('High', '.probClose_new')
  I.see('LE_AME', '.legalEntity_new')
  I.see('Asset Intensive', '.productType_new')
  I.see('2011-11-11', '.effectiveDate_new') */
})
