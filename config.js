const agent = require('superagent')

const baseUrl = process.env.BASE_URL || 'http://localhost:3005'
const vaultPath = process.env.VAULT_PATH || 'StableValue_dev'
const vaultAppId = process.env.VAULT_APP_ID
const vaultUserId = process.env.VAULT_USER_ID || 'StableValue_dev_USER'

const logIntoVault = async () => {
  const {body} = await agent.post('https://serviceregistry.rgare.net:8201/v1/auth/app-id/login')
    .type('application/json')
    .send({
      app_id: vaultAppId,
      user_id: vaultUserId
    })
  return body.auth.client_token
}

const getVaultSecret = async (clientToken, secret) => {
  const {body} = await agent.get(`https://serviceregistry.rgare.net:8201/v1/secret/${vaultPath}/${secret}`)
    .type('application/json')
    .set('X-Vault-Token', clientToken)
  return body.data.value
}

const getUserAndPass = async () => {
  let {user, password} = {}

  if (vaultAppId) {
    const vaultToken = await logIntoVault()
    user = await getVaultSecret(vaultToken, 'API_TESTING_USER')
    password = await getVaultSecret(vaultToken, 'API_TESTING_USER_PASSWORD')
  } else {
    console.log('No VAULT_APP_ID environment variable was found, unable to get API_TESTING_USER and API_TESTING_USER_PASSWORD')
  }
  return {user, password}
}

module.exports = {
  baseUrl,
  getUserAndPass,
  vault: {
    path: vaultPath,
    appId: vaultAppId,
    userId: vaultUserId
  }
}