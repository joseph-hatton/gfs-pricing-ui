# Pricing UI

---

## Version Control

* https://tfs.rgare.net/UWSolutions/uws-gfs/_git/gfs-pricing-ui

---

## Continuous Integration

* https://almjenkins.rgare.net/job/ActuarialSolutions/job/GFS%20Pricing/
  * https://almjenkins.rgare.net/job/ActuarialSolutions/job/GFS%20Pricing/job/build%20ui/
  * https://almjenkins.rgare.net/job/ActuarialSolutions/job/GFS%20Pricing/job/integration%20tests%20against%20dev/

---

## Local Development

1. `npm install`
2. `npm run dev`

---

## Build Deployable

1. `npm install`
2. `npm run build`
  * Builds an nginx container with the js/images/html of the webapp

---

## Unit Tests

* `npm run test`
  * To do a single run with coverage cli output
* `npm run test:dev`
  * To have the tests re-run when any source/test file is changed
* `npm run test:coverage`
  * To get the coverage output

---

## Integration Tests

* Against local UI (**DOESN'T** need a VAULT_APP_ID to get a user/pass)
  1.  run `npm run dev` to have a local UI
  2.  `npm run test:it`
* Against dev (**DOES** need a VAULT_APP_ID to get a user/pass)
  1.  `VAULT_APP_ID={put your vault app id here} npm run test:itd`
    * The code uses StableValue_dev and StableValue_dev_USER to get a user/password to authenticate with SecureAuth.